-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2015 at 11:53 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbbnb`
--

-- --------------------------------------------------------

--
-- Table structure for table `api`
--

CREATE TABLE IF NOT EXISTS `api` (
`client_id` int(10) unsigned NOT NULL,
  `public_id` char(64) NOT NULL DEFAULT '',
  `private_key` char(64) NOT NULL DEFAULT '',
  `status` enum('ACTIVE','INACTIVE') DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `api`
--

INSERT INTO `api` (`client_id`, `public_id`, `private_key`, `status`) VALUES
(1, '', '593fe6ed77014f9507761028801aa376f141916bd26b1b3f0271b5ec3135b989', 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `centernews`
--

CREATE TABLE IF NOT EXISTS `centernews` (
`newsid` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `newsslugs` varchar(500) NOT NULL,
  `author` varchar(250) NOT NULL,
  `body` text NOT NULL,
  `banner` varchar(500) NOT NULL,
  `newslocation` varchar(500) NOT NULL,
  `status` int(11) NOT NULL,
  `date` date NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `type` varchar(250) NOT NULL DEFAULT 'Posts'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `centernews`
--

INSERT INTO `centernews` (`newsid`, `title`, `newsslugs`, `author`, `body`, `banner`, `newslocation`, `status`, `date`, `views`, `type`) VALUES
(1, 'asdas', 'asdas', 'dasdasd', '<p>asdasd</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Scottsdale', 1, '2015-05-13', 1, 'News'),
(2, 'asnmnbmnbm', 'asnmnbmnbm', 'nbmnbmnbm', '<p>bnmbnmbnmnm</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Brea', 1, '2015-05-07', 1, 'News'),
(3, 'jhkhjkjhkjk', 'jhkhjkjhkjk', 'khjkhjkh', '<p>jhkhjkjhkhjk</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Scottsdale', 1, '2015-04-30', 1, 'News'),
(4, 'bnmbmbm', 'bnmbmbm', 'mbnmbm', '<p>bnmbmbnmbnm</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Brea', 1, '2015-04-08', 1, 'News'),
(5, 'dfhdahaeh', 'dfhdahaeh', 'erhrehaehreh', '<p>dfhdfhdfhdhf</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Anaheim Hills', 1, '2015-04-16', 1, 'News'),
(6, 'cvcxvcx', 'cvcxvcx', 'vcxvcxv', '<p>xcvxcvxcv</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Anaheim Hills', 1, '2015-05-21', 1, 'News'),
(7, 'xcvxcvcx', 'xcvxcvcx', 'vxcvcxvcxv', '<p>zxcxzczxc</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Glendale-AZ', 1, '2015-05-07', 1, 'News'),
(8, 'asdsad', 'asdsad', 'dasdasd', '<p>sadsad</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Sun City', 1, '2015-05-14', 1, 'News'),
(9, 'xzczxc', 'xzczxc', 'xzcxzczxczxc', '<p>xcxzc</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Glendale-AZ', 1, '2015-05-09', 1, 'News');

-- --------------------------------------------------------

--
-- Table structure for table `centernewsimage`
--

CREATE TABLE IF NOT EXISTS `centernewsimage` (
`id` int(11) NOT NULL,
  `filename` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `leftsidebaritem`
--

CREATE TABLE IF NOT EXISTS `leftsidebaritem` (
`id` int(11) NOT NULL,
  `sidebarid` int(11) NOT NULL,
  `item` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leftsidebaritem`
--

INSERT INTO `leftsidebaritem` (`id`, `sidebarid`, `item`) VALUES
(1, 1, 'About Body & Brain'),
(2, 1, 'How Are We Different?'),
(3, 1, 'Core Energy Principles'),
(4, 1, 'Founder Ilchi Lee'),
(5, 1, '30 Day Guarantee'),
(6, 1, 'Research'),
(7, 1, 'FAQ'),
(8, 1, 'Practice at Home'),
(9, 2, 'Overview'),
(10, 2, 'Video Classes'),
(11, 2, 'Tai Chi & Qigong'),
(12, 2, 'Energy Meditation'),
(13, 2, 'Core Strengthening'),
(14, 2, 'Glossary'),
(15, 2, 'Class Etiquette'),
(16, 2, 'Plate Balancing Exercise'),
(17, 3, 'Overview'),
(18, 3, 'Initial Awakening'),
(19, 3, 'Finding True Self'),
(20, 3, 'Sedona Retreat for Change'),
(21, 3, 'Solar Body Natural Healing Course'),
(22, 3, 'DahnMuDo'),
(23, 3, 'Brain Management Training'),
(24, 3, 'Dahn Master Course'),
(25, 3, 'Meditation Tours'),
(26, 3, 'Schedules'),
(27, 3, 'Healthier Together'),
(28, 4, 'Web Design'),
(29, 4, 'Our Programs'),
(30, 4, 'Getting Started'),
(31, 4, 'Success Stories'),
(32, 5, 'Overview'),
(33, 5, 'Community Classes'),
(34, 5, 'Project Orange'),
(35, 5, 'Book Donations'),
(36, 5, 'Nevada 1st Project'),
(37, 5, 'Nicaragua Project');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
`newsid` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `newsslugs` varchar(500) NOT NULL,
  `author` varchar(250) NOT NULL,
  `body` text NOT NULL,
  `banner` varchar(500) NOT NULL,
  `newslocation` varchar(500) NOT NULL,
  `category` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `date` date NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `type` varchar(250) NOT NULL DEFAULT 'Posts'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`newsid`, `title`, `newsslugs`, `author`, `body`, `banner`, `newslocation`, `category`, `status`, `date`, `views`, `type`) VALUES
(3, 'cxczx', 'cxczx', 'sdasd', '<p>dsaadasd</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Main Site', 'Nutrition', 1, '2015-05-07', 1, 'News'),
(4, 'sadasd', 'sadasd', 'asdasd', '<p>asdsad</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Main Site', 'Yoga Tips', 1, '2015-05-13', 1, 'News'),
(5, 'bcnbnbv', 'bcnbnbv', 'n,mnv,vb,', '<p>xcvxcvcxvxcvcvcv</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Main Site', 'Nutrition', 1, '2015-05-13', 1, 'News'),
(6, 'ttyiuuyuoppi[op[', 'ttyiuuyuoppiop', 'po[po[po[op[op', '<p>fzdvdfvfdgfdbfdb</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Main Site', 'Energy Principles', 1, '2015-04-16', 1, 'News'),
(7, 'cxvcxvcvzbvc', 'cxvcxvcvzbvc', 'bvcbxcvb', '<p>xcvbcvbcxvbvcb</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Main Site', 'Founder''s Wisdom', 1, '2015-04-24', 1, 'News'),
(8, 'cxvxcvcxv', 'cxvxcvcxv', 'xcvxcv', '<p>zxczxczxcxzc</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Main Site', 'Tai Chi & Qigong', 0, '2015-05-07', 1, 'News'),
(9, 'xzczxczxczxc', 'xzczxczxczxc', 'zxczxczxczxc', '<p>xzczxczxc</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Main Site', 'Yoga Tips', 1, '2015-05-07', 1, 'News'),
(10, 'vcxvxcvxcv', 'vcxvxcvxcv', 'xcvcxvcx', '<p>xcvcxvcxvcv</p>\n', '11329903_671015593004752_4636015385090592721_n.jpg', 'Main Site', 'Nutrition', 1, '2015-05-20', 1, 'News');

-- --------------------------------------------------------

--
-- Table structure for table `newscategory`
--

CREATE TABLE IF NOT EXISTS `newscategory` (
`id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newscategory`
--

INSERT INTO `newscategory` (`id`, `name`) VALUES
(1, 'Yoga Tips'),
(2, 'Meditation'),
(3, 'Nutrition'),
(4, 'Tai Chi & Qigong'),
(5, 'Founder''s Wisdom'),
(6, 'Energy Principles'),
(7, 'Spirit of Body & Brain yoga'),
(8, 'Body & Brain yoga ');

-- --------------------------------------------------------

--
-- Table structure for table `newsimage`
--

CREATE TABLE IF NOT EXISTS `newsimage` (
`id` int(11) NOT NULL,
  `filename` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsimage`
--

INSERT INTO `newsimage` (`id`, `filename`) VALUES
(1, '11329903_671015593004752_4636015385090592721_n.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pageimage`
--

CREATE TABLE IF NOT EXISTS `pageimage` (
`id` int(11) NOT NULL,
  `filename` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pageleftbar`
--

CREATE TABLE IF NOT EXISTS `pageleftbar` (
`id` int(11) NOT NULL,
  `pageid` int(11) NOT NULL,
  `item` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pageleftbar`
--

INSERT INTO `pageleftbar` (`id`, `pageid`, `item`) VALUES
(25, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pagerightbar`
--

CREATE TABLE IF NOT EXISTS `pagerightbar` (
`id` int(11) NOT NULL,
  `pageid` int(11) NOT NULL,
  `item` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pagerightbar`
--

INSERT INTO `pagerightbar` (`id`, `pageid`, `item`) VALUES
(73, 1, 1),
(74, 1, 2),
(75, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
`pageid` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `pageslugs` varchar(100) NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `pagelayout` int(11) NOT NULL,
  `type` varchar(250) NOT NULL DEFAULT 'Pages'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `queryerror`
--

CREATE TABLE IF NOT EXISTS `queryerror` (
`error_id` int(10) unsigned NOT NULL,
  `query` text,
  `file` varchar(1024) DEFAULT '',
  `line` int(10) unsigned DEFAULT NULL,
  `error_string` varchar(1024) DEFAULT '',
  `error_no` int(10) unsigned DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `execution_script` varchar(1024) DEFAULT '',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `runtimeerror`
--

CREATE TABLE IF NOT EXISTS `runtimeerror` (
`error_id` int(10) unsigned NOT NULL,
  `title` varchar(2048) NOT NULL DEFAULT '',
  `file` varchar(1024) DEFAULT '',
  `line` int(10) unsigned DEFAULT NULL,
  `error_type` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `server_name` varchar(100) DEFAULT NULL,
  `execution_script` varchar(1024) NOT NULL DEFAULT '',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(250) NOT NULL,
  `task` varchar(20) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `country` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `profile_pic_name` varchar(250) DEFAULT NULL,
  `activation_code` varchar(150) DEFAULT NULL,
  `status` varchar(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `task`, `first_name`, `last_name`, `birthday`, `gender`, `country`, `state`, `profile_pic_name`, `activation_code`, `status`, `created_at`, `updated_at`) VALUES
('03686d5c-bd04-45a4-d8ff-5f7a00ee23df', 'superagent', 'efrenbautistajr@geeksnest.com', '7c222fb2927d828af22f592134e8932480637c0d', '', 'efren', 'bautista', '1986-02-15', 'male', 'Philippines', 'Here', NULL, NULL, '1', '2015-04-20 16:09:57', '2015-05-13 06:47:41'),
('14F6A274-19C8-4AC6-B03C-79A74FF7DE67', 'user', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'BA1E4140-C6BC-41E8-A078-BB69E34FA3A9', '0', '2015-05-21 05:32:54', '2015-05-20 21:32:54'),
('1C54E286-DE95-4C9B-8B7B-49DCE1F0C7DA', 'user12', 'email@email', 'ca9ae38be74d2bd370bee71c6353a8157cb4e414', 'Administrator ', 'fname', 'lname', '2015-05-20', 'Male', NULL, NULL, NULL, 'FBFE036F-DCD0-4ABF-A6A6-6DDD9199FD74', '0', '2015-05-21 05:32:59', '2015-05-20 21:32:59'),
('C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 'ebautistaj', 'efrenbautistajr@gmail.com', '13ec182672f07a51b64d88483486189000cc4d87', '', 'efren', 'bautista', '1999-03-15', 'male', 'Angola', 'Bie', NULL, '', '1', '2015-04-20 16:09:57', '2015-04-20 08:09:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api`
--
ALTER TABLE `api`
 ADD PRIMARY KEY (`client_id`), ADD UNIQUE KEY `private_key` (`private_key`), ADD UNIQUE KEY `public_id` (`public_id`);

--
-- Indexes for table `centernews`
--
ALTER TABLE `centernews`
 ADD PRIMARY KEY (`newsid`);

--
-- Indexes for table `centernewsimage`
--
ALTER TABLE `centernewsimage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leftsidebaritem`
--
ALTER TABLE `leftsidebaritem`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`newsid`);

--
-- Indexes for table `newscategory`
--
ALTER TABLE `newscategory`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsimage`
--
ALTER TABLE `newsimage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pageimage`
--
ALTER TABLE `pageimage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pageleftbar`
--
ALTER TABLE `pageleftbar`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pagerightbar`
--
ALTER TABLE `pagerightbar`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
 ADD PRIMARY KEY (`pageid`);

--
-- Indexes for table `queryerror`
--
ALTER TABLE `queryerror`
 ADD PRIMARY KEY (`error_id`);

--
-- Indexes for table `runtimeerror`
--
ALTER TABLE `runtimeerror`
 ADD PRIMARY KEY (`error_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api`
--
ALTER TABLE `api`
MODIFY `client_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `centernews`
--
ALTER TABLE `centernews`
MODIFY `newsid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `centernewsimage`
--
ALTER TABLE `centernewsimage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `leftsidebaritem`
--
ALTER TABLE `leftsidebaritem`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
MODIFY `newsid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `newscategory`
--
ALTER TABLE `newscategory`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `newsimage`
--
ALTER TABLE `newsimage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pageimage`
--
ALTER TABLE `pageimage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pageleftbar`
--
ALTER TABLE `pageleftbar`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `pagerightbar`
--
ALTER TABLE `pagerightbar`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
MODIFY `pageid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `queryerror`
--
ALTER TABLE `queryerror`
MODIFY `error_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `runtimeerror`
--
ALTER TABLE `runtimeerror`
MODIFY `error_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
