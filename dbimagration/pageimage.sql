-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2015 at 10:06 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbbnb`
--

-- --------------------------------------------------------

--
-- Table structure for table `pageimage`
--

CREATE TABLE IF NOT EXISTS `pageimage` (
`id` int(11) NOT NULL,
  `filename` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pageimage`
--

INSERT INTO `pageimage` (`id`, `filename`) VALUES
(1, '19686_671015613004750_6655552322501895685_n.jpg'),
(2, '11059529_671015606338084_2006792094720043078_n.jpg'),
(3, '11329903_671015593004752_4636015385090592721_n.jpg'),
(4, '11233426_671015599671418_8658272624359384196_n.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pageimage`
--
ALTER TABLE `pageimage`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pageimage`
--
ALTER TABLE `pageimage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
