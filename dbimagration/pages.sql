-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2015 at 10:06 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbbnb`
--

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
`pageid` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `pageslugs` varchar(100) NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `pagelayout` int(11) NOT NULL,
  `type` varchar(250) NOT NULL DEFAULT 'Pages'
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`pageid`, `title`, `pageslugs`, `body`, `status`, `pagelayout`, `type`) VALUES
(1, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 0, 2, 'Pages'),
(2, 'Asdasd', 'asdasd', '<p>asdasd</p>\n', 0, 2, 'Pages'),
(3, 'bvnvbn', 'bvnvbn', '<p>vbnvbn</p>\n', 1, 2, 'Pages'),
(4, 'bnmbnmnmnm', 'bnmbnmnmnm', '<p>nmnmnmnm</p>\n', 0, 2, 'Pages'),
(5, 'iuouio', 'iuouio', '<p>uiouiouio</p>\n', 0, 2, 'Pages'),
(6, 'popppp', 'popppp', '<p>uiouiouiooppopopop</p>\n', 0, 2, 'Pages'),
(7, 'ghghgfh', 'ghghgfh', '<p>fghfghfghfgh</p>\n', 1, 2, 'Pages'),
(8, 'cxzcxc', 'cxzcxc', '<p>zxczxczxc</p>\n', 0, 2, 'Pages'),
(9, 'zxczxczxc', 'zxczxczxc', '<p>zxczxczxczxczxc</p>\n', 1, 2, 'Pages'),
(10, 'vbnvbnvbmvb', 'vbnvbnvbmvb', '<p>bnvnbvn</p>\n', 0, 2, 'Pages'),
(11, 'klklklkl', 'klklklkl', '<p>klklklkl</p>\n', 0, 2, 'Pages'),
(12, 'nhnhnhn', 'nhnhnhn', '<p>hnhnhnhnhn</p>\n', 1, 2, 'Pages'),
(13, 'vbnvbnbvn', 'vbnvbnbvn', '<p>vbncvnbn</p>\n', 0, 2, 'Pages'),
(14, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(15, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 0, 2, 'Pages'),
(16, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(17, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 0, 2, 'Pages'),
(18, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(19, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 0, 2, 'Pages'),
(20, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(21, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(22, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 0, 2, 'Pages'),
(23, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(24, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 0, 2, 'Pages'),
(25, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(26, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 0, 2, 'Pages'),
(27, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(28, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 0, 2, 'Pages'),
(29, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(30, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(31, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 0, 2, 'Pages'),
(32, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 0, 2, 'Pages'),
(33, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 0, 2, 'Pages'),
(34, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(35, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(36, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(37, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(38, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(39, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(40, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(41, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(42, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(43, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(44, 'cxvcxvbxcvb', 'cxvcxvbxcv', '<p>xvcbcvxbvcxb</p>\n', 1, 2, 'Pages'),
(45, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(46, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(47, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(48, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(49, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(50, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(51, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(52, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(53, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(54, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(55, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(56, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(57, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(58, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(59, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(60, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(61, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(62, 'asdasd', 'asdasd', '<p>asdasdasd</p>\n', 1, 2, 'Pages'),
(63, 'sampol', 'sampol', '<p>fsadgsdfgsdghsdhsdhsdhgsdfh</p>\n', 1, 1, 'Pages'),
(64, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(65, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(66, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(67, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(68, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(69, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(70, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(71, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(72, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(73, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(74, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(75, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(76, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(77, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(78, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(79, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(80, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(81, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(82, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(83, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(84, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(85, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(86, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(87, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(88, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(89, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(90, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(91, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(92, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(93, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(94, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(95, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(96, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(97, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(98, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(99, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(100, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(101, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(102, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(103, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(104, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(105, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(106, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(107, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(108, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(109, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(110, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(111, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(112, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(113, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(114, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(115, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(116, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(117, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(118, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(119, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(120, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(121, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(122, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(123, 'asdasd', 'asdasd', '<p>asdasd</p>\n', 1, 2, 'Pages'),
(124, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(125, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(126, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(127, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(128, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(129, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(130, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(131, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(132, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(133, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(134, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(135, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(136, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(137, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(138, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(139, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(140, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(141, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(142, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(143, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(144, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(145, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(146, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(147, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(148, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(149, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(150, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(151, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(152, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(153, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(154, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(155, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(156, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(157, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(158, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(159, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(160, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(161, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(162, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(163, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(164, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(165, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(166, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(167, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(168, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(169, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(170, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(171, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(172, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(173, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(174, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(175, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(176, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(177, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(178, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(179, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(180, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(181, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(182, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(183, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(184, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(185, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(186, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(187, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(188, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(189, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(190, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(191, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(192, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(193, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(194, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(195, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(196, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(197, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(198, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(199, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(200, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(201, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(202, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(203, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(204, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(205, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(206, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(207, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(208, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(209, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(210, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(211, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(212, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(213, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(214, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(215, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(216, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(217, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(218, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(219, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(220, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(221, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(222, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(223, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(224, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(225, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages'),
(226, 'sadasdasda', 'sadasdasd', '<p>sdasdasdasd</p>\n', 1, 2, 'Pages');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
 ADD PRIMARY KEY (`pageid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
MODIFY `pageid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=227;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
