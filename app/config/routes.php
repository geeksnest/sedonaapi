<?php

/**
 * @author Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.
 */
$routes[] = [
    'method' => 'get',
    'route' => '/tests',
    'handler' => ['Controllers\SpecialpageController', 'testAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/ping',
    'handler' => ['Controllers\ExampleController', 'pingAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/authenticate/get',
    'handler' => ['Controllers\AuthenticateController', 'getAccessTokenAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/test/{id}',
    'handler' => ['Controllers\ExampleController', 'testAction']
];

$routes[] = [
    'method' => 'post',
    'route' => '/skip/{name}',
    'handler' => ['Controllers\ExampleController', 'skipAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'post',
    'route' => '/map/marker/upload',
    'handler' => ['Controllers\MapController', 'uploadPicsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/map/create/info',
    'handler' => ['Controllers\MapController', 'saveMapMarkerAction'],
    'authentication' => FALSE
];

//booking router
$routes[] = [
    'method' => 'post',
    'route' => '/booking/send',
    'handler' => ['Controllers\BookingController', 'sendbookingAction'],
    'authentication' => FALSE
];


//rainier routers

//pages routers
$routes[] = [
    'method' => 'post',
    'route' => '/pages/saveimage',
    'handler' => ['Controllers\PagesController', 'saveimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/listimages',
    'handler' => ['Controllers\PagesController', 'listimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/get',
    'handler' => ['Controllers\PagesController', 'getAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/create',
    'handler' => ['Controllers\PagesController', 'createPagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/sortcontent',
    'handler' => ['Controllers\PagesController', 'sortAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/createpagecategory',
    'handler' => ['Controllers\PagesController', 'createpagecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/listpagecategory',
    'handler' => ['Controllers\PagesController', 'listpagecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/managepage/{num}/{off}/{keyword}/{sort}',
    'handler' => ['Controllers\PagesController', 'managepagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/managepage/aura/{num}/{off}/{keyword}/{sort}',
    'handler' => ['Controllers\PagesController', 'managepageauraAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/managepagecategory/{num}/{off}/{keyword}',
    'handler' => ['Controllers\PagesController', 'managePagesCategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/updatepagestatus/{status}/{pageid}/{keyword}',
    'handler' => ['Controllers\PagesController', 'pageUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/getpages',
    'handler' => ['Controllers\PagesController', 'getpagesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];


$routes[] = [
    'method' => 'get',
    'route' => '/page/pagedelete/{pageid}',
    'handler' => ['Controllers\PagesController', 'pagedeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/categorydelete/{catid}',
    'handler' => ['Controllers\PagesController', 'categorydeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/updatecategorynames/{catid}',
    'handler' => ['Controllers\PagesController', 'updatecategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/pageedit/{pageid}',
    'handler' => ['Controllers\PagesController', 'pageeditoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/pagerightitem/{pageid}',
    'handler' => ['Controllers\PagesController', 'pagerightitemAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/saveeditedpage',
    'handler' => ['Controllers\PagesController', 'saveeditedPagesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/getpage/{catslugs}/{pageslugs}',
    'handler' => ['Controllers\PagesController', 'getPageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/getpagecategory/{pageslugs}',
    'handler' => ['Controllers\PagesController', 'getPagecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/pages/deletepagesimg',
 'handler' => ['Controllers\PagesController', 'deletepagesimgAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];

$routes[] = [
 'method' => 'get',
 'route' => '/pages/loadcatpages/{catid}',
 'handler' => ['Controllers\PagesController', 'loadcatpagesAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/updatepagescategory',
    'handler' => ['Controllers\PagesController', 'updatepagescatAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];


//TESTEMONIALS FRONTEND
$routes[] = [
    'method' => 'get',
    'route' => '/page/gettestimonial/{catslugs}/{pageslugs}',
    'handler' => ['Controllers\TestimonialsController', 'gettestimonialAction'],
    'authentication' => FALSE
];


//MENU FRONTEND
$routes[] = [
    'method' => 'get',
    'route' => '/menu/{cat}',
    'handler' => ['Controllers\PagesController', 'listmenuAction'],
    'authentication' => FALSE
];

//news routers
$routes[] = [
    'method' => 'post',
    'route' => '/news/saveimage',
    'handler' => ['Controllers\NewsController', 'saveimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/savevideo',
    'handler' => ['Controllers\NewsController', 'featuredvideoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/news/saveauthorimage',
    'handler' => ['Controllers\NewsController', 'saveauthorimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/listimages',
    'handler' => ['Controllers\NewsController', 'listimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/listvideo',
    'handler' => ['Controllers\NewsController', 'listvideoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/authorlistimages',
    'handler' => ['Controllers\NewsController', 'authorlistimagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/listcategory',
    'handler' => ['Controllers\NewsController', 'listcategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/listtags',
    'handler' => ['Controllers\NewsController', 'listtagsAction'],
    'authentication' => FALSE
];

$routes[] = [
        'method' => 'get',
        'route' => '/tags/loadconflict/{id}',
        'handler' => ['Controllers\NewsController', 'tagconflictAction'],
        'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/listauthor',
    'handler' => ['Controllers\NewsController', 'listAuthorAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/displayvideo',
    'handler' => ['Controllers\NewsController', 'displayvideoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/create',
    'handler' => ['Controllers\NewsController', 'createNewsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/createauthor',
    'handler' => ['Controllers\NewsController', 'createAuthorAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/newscenter/create',
    'handler' => ['Controllers\NewsController', 'createNewsCenterAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/managenews/{num}/{off}/{keyword}/{sort}',
    'handler' => ['Controllers\NewsController', 'manageNewsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/manageauthor/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'manageAuthorAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenewsstatus/{status}/{newsid}/{keyword}',
    'handler' => ['Controllers\NewsController', 'newsUpdatestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenewscenterstatus/{status}/{newsid}/{keyword}',
    'handler' => ['Controllers\NewsController', 'newsCenterUpdatestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/newsdelete/{newsid}',
    'handler' => ['Controllers\NewsController', 'newsdeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/authordelete/{authorid}',
    'handler' => ['Controllers\NewsController', 'authordeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/newscenterdelete/{newsid}',
    'handler' => ['Controllers\NewsController', 'newscenterdeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/newsedit/{newsid}',
    'handler' => ['Controllers\NewsController', 'newseditoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/editauthor/{authorid}',
    'handler' => ['Controllers\NewsController', 'authoreditoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/newscenteredit/{newsid}',
    'handler' => ['Controllers\NewsController', 'newscentereditoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenews',
    'handler' => ['Controllers\NewsController', 'updateNewsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/edieauthor',
    'handler' => ['Controllers\NewsController', 'authorupdateAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/newscenter/edit',
    'handler' => ['Controllers\NewsController', 'editNewsCenterAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/managecategory/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'managecategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/managetags/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'managetagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/savecategory',
    'handler' => ['Controllers\NewsController', 'createcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/savetags',
    'handler' => ['Controllers\NewsController', 'createtagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/addvid',
    'handler' => ['Controllers\NewsController', 'savevideoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatecategorynames/{catname}/{id}',
    'handler' => ['Controllers\NewsController', 'updatecategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatetags/{tagname}/{id}',
    'handler' => ['Controllers\NewsController', 'updatetagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/categorydelete/{id}',
    'handler' => ['Controllers\NewsController', 'categorydeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/tagsdelete/{id}',
    'handler' => ['Controllers\NewsController', 'tagsdeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/listcategory',
 'handler' => ['Controllers\NewsController', 'listcategoryfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/featurednews',
 'handler' => ['Controllers\NewsController', 'featurednewsfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/latest',
 'handler' => ['Controllers\NewsController', 'latestnewsfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/founderswisdom',
 'handler' => ['Controllers\NewsController', 'founderswisdomfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/view/{newsslugs}',
 'handler' => ['Controllers\NewsController', 'viewfronentnewsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/listtags/{newsslugs}',
 'handler' => ['Controllers\NewsController', 'viewfronenttagsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/listnewsbycategory/{category}/{offset}/{page}',
 'handler' => ['Controllers\NewsController', 'listnewsbycategoryAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/listnewsbytag/{tag}/{offset}/{page}',
    'handler' => ['Controllers\NewsController', 'listnewsbytagAction'],
    'authentication' => FALSE
];


$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/listnews/{offset}/{page}',
 'handler' => ['Controllers\NewsController', 'listAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/author/{id}/{offset}/{page}',
    'handler' => ['Controllers\NewsController', 'authorAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/news/deletenewsimg',
 'handler' => ['Controllers\NewsController', 'deletenewsimgAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];

$routes[] = [
 'method' => 'post',
 'route' => '/news/deletevideo',
 'handler' => ['Controllers\NewsController', 'deletevideoAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];

$routes[] = [
 'method' => 'post',
 'route' => '/news/deleteauthorimg',
 'handler' => ['Controllers\NewsController', 'deleteauthorimgAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/loadconflict/{id}',
 'handler' => ['Controllers\NewsController', 'loadconflictsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/news/updateconflict',
 'handler' => ['Controllers\NewsController', 'updateconflictAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/tags/updateconflict',
    'handler' => ['Controllers\NewsController', 'updateTagConflictAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/news/updateconflict',
 'handler' => ['Controllers\NewsController', 'updateconflictAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/archives',
    'handler' => ['Controllers\NewsController', 'listarchiveAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/loadauthornews/{id}',
 'handler' => ['Controllers\NewsController', 'loadauthornewsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/news/updatenewsauthors',
 'handler' => ['Controllers\NewsController', 'updatenewsauthorsAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/listnewsbyarchive/{month}/{year}/{offset}/{page}',
    'handler' => ['Controllers\NewsController', 'listnewsbyarchiveAction'],
    'authentication' => FALSE
];
/** Kyben
 */
/* Validation for username exist */
$routes[] = [
    'method' => 'get',
    'route' => '/validate/username/{name}/{id}',
    'handler' => ['Controllers\UserController', 'userExistAction'],
    'authentication' => FALSE
];
/* Validation for email exist */
$routes[] = [
    'method' => 'get',
    'route' => '/validate/email/{name}/{id}',
    'handler' => ['Controllers\UserController', 'emailExistAction'],
    'authentication' => FALSE
];
/* Validation for old password */$routes[] = [
        'method' => 'get',
        'route' => '/validate/password/{id}/{password}',
        'handler' => ['Controllers\UserController', 'validatePasswordAction'],
        'authentication' => FALSE
];
/* Submit User Regester */
$routes[] = [
    'method' => 'post',
    'route' => '/user/register',
    'handler' => ['Controllers\UserController', 'registerUserAction'],
    'authentication' => FALSE
];
/* List all User */
$routes[] = [
    'method' => 'get',
    'route' => '/user/list/{num}/{off}/{keyword}/{id}',
    'handler' => ['Controllers\UserController', 'userListAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
/* User Info */
$routes[] = [
    'method' => 'get',
    'route' => '/user/info/{id}',
    'handler' => ['Controllers\UserController', 'userInfoction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/user/update',
    'handler' => ['Controllers\UserController', 'userUpdateAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/user/changepass',
    'handler' => ['Controllers\UserController', 'userChangepassAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
//change other user password
$routes[] = [
    'method' => 'post',
    'route' => '/user/changepassword',
    'handler' => ['Controllers\UserController', 'userChangepasswordAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
/* DELETE User */
$routes[] = [
    'method' => 'get',
    'route' => '/user/delete/{id}',
    'handler' => ['Controllers\UserController', 'deleteUserAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/user/activation',
    'handler' => ['Controllers\UserController', 'activationAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/user/login/{username}/{password}',
    'handler' => ['Controllers\UserController', 'loginAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/user/putaka',
    'handler' => ['Controllers\UserController', 'putaAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/forgotpassword/reset',
    'handler' => ['Controllers\UserController', 'resetpasswordAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/checktoken/check/{email}/{token}',
    'handler' => ['Controllers\UserController', 'changepasswordAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/forgotpassword/updatepassword',
    'handler' => ['Controllers\UserController', 'updatepasswordAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/user/updatestatus/{id}/{newstat}',
    'handler' => ['Controllers\UserController', 'updatestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
        'method' => 'get',
        'route' => '/user/delete/multiple/{id}',
        'handler' => ['Controllers\UserController', 'deleteUsersAction'],
        'authentication' => TRUE,
        'level' => 'admin'
];
// Testimonials
$routes[] = [
    'method' => 'get',
    'route' => '/testimonials/list/{num}/{off}/{keyword}',
    'handler' => ['Controllers\TestimonialsController', 'listAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/testimonials/create',
    'handler' => ['Controllers\TestimonialsController', 'createAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/testimonials/get/{id}',
    'handler' => ['Controllers\TestimonialsController', 'getAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/testimonials/update',
    'handler' => ['Controllers\TestimonialsController', 'updateAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/testimonials/delete/{id}',
    'handler' => ['Controllers\TestimonialsController', 'deleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
// End Testimonials

$routes[] = [
    'method' => 'get',
    'route' => '/booking/list/{num}/{off}/{keyword}/{searchdate}',
    'handler' => ['Controllers\BookingController', 'listAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/get/{id}',
    'handler' => ['Controllers\BookingController', 'getAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/read/{id}',
    'handler' => ['Controllers\BookingController', 'readAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/booking/reply',
    'handler' => ['Controllers\BookingController', 'replyAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/getreplies/{id}',
    'handler' => ['Controllers\BookingController', 'getrepliesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/delete/{id}',
    'handler' => ['Controllers\BookingController', 'deleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/booking/offline/reservation/add',
    'handler' => ['Controllers\BookingController', 'addreservationAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/schedule/validate/{stime}/{etime}',
    'handler' => ['Controllers\BookingController', 'validateAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/member/getbyemail/{email}',
    'handler' => ['Controllers\BookingController', 'getbyemailAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/invoice/get/{invoiceno}',
    'handler' => ['Controllers\BookingController', 'getinvoiceAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/getmonthevents/{month}/{year}',
    'handler' => ['Controllers\BookingController', 'getmontheventsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/booking/updateshedule',
    'handler' => ['Controllers\BookingController', 'updatesheduleAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/voidschedule/{id}/{status}/{message}',
    'handler' => ['Controllers\BookingController', 'updatestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/countingstatus/{currtime}',
    'handler' => ['Controllers\BookingController', 'countingstatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/listschedule/{num}/{off}/{keyword}/{day}/{month}/{year}/{status}/{currdate}',
    'handler' => ['Controllers\BookingController', 'listbookingAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];


$routes[] = [
    'method' => 'get',
    'route' => '/booking/salesreport/{num}/{off}/{start}/{end}/{status}/{service}/{subservice}/{prices}/{payment}/{members}',
    'handler' => ['Controllers\ReportsController', 'salesreportAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/salesreportdummy',
    'handler' => ['Controllers\ReportsController', 'salesreportdummyAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/searchreportdummy/{start}/{end}/{status}/{service}/{subservice}/{prices}/{payment}/{members}',
    'handler' => ['Controllers\ReportsController', 'searchreportdummyAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

//memberreport
$routes[] = [
    'method' => 'get',
    'route' => '/booking/memberreport/{num}/{off}/{start}/{end}/{prices}/{payment}/{members}',
    'handler' => ['Controllers\ReportsController', 'memberreportAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/memberreportdummy/{start}/{end}/{status}/{payment}/{members}',
    'handler' => ['Controllers\ReportsController', 'memberreportdummyAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/totsum',
    'handler' => ['Controllers\ReportsController', 'totsumAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/expected',
    'handler' => ['Controllers\ReportsController', 'expectedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/servicelist',
    'handler' => ['Controllers\ReportsController', 'servicelistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/subservice/{serv}',
    'handler' => ['Controllers\ReportsController', 'subserviceAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/pricelist/{subserv}',
    'handler' => ['Controllers\ReportsController', 'pricelistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/memberslist',
    'handler' => ['Controllers\ReportsController', 'memberslistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

//Sales Chart
$routes[] = [
    'method' => 'get',
    'route' => '/booking/reservestats',
    'handler' => ['Controllers\ReportsController', 'reservestatsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/reserve_statservice',
    'handler' => ['Controllers\ReportsController', 'reserve_statserviceAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/Sales_Reservation',
    'handler' => ['Controllers\ReportsController', 'Sales_ReservationAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/MonthlyReservation',
    'handler' => ['Controllers\ReportsController', 'MonthlyReservationAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/MonthlySchedule',
    'handler' => ['Controllers\ReportsController', 'MonthlyScheduleAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/UserStat',
    'handler' => ['Controllers\ReportsController', 'UserStatAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];


//Member Charts!
$routes[] = [
    'method' => 'get',
    'route' => '/booking/TopUser/{num}/{off}',
    'handler' => ['Controllers\ReportsController', 'TopUserAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/OverallUserStat',
    'handler' => ['Controllers\ReportsController', 'OverallUserStatAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/TopService',
    'handler' => ['Controllers\ReportsController', 'TopServiceAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/TopSubservice',
    'handler' => ['Controllers\ReportsController', 'TopSubserviceAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/OverallUser',
    'handler' => ['Controllers\ReportsController', 'OverallUserAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

//test taker module
$routes[] = [
    'method' => 'get',
    'route' => '/test/max',
    'handler' => ['Controllers\TestController', 'maxAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/test/savetest',
    'handler' => ['Controllers\TestController', 'savetestAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/test/saveresult',
    'handler' => ['Controllers\TestController', 'saveresultAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/test/view/{num}/{off}/{keyword}',
    'handler' => ['Controllers\TestController', 'testnameAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/test/testmemberlist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\TestController', 'testmemberlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/test/viewtest/{testid}',
    'handler' => ['Controllers\TestController', 'viewtestAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/test/viewQuestion/{q_id}',
    'handler' => ['Controllers\TestController', 'viewQuestionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/test/viewTestname/{testid}',
    'handler' => ['Controllers\TestController', 'viewTestnameAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/test/EditQuestion',
    'handler' => ['Controllers\TestController', 'EditQuestionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/test/AddEditQ',
    'handler' => ['Controllers\TestController', 'AddEditQAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/test/AddEditResult',
    'handler' => ['Controllers\TestController', 'AddEditResultAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/test/AddEditRec',
    'handler' => ['Controllers\TestController', 'AddEditRecAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/test/AddEditProd',
    'handler' => ['Controllers\TestController', 'AddEditProdAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/test/EditTestname',
    'handler' => ['Controllers\TestController', 'EditTestnameAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/test/DeleteQuestion/{q_id}',
    'handler' => ['Controllers\TestController', 'DeleteQuestionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/test/DeleteResult/{id}/{choice}/{testid}',
    'handler' => ['Controllers\TestController', 'DeleteResultAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/test/DeleteRec/{id}',
    'handler' => ['Controllers\TestController', 'DeleteRecAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/test/DeleteProd/{id}',
    'handler' => ['Controllers\TestController', 'DeleteProdAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/test/DeleteTest/{testid}',
    'handler' => ['Controllers\TestController', 'DeleteTestAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/test/updatestatus/{testid}/{newstat}',
    'handler' => ['Controllers\TestController', 'updatestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/test/viewResult/{testid}',
    'handler' => ['Controllers\TestController', 'viewResultAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/test/viewServices/{id}',
    'handler' => ['Controllers\TestController', 'viewServicesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/test/EditResult',
    'handler' => ['Controllers\TestController', 'EditResultAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/test/EditServices',
    'handler' => ['Controllers\TestController', 'EditServicesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/test/EditProducts',
    'handler' => ['Controllers\TestController', 'EditProductsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/test/arrange',
    'handler' => ['Controllers\TestController', 'arrangeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/test/DeleteMem/{id}',
    'handler' => ['Controllers\TestController', 'DeleteMemAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];


// !Menu Creator! List@@!
$routes[] = [
    'method' => 'get',
    'route' => '/menu/menulist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\MenuController', 'menulistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !Menu Creator! Sub Menu List@@!
$routes[] = [
    'method' => 'get',
    'route' => '/menu/submenulist/{menuID}',
    'handler' => ['Controllers\MenuController', 'submenulistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];


// !Menu Creator! Sub Menu Name@@!
$routes[] = [
    'method' => 'get',
    'route' => '/menu/submenuname/{submenuID}',
    'handler' => ['Controllers\MenuController', 'submenunameAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !Menu Creator! Add Menu@@!
$routes[] = [
    'method' => 'post',
    'route' => '/menu/addmenu',
    'handler' => ['Controllers\MenuController', 'menuAddAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !Menu Creator! Save Menu@@!
$routes[] = [
    'method' => 'post',
    'route' => '/menu/savemenu/{menu}/{menuID}/{name}',
    'handler' => ['Controllers\MenuController', 'savemenuAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !Menu Creator! Add Sub Menu@@!
$routes[] = [
    'method' => 'post',
    'route' => '/menu/subaddmenu',
    'handler' => ['Controllers\MenuController', 'submenuAddAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !Menu Creator! Update Sub Menu@@!
$routes[] = [
    'method' => 'post',
    'route' => '/menu/updatesubmenu',
    'handler' => ['Controllers\MenuController', 'updatesubmenuAddAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !Menu Creator! Add Sub Menu@@!
$routes[] = [
    'method' => 'post',
    'route' => '/menu/addsubmenu',
    'handler' => ['Controllers\MenuController', 'addsubmenuAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !Menu Creator! Delete Menu@@!
$routes[] = [
    'method' => 'post',
    'route' => '/menu/subdelete/{submenuID}',
    'handler' => ['Controllers\MenuController', 'submenuDeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !Menu Creator! Delete Sub Menu@@!
$routes[] = [
    'method' => 'post',
    'route' => '/menu/delete/{menuID}',
    'handler' => ['Controllers\MenuController', 'menuDeleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];


// !Menu Creator! Page List@@!
$routes[] = [
    'method' => 'get',
    'route' => '/menu/pagelist',
    'handler' => ['Controllers\MenuController', 'pagelistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !Menu Creator! Category List@@!
$routes[] = [
    'method' => 'get',
    'route' => '/menu/categorylist',
    'handler' => ['Controllers\MenuController', 'categorylistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !Menu Creator! Tags List@@!
$routes[] = [
    'method' => 'get',
    'route' => '/menu/taglist',
    'handler' => ['Controllers\MenuController', 'taglistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !Menu Creator! Post List@@!
$routes[] = [
    'method' => 'get',
    'route' => '/menu/postlist',
    'handler' => ['Controllers\MenuController', 'postlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !Menu Creator! Aura List@@!
$routes[] = [
    'method' => 'get',
    'route' => '/menu/auralist',
    'handler' => ['Controllers\MenuController', 'auralistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// !FRONTEND! View Menu@@!
$routes[] = [
    'method' => 'get',
    'route' => '/menu/viewfrontendmenu/{shortCode}',
    'handler' => ['Controllers\MenuController', 'viewfrontendmenuAction'],
    'authentication' => FALSE
];


//DASHBOARD!
$routes[] = [
    'method' => 'get',
    'route' => '/booking/DailySchedule',
    'handler' => ['Controllers\ReportsController', 'DailyScheduleAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/TotalNews',
    'handler' => ['Controllers\ReportsController', 'TotalNewsAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/DailyReservation',
    'handler' => ['Controllers\ReportsController', 'DailyReservationAction'],
    'authentication' => FALSE
];



// !Slider! Add Album @@!
$routes[] = [
    'method' => 'post',
    'route' => '/slider/addAlbum',
    'handler' => ['Controllers\AlbumController', 'addAlbumAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
//!l0@d @lbum!
$routes[] = [
    'method' => 'get',
    'route' => '/slider/albumslist/{all}',
    'handler' => ['Controllers\AlbumController', 'albumslistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

//!l0@d @lbum c@tegory!
$routes[] = [
    'method' => 'get',
    'route' => '/slider/category',
    'handler' => ['Controllers\AlbumController', 'categorylistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];


//!s3t M@in sl!d3r!
$routes[] = [
    'method' => 'get',
    'route' => '/slider/setMainSlider/{album_id}/{category}',
    'handler' => ['Controllers\AlbumController', 'setMainSliderAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
//load edit @lbum!
$routes[] = [
    'method' => 'get',
    'route' => '/slider/albumsimages/{albumid}',
    'handler' => ['Controllers\AlbumController', 'albumsimagesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
//Edit Album Image
$routes[] = [
    'method' => 'get',
    'route' => '/slider/editImage/{imgID}',
    'handler' => ['Controllers\AlbumController', 'editImageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
//Edit Update Image
$routes[] = [
    'method' => 'post',
    'route' => '/slider/updateImage',
    'handler' => ['Controllers\AlbumController', 'updateImageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
//Delete Image
$routes[] = [
    'method' => 'get',
    'route' => '/slider/deleteImage/{imgID}',
    'handler' => ['Controllers\AlbumController', 'deleteImageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
//Setstatus
$routes[] = [
    'method' => 'get',
    'route' => '/slider/setstatus/{imgID}/{status}',
    'handler' => ['Controllers\AlbumController', 'setstatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

//Sort Slider Images
$routes[] = [
    'method' => 'get',
    'route' => '/slider/sortSlider/{imgID}/{sort}/{album_id}',
    'handler' => ['Controllers\AlbumController', 'sortSliderAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
//Add Album Item
$routes[] = [
    'method' => 'post',
    'route' => '/slider/addAlbumItem',
    'handler' => ['Controllers\AlbumController', 'addAlbumItemAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
//update photo slider
$routes[] = [
    'method' => 'post',
    'route' => '/slider/updateAlbumItem',
    'handler' => ['Controllers\AlbumController', 'updateAlbumItemAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
//Delete Album
$routes[] = [
    'method' => 'get',
    'route' => '/slider/deleteAlbum/{album_id}',
    'handler' => ['Controllers\AlbumController', 'deleteAlbumAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];


// Prices
$routes[] = [
    'method' => 'post',
    'route' => '/service/price/add',
    'handler' => ['Controllers\PagesController', 'addpriceAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// front-end test module
$routes[] = [
    'method' => 'get',
    'route' => '/test/testtaker',
    'handler' => ['Controllers\TestController', 'testtakerAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/test/testname',
    'handler' => ['Controllers\TestController', 'testnamefrontAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/test/testitems/{testid}',
    'handler' => ['Controllers\TestController', 'testitemsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/test/displayresult',
    'handler' => ['Controllers\TestController', 'displayresultAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/test/testmember',
    'handler' => ['Controllers\TestController', 'sendresultAction'],
    'authentication' => FALSE
];

//front end sl!d3R!
$routes[] = [
    'method' => 'get',
    'route' => '/utility/mainslides',
    'handler' => ['Controllers\AlbumController', 'mainslideAction'],
    'authentication' => FALSE
];


//dynamic content for all categories
$routes[] = [
    'method' => 'get',
    'route' => '/utility/catcontent/{pageslugs}',
    'handler' => ['Controllers\PagesController', 'catcontentAction'],
    'authentication' => FALSE
];


//Healing Slide
$routes[] = [
    'method' => 'get',
    'route' => '/utility/healingslides',
    'handler' => ['Controllers\AlbumController', 'healingslideAction'],
    'authentication' => FALSE
];

//Reading Slide
$routes[] = [
    'method' => 'get',
    'route' => '/utility/readingslides',
    'handler' => ['Controllers\AlbumController', 'readingslideAction'],
    'authentication' => FALSE
];
//acupuncture Slide
$routes[] = [
    'method' => 'get',
    'route' => '/utility/acupunctureslides',
    'handler' => ['Controllers\AlbumController', 'acupunctureslideAction'],
    'authentication' => FALSE
];
//retreat Slide
$routes[] = [
    'method' => 'get',
    'route' => '/utility/retreatslides',
    'handler' => ['Controllers\AlbumController', 'retreatslideAction'],
    'authentication' => FALSE
];
//workshop Slide
$routes[] = [
    'method' => 'get',
    'route' => '/utility/workshopslides',
    'handler' => ['Controllers\AlbumController', 'workshopslideAction'],
    'authentication' => FALSE
];

//Specialpage@@@@
$routes[] = [
    'method' => 'get',
    'route' => '/utility/normalpage/{pageslugs}',
    'handler' => ['Controllers\SpecialpageController', 'normalpageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/utility/specialpage/{pageslugs}',
    'handler' => ['Controllers\SpecialpageController', 'specialpageAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/utility/special/{pageslugs}',
    'handler' => ['Controllers\SpecialpageController', 'specialAction'],
    'authentication' => FALSE
];



// Settings
    /*google analytics*/
$routes[] = [
    'method' => 'get',
    'route' => '/settings/load',
    'handler' => ['Controllers\SettingsController', 'loadsettingsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/update/googlescript',
    'handler' => ['Controllers\SettingsController', 'updatescriptAction'],
    'authentication' => FALSE
];

    /*maintenance*/
$routes[] = [
    'method' => 'post',
    'route' => '/settings/save/maintenance',
    'handler' => ['Controllers\SettingsController', 'savemaintenanceAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/off/maintenance',
    'handler' => ['Controllers\SettingsController', 'offmaintenanceAction'],
    'authentication' => FALSE
];

/*Contact us module*/
$routes[] = [
    'method' => 'post',
    'route' => '/contactus/save',
    'handler' => ['Controllers\ContactusController', 'sendAction'],
    'authentication' => FALSE
];
$routes[] = [
        'method' => 'get',
        'route' => '/contactus/list/{num}/{off}/{keyword}/{searchdate}',
        'handler' => ['Controllers\ContactusController', 'listAction'],
        'authentication' => FALSE
];
$routes[] = [
        'method' => 'get',
        'route' => '/contactus/delete/{id}',
        'handler' => ['Controllers\ContactusController', 'deleteAction'],
        'authentication' => FALSE
];
$routes[] = [
        'method' => 'get',
        'route' => '/contactus/get/{id}',
        'handler' => ['Controllers\ContactusController', 'getAction'],
        'authentication' => FALSE
];
$routes[] = [
        'method' => 'get',
        'route' => '/contactus/read/{id}',
        'handler' => ['Controllers\ContactusController', 'readAction'],
        'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/contactus/getreplies/{id}',
    'handler' => ['Controllers\ContactusController', 'getrepliesAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/contactus/reply',
    'handler' => ['Controllers\ContactusController', 'replyAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/contactdetails/update',
    'handler' => ['Controllers\ContactusController', 'updateAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/contactdetails/getinfo',
    'handler' => ['Controllers\ContactusController', 'getinfoAction'],
    'authentication' => FALSE
];

/* Routes for frontend Reservation Registration */

$routes[] = [
    'method' => 'post',
    'route' => '/booking/registration',
    'handler' => ['Controllers\BookingController', 'registrationAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/confirmation/{value}',
    'handler' => ['Controllers\BookingController', 'confirmationAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/booking/login',
    'handler' => ['Controllers\BookingController', 'loginAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/booking/updatepayment',
    'handler' => ['Controllers\BookingController', 'updatepaymentAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/booking/onlinereservation',
    'handler' => ['Controllers\BookingController', 'onlinereservationAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/booking/forgotpassword',
    'handler' => ['Controllers\BookingController', 'forgotpasswordAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/booking/checktoken',
    'handler' => ['Controllers\BookingController', 'checktokenAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/booking/changepassword',
    'handler' => ['Controllers\BookingController', 'changepasswordAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/booking/resendconfirmation',
    'handler' => ['Controllers\BookingController', 'resendconfirmationAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/booking/member/sendcompletion/{email}',
    'handler' => ['Controllers\BookingController', 'usercompletionAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/booking/member/checkcompletion',
    'handler' => ['Controllers\BookingController', 'getprofilecompletiongAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/booking/member/completion',
    'handler' => ['Controllers\BookingController', 'setprofilecompletiongAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/booking/member/update',
    'handler' => ['Controllers\BookingController', 'profileupdataAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/booking/member/updatepassword',
    'handler' => ['Controllers\BookingController', 'updatepasswordAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/booking/member/reservations/{id}',
    'handler' => ['Controllers\BookingController', 'memberreservationsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/homepage/createcontent',
    'handler' => ['Controllers\HomepageController', 'createAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
        'method' => 'get',
        'route' => '/homepage/list/{num}/{off}/{keyword}',
        'handler' => ['Controllers\HomepageController', 'listAction'],
        'authentication' => TRUE,
        'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/homepage/sortcontent',
    'handler' => ['Controllers\HomepageController', 'sortAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/homepage/delete/{id}',
    'handler' => ['Controllers\HomepageController', 'deleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/homepage/info/{id}',
    'handler' => ['Controllers\HomepageController', 'Infoction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/homepage/update',
    'handler' => ['Controllers\HomepageController', 'UpdateAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/homepage/showall',
    'handler' => ['Controllers\HomepageController', 'showallAction'],
    'authentication' => FALSE
];

//USER IMAGE UPLOAD AND LIST
$routes[] = [
    'method' => 'post',
    'route' => '/panel/saveimage',
    'handler' => ['Controllers\HomepageController', 'saveimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/panel/listimages',
    'handler' => ['Controllers\HomepageController', 'listimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/panel/deleteimage/{id}',
    'handler' => ['Controllers\HomepageController', 'deleteimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/homepage/categories',
    'handler' => ['Controllers\HomepageController', 'categoriesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/homepage/services',
    'handler' => ['Controllers\HomepageController', 'servicesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/menu/aura',
    'handler' => ['Controllers\PagesController', 'menuauraAction'],
    'authentication' => FALSE
];

///////////SPECIAL PAGE -> WEW
$routes[] = [
    'method' => 'get',
    'route' => '/specialpage/uniquepage/{slugs}',
    'handler' => ['Controllers\SpecialpageController', 'getPageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/specialpage/save',
    'handler' => ['Controllers\SpecialpageController', 'saveAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/specialpage/update',
    'handler' => ['Controllers\SpecialpageController', 'updateAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/specialpage/updatestatus/{status}/{pageid}/{keyword}',
    'handler' => ['Controllers\SpecialpageController', 'updatestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/specialpage/list/{num}/{off}/{keyword}',
    'handler' => ['Controllers\SpecialpageController', 'listAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/specialpage/delete/{id}',
    'handler' => ['Controllers\SpecialpageController', 'deleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/specialpage/info/{id}',
    'handler' => ['Controllers\SpecialpageController', 'infoAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/specialpage/getmenu',
    'handler' => ['Controllers\SpecialpageController', 'getmenuAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/special/loadtesti',
    'handler' => ['Controllers\SpecialpageController', 'loadtestiAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/special/loadnews',
    'handler' => ['Controllers\SpecialpageController', 'loadnewsAction'],
    'authentication' => FALSE
];

//rainier routes
$routes[] = [
    'method' => 'post',
    'route' => '/feuser/registeruser',
    'handler' => ['Controllers\FeuserController', 'registeruserAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/feuser/checkfacebook',
    'handler' => ['Controllers\FeuserController', 'checkfacebookAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/feuser/login',
    'handler' => ['Controllers\FeuserController', 'loginAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/feuser/logout',
    'handler' => ['Controllers\FeuserController', 'logoutAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// ecommerce frontend
$routes[] = [
    'method' => 'get',
    'route' => '/products/fe/list/{page}/{limit}/{sort}/{keyword}',
    'handler' => ['Controllers\ProductController', 'fe_listofproductsAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/shop/addtocart',
    'handler' => ['Controllers\OrderController', 'addtocartAction'],
    'authentication' => FALSE
];
// $routes[] = [
//         'method' => "post",
//         "route" => "/mycart/fe",
//         "handler" => ['Controllers\ProductController', 'fe_mycartAction'],
//         "authentication" => FALSE
// ];
$routes[] = [
        'method' => "get",
        "route" => "/fe/product/{slugs}",
        "handler" => ['Controllers\ProductController', 'fe_productAction'],
        "authentication" => FALSE
];
$routes[] = [
        'method' => "post",
        "route" => "/checkout/placeorder/{id}",
        "handler" => ['Controllers\OrderController', 'placeorderAction'],
        "authentication" => TRUE,
        "level" => "member"
];
$routes[] = [
    "method" => "post",
    "route" => "/shop/removeitem/{productid}/{memberid}",
    "handler" => ['Controllers\OrderController', 'removeitemAction'],
    "authentication" => TRUE,
    "level" => 'member'
];
$routes[] = [
    "method" => "post",
    "route" => '/shop/removeallitem/{memberid}',
    "handler" => ['Controllers\OrderController', 'removeallitemAction'],
    "authentication" => TRUE,
    "level" => "member"
];
$routes[] = [
    "method" => "post",
    "route" => "/shop/updatequantity/{id}/{quantity}",
    "handler" => ["Controllers\OrderController", "updatebagAction"],
    "authentication" => TRUE,
    "level" => "member"
];
$routes[] = [
    "method" => "get",
    "route" => "/feuser/checkemail/{email}",
    "handler" => ["Controllers\OrderController", "checkmailAction"],
    "authentication" => FALSE
];


//SHORTCODE
$routes[] = [
        'method' => "get",
        "route" => "/shortcode/testimonials/{column}/{keyword}/{limit}",
        "handler" => ['Controllers\ShortcodeController', 'testimonialAction'],
        "authentication" => FALSE
];

$routes[] = [
        'method' => "get",
        "route" => "/shortcode/news/{column}/{keyword}/{limit}",
        "handler" => ['Controllers\ShortcodeController', 'newsAction'],
        "authentication" => FALSE
];

//FOoTeR
$routes[] = [
        'method' => "post",
        "route" => "/special/footer/save",
        "handler" => ['Controllers\FooterController', 'saveAction'],
        "authentication" => FALSE
];

$routes[] = [
        'method' => "get",
        "route" => "/special/footer/info",
        "handler" => ['Controllers\FooterController', 'infoAction'],
        "authentication" => FALSE
];
$routes[] = [
        'method' => "get",
        "route" => "/utility/footer",
        "handler" => ['Controllers\FooterController', 'footerAction'],
        "authentication" => FALSE
];

// $routes[] = [
//         'method' => "get",
//         "route" => "/special/footer/frontend",
//         "handler" => ['Controllers\FooterController', 'footerpageAction'],
//         "authentication" => FALSE
// ];

//jeevon
//product backend
$routes[] = [
    'method' => 'post',
    'route' => '/add/product',
    'handler' => ['Controllers\ProductController', 'addproductAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadimages/{id}',
    'handler' => ['Controllers\ProductController', 'loadimagesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/product/saveimage/{id}',
    'handler' => ['Controllers\ProductController', 'saveimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/deleteimage/{id}',
    'handler' => ['Controllers\ProductController', 'deleteimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadaddproduct',
    'handler' => ['Controllers\ProductController', 'loadproductAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/load/{data}',
    'handler' => ['Controllers\ProductController', 'autocompleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/product/loadlist/{num}/{off}/{keyword}/{find}',
    'handler' => ['Controllers\ProductController', 'getlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/validatecode/{id}/{name}',
    'handler' => ['Controllers\ProductController', 'validatepcodeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/product/validatename/{id}/{name}',
    'handler' => ['Controllers\ProductController', 'validatenameAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/delete/{id}',
    'handler' => ['Controllers\ProductController', 'deleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadedit/{id}',
    'handler' => ['Controllers\ProductController', 'loadeditAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route'=> '/product/edit',
    'handler' => ['Controllers\ProductController', 'editAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/updatestatus/{id}/{status}',
    'handler' => ['Controllers\ProductController', 'updatestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/validateslug/{id}/{slugs}',
    'handler' => ["Controllers\ProductController", 'validateslugAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/addquantity/{id}',
    'handler' => ['Controllers\ProductController', 'addquantityAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/adddiscount/{id}',
    'handler' => ['Controllers\ProductController', 'adddiscountAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/viewproduct/{id}',
    'handler' => ['Controllers\ProductController', 'viewproductAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/viewdetails/{id}/{type}/{page}',
    'handler' => ['Controllers\ProductController', 'viewdetailsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/selectimage',
    'handler' => ['Controllers\ProductController', 'selectimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadcategorylist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\ProductController', 'loadcategorylistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/validatecategory/{id}/{category}',
    'handler' => ['Controllers\ProductController', 'validatecategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadsubcateogry',
    'handler' => ['Controllers\ProductController', 'loadsubcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/saveCategory',
    'handler' => ['Controllers\ProductController', 'savecategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/deleteCategory/{id}',
    'handler' => ['Controllers\ProductController', 'deletecategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadeditcategory/{id}',
    'handler' => ['Controllers\ProductController', 'loadeditcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/editcategory',
    'handler' => ['Controllers\ProductController', 'editcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadtags/{num}/{off}/{keyword}',
    'handler' => ['Controllers\ProductController', 'loadtagslistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/deletetags/{id}',
    'handler' => ['Controllers\ProductController', 'deletetagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/validatetags/{id}/{tags}',
    'handler' => ['Controllers\ProductController', 'validatetagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/saveTag',
    'handler' => ['Controllers\ProductController', 'savetagAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/updatetags',
    'handler' => ['Controllers\ProductController', 'updatetagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadsubcategory/{num}/{off}/{keyword}',
    'handler' => ['Controllers\ProductController', 'loadsubcatAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/validatesubcategory/{id}/{subcategory}',
    'handler' => ['Controllers\ProductController', 'validatesubcatAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadtypes',
    'handler' => ['Controllers\ProductController', 'loadtypesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/addsubcategory',
    'handler' => ['Controllers\ProductController', 'addsubcatAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/deletesubcategory/{id}',
    'handler' => ['Controllers\ProductController', 'deletesubcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadeditsubcategory/{id}',
    'handler' => ['Controllers\ProductController', 'loadeditsubcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/updatesubcategory',
    'handler' => ['Controllers\ProductController', 'updatesubcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadtypes/{num}/{off}/{keyword}',
    'handler' => ['Controllers\ProductController', 'loadtypelistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/deletetype/{id}',
    'handler' => ['Controllers\ProductController', 'deletetypeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/updatetype',
    'handler' => ['Controllers\ProductController', 'updatetypeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' =>'/product/validatetype/{type}',
    'handler' => ['Controllers\ProductController', 'validatetypeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/savetype',
    'handler' => ['Controllers\ProductController', 'savetypeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/getsubcat',
    'handler' => ['Controllers\ProductController', 'getsubcatAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/gettypes',
    'handler' => ['Controllers\ProductController', 'gettypesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/getReport/{id}',
    'handler' => ['Controllers\ProductController', 'getreportAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadtagssize',
    'handler' => ['Controllers\ProductController', 'loadtagssizeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadsize/{num}/{off}/{keyword}',
    'handler' => ['Controllers\ProductController', 'loadsizeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/deletesize/{id}',
    'handler' => ['Controllers\ProductController', 'deletesizeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/updatesize',
    'handler' => ['Controllers\ProductController', 'updatesizeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/getproductreport',
    'handler' => ['Controllers\ProductController', 'productreportAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

/*backend - ORDER*/
$routes[] = [
    'method' => 'get',
    'route' => '/order/getproducts',
    'handler' => ['Controllers\OrderController', 'getproductsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/order/getaccountinfo/{email}',
    'handler' => ['Controllers\OrderController', 'getaccountinfAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/order/offline/add',
    'handler' => ['Controllers\OrderController', 'offlineorderAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/order/list/{num}/{off}/{keyword}/{filter}',
    'handler' => ['Controllers\OrderController', 'orderlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/order/loadreportlist/{num}/{off}/{startdate}/{enddate}/{status}/{payment}/{members}',
    'handler' => ['Controllers\OrderReportsController', 'orderlistAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/order/get/{id}',
    'handler' => ['Controllers\OrderController', 'getorderAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/order/comment/add',
    'handler' => ['Controllers\OrderController', 'addcommentAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/order/updatestatus',
    'handler' => ['Controllers\OrderController', 'updatestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/order/getreport',
    'handler' => ['Controllers\OrderController', 'getreportAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

//order charts
$routes[] = [
    'method' => 'get',
    'route' => '/order/TopMonthly',
    'handler' => ['Controllers\OrderReportsController', 'TopMonthlyAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/order/MonthlySales',
    'handler' => ['Controllers\OrderReportsController', 'MonthlySalesAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/order/DailyOrders',
    'handler' => ['Controllers\OrderReportsController', 'DailyOrdersAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/order/MonthlyOrders',
    'handler' => ['Controllers\OrderReportsController', 'MonthlyOrdersAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/order/TopProducts',
    'handler' => ['Controllers\OrderReportsController', 'TopProductsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/feuser/checkout/login',
    'handler' => ['Controllers\FeuserController', 'checkoutloginAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/shop/savepaypaltemp',
    'handler' => ['Controllers\OrderController', 'savepaypaltempAction'],
    'authentication' => TRUE,
    'level' => 'member'
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/paypal/paypalreturn',
    'handler' => ['Controllers\PaypalController', 'paypalreturnAction'],
    'authentication' => FALSE
];

return $routes;
