<?php

namespace Controllers;

use \Models\Billinginfo as Billinginfo;
use \Models\Ordertemp as Ordertemp;
use \Models\Orderlist as Orderlist;
use \Models\Shippinginfo as Shippinginfo;
use \Models\Members as Members;
use \Models\Orderprodtemp as Orderprodtemp;
use \Models\Orderprod as Orderprod;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class PaypalController extends \Phalcon\Mvc\Controller {

    public function paypalreturnAction(){
        // $request = new \Phalcon\Http\Request();
        // $temptransactionid = $request->getPost('id');
        // PaypalController::shopsaving($temptransactionid, 'Paypal');
        // die();
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
          $keyval = explode ('=', $keyval);
          if (count($keyval) == 2)
             $myPost[$keyval[0]] = urldecode($keyval[1]);
        }

        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')) {
           $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            }
            else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }


        $ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
        // $ch = curl_init('https://www.paypal.com/cgi-bin/webscr');

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

        if( !($res = curl_exec($ch)) ) {
            curl_close($ch);
            exit;
        }
        curl_close($ch);

        $data = explode('|', $_POST['custom']);
        $moduletype = $data[0];
        $temptransactionid = $data[1];
        $txn_id = $_POST['txn_id'];
        $payment_status = $_POST['payment_status'];
        $parent_txn_id = $_POST['parent_txn_id'];

        if (strcmp ($res, "VERIFIED") == 0){
          var_dump($temptransactionid);
          if($moduletype == 'shop'){
            PaypalController::shopsaving($temptransactionid, 'Paypal');
          }

        } else if (strcmp ($res, "INVALID") == 0) {
            echo "The response from IPN was: <b>" .$res ."</b>";
        }
    }

    public function shopsaving($transactionid, $paymenttype) {
        $ordert = Ordertemp::findFirst("invoiceno='$transactionid' AND status=0");
        if($ordert){
            $b = Billinginfo::findFirst("memberid='" . $ordert->memberid . "'");
            if($b){
                try {
                    $transactionManager = new TransactionManager();
                    $transaction = $transactionManager->get();

                    $order = new Orderlist();
                    $guid = new \Utilities\Guid\Guid();
                    $order->setTransaction($transaction);
                    $order->assign(array(
                        'id' => $guid->GUID(),
                        'invoiceno' => $transactionid,
                        'memberid' => $b->memberid,
                        'amount' => $ordert->amount,
                        'paymenttype' => $ordert->paymenttype,
                        'status' => 'pending',
                        'created_at' => date("Y-m-d H:i:s"),
                        'firstname' => $b->firstname,
                        'lastname' => $b->lastname,
                        'address' => $b->address,
                        'address2' => $b->address2,
                        'city' => $b->city,
                        'country' => $b->country,
                        'state' => $b->state,
                        'zipcode' => $b->zipcode,
                        'phoneno' => $b->phoneno ));

                    if($order->save() == false){
                        $data = [];
                        foreach ($order->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    $sh = new Shippinginfo();
                    $guid = new \Utilities\Guid\Guid();
                    $sh->assign(array(
                        'id' => $guid->GUID(),
                        'orderid' => $order->id,
                        'firstname' => $ordert->firstname,
                        'lastname' => $ordert->lastname,
                        'address' => $ordert->address,
                        'address2' => $ordert->address2,
                        'city' => $ordert->city,
                        'country' => $ordert->country,
                        'state' => $ordert->state,
                        'zipcode' => $ordert->zipcode,
                        'phoneno' => $ordert->phoneno ));

                    if($sh->save() == false){
                        $data = [];
                        foreach ($sh->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    //set temporary order
                    $ordert->setTransaction($transaction);
                    $ordert->status = 1;
                    if($ordert->save() == false){
                        $data = [];
                        foreach ($ordert->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    //save orderlist
                    $orderprodt = Orderprodtemp::find("orderid='$transactionid'");
                    if($orderprodt){
                        foreach ($orderprodt->toArray() as $key => $value) {
                            $guid = new \Utilities\Guid\Guid();
                            $orderprod = new Orderprod();
                            $orderprod->setTransaction($transaction);
                            $orderprod->assign(array(
                                'id' => $guid->GUID(),
                                'orderid' => $order->id,
                                'productid' => $value['productid'],
                                'quantity' => $value['quantity'] ));
                            if($orderprod->save() == false){
                                $data = [];
                                foreach ($orderprod->getMessages() as $message) {
                                    $data[] = $message;
                                }
                                $transaction->rollback();
                            }

                            $ot = Orderprodtemp::findFirst("id='" . $value['id'] . "'");
                            $ot->setTransaction($transaction);
                            if($ot->delete() == false){
                                $data = [];
                                foreach ($ot->getMessages() as $message) {
                                    $data[] = $message;
                                }
                                $transaction->rollback();
                            }
                        }
                    }

                    $transaction->commit();

                    $body = "<p>Order Information</p>";
                    $body .= "<p>Invoice no. ".$transactionid."</p>";
                    $body .= "<p>Total amount : " . $order->amount . "</p>";
                    $body .= "<p class='text-bold'>Billing Information</p>";
                    $body .= "<p>Name : " . $b->firstname . " "  . $b->lastname . "</p>";
                    $body .= "<p>Address : " . $b->address . " </p>";
                    $body .= "<p>Phone no. : " . $b->phoneno . " </p>";
                    $body .= "<table>
                                  <thead>
                                      <tr>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total Price</th>
                                      </tr>
                                  </thead>
                                  <tbody>";

                    $bag = CB::bnbQuery("SELECT product.name,
                        product.price,
                        orderprod.quantity,
                        product.discount,
                        product.discount_from,
                        product.discount_to FROM orderprod INNER JOIN product ON orderprod.productid=product.productid WHERE orderprod.orderid='" . $order->id . "'");
                    foreach ($bag as $key => $value) {
                        $body .= "<tr>";
                        $body .= "<td>" . $value['name'] . "</td>";

                        if($value['discount'] != null && (strtotime(date("Y-m-d H:i:s")) >= strtotime($value["discount_from"]) && strtotime(date("Y-m-d H:i:s")) <= strtotime($value["discount_to"]))){
                            $body .= "<td>" . $value['price'] . "(-" . $value['discount'] . "%)</td>";
                        }else {
                            $body .= "<td>" . $value['price'] . "</td>";
                        }
                        $body .= "<td>" . $value['quantity'] . "</td>";
                        if($value['discount'] != null && (strtotime(date("Y-m-d H:i:s")) >= strtotime($value["discount_from"]) && strtotime(date("Y-m-d H:i:s")) <= strtotime($value["discount_to"]))){
                            $price = ($value['price'] - ($value['price'] * ($value['discount'] / 100)));
                            $total = $price * $value['quantity'];
                            $body .= "<td>" . $total . "</td>";
                        }else {
                            $total = $value['price'] * $value['quantity'];
                            $body .= "<td>" . $total . "</td>";
                        }
                        $body .= "</tr>";
                    }

                    $body .= "</tbody>
                              </table>";

                    $body .= "<p class='text-bold'>Shipping Information</p>";
                    $body .= "<p>Name : " . $sh->firstname . " "  . $sh->lastname . "</p>";
                    $body .= "<p>Address : " . $sh->address . " </p>";
                    $body .= "<p>Phone no. : " . $sh->phoneno . " </p>";

                    $m = Members::findFirst("memberid='$order->memberid'");

                    CB::sendMail($m->email, 'Body and Brain Order Confirmation', $body);

                    return 'success';

                } catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {

                    return 'error';

                    die( json_encode(array('401' => $e->getMessage())) );
                }
            }
        }
    }
}
