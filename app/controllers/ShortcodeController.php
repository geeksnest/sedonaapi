<?php

namespace Controllers;

use \Models\Testimonials as Testimonials;
use \Models\Pages as Pages;
use \Controllers\ControllerBase as CB;

class ShortcodeController extends \Phalcon\Mvc\Controller {

    public function testimonialAction($column,$keyword,$limit){
        if($column=='id'){
             $datatestimonial = Testimonials::findFirst("id='".$keyword."'");
             $testimonial[] = array(
                'name' => $datatestimonial->name,
                'message' => $datatestimonial->message,
                'date' => $datatestimonial->date,
                'picture' => $datatestimonial->picture
                );
        }
        elseif($column=='category'){
            $pageid = Pages::findFirst("pageslugs='".$keyword."'");
            if($limit==undefined){
                 $datatestimonial = Testimonials::find("pageid='".$pageid->pageid."'");
            }else{
                 $datatestimonial = Testimonials::find("pageid='".$pageid->pageid."' ORDER BY date desc LIMIT $limit");
            }
            foreach ($datatestimonial as $v) {
                $testimonial[] = array(
                    'name' => $v->name,
                    'message' => $v->message,
                    'date' => $v->date,
                    'picture' => $v->picture
                    );
            }
        }
        elseif($limit!=undefined){
                $datatestimonial = Testimonials::find(array("ORDER"=>"date desc", "limit"=>$limit));
                foreach ($datatestimonial as $v) {
                    $testimonial[] = array(
                        'name' => $v->name,
                        'message' => $v->message,
                        'date' => $v->date,
                        'picture' => $v->picture
                        );
                }
        }
        else{
            $datatestimonial = Testimonials::find(array("ORDER"=>"date desc"));
            foreach ($datatestimonial as $v) {
                $testimonial[] = array(
                    'name' => $v->name,
                    'message' => $v->message,
                    'date' => $v->date,
                    'picture' => $v->picture
                    );
            }
        }
        echo json_encode($testimonial);
    }


    public function newsAction($column,$keyword,$limit){
        $app = new CB();

        $db = \Phalcon\DI::getDefault()->get('db');

        if($column == 'category'){
            if($limit==undefined){
                $stmt = $db->prepare("SELECT * FROM newscat INNER JOIN newscategory ON newscategory.categoryid = newscat.catid INNER JOIN news ON news.newsid=newscat.newsid LEFT JOIN author ON news.author=author.authorid WHERE newscategory.categoryslugs = '". $keyword ."' and status = 1 ORDER BY date DESC");
            }else{
                $stmt = $db->prepare("SELECT * FROM newscat INNER JOIN newscategory ON newscategory.categoryid = newscat.catid INNER JOIN news ON news.newsid=newscat.newsid LEFT JOIN author ON news.author=author.authorid WHERE newscategory.categoryslugs = '". $keyword ."' and status = 1 ORDER BY date DESC LIMIT $limit");
            }
        }
        elseif($column == 'title'){
            $stmt = $db->prepare("SELECT news.title, news.newsid, news.newsslugs, news.imagethumb, news.videothumb, news.date, author.name FROM news LEFT JOIN author ON news.author=author.authorid WHERE status = 1 AND newsslugs = '".$keyword."'");
        }
        elseif($limit != undefined){
            $stmt = $db->prepare("SELECT news.title, news.newsid, news.newsslugs, news.imagethumb, news.videothumb, news.date, author.name FROM news LEFT JOIN author ON news.author=author.authorid WHERE status = 1 ORDER BY date DESC LIMIT $limit");
        }
        else{
          $stmt = $db->prepare("SELECT news.title, news.newsid, news.newsslugs, news.imagethumb, news.videothumb, news.date, author.name FROM news LEFT JOIN author ON news.author=author.authorid WHERE status = 1 ORDER BY date DESC");
        }
        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach($searchresult as $sr=>$val){
            $categories = $app->dbSelect("SELECT newscategory.categoryid, newscategory.categoryname FROM newscategory INNER JOIN newscat ON newscategory.categoryid = newscat.catid WHERE newscat.newsid ='".$val['newsid']."' ");
            $searchresult[$sr]['category'] = $categories;
        }
         echo json_encode($searchresult);
    }
}
