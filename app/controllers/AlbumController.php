<?php

namespace Controllers;

use \Models\Album as Album;
use \Models\Image as Image;
use \Models\Slider as Slider;
use \Models\Pagecategory as Pagecategory;
use \Controllers\ControllerBase as CB;

class AlbumController extends \Phalcon\Mvc\Controller {

          public function addAlbumAction() {
        var_dump($_POST);

        $files = $_POST['fileout'];
        $album = Album::findFirst("album_name='" . $_POST['albumname'] ."'");
        if($album){
            $generateid = md5(uniqid(rand(), true));
            // $max = Slider::maximum("album_name='" . $_POST['albumname'] ."'");
            $max = Slider::maximum(
                array(
                    "column"     => "sort",
                    "conditions" => "album_name = '". $_POST['albumname'] ."'"
                    )
                );
            $imgUpload = new Slider();
            $imgUpload->assign(array(
                'imgID'=>$generateid,
                'description'=>'Description Here',
                'descsize'=>'20',
                'desccolor'=>'#FFFFFF',
                'title'=>'Title Here',
                'titlecolor'=>'#FFFFFF',
                'titlesize'=>'46',
                'position'=>'left',
                'box'=>'off',
                'album_name'=>$_POST['albumname'],
                'foldername'=>$_POST['foldername'],
                'album_id'=>$album->album_id,
                'img'=>$_POST['img'],
                'sort'=>$max + 1,
                'status'=>1,
                'display'=>'yes',
                'date_created'=> date('Y-m-d'),
                'date_updated'=> date("Y-m-d H:i:s")
                ));
            if (!$imgUpload->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $imgUpload->getMessages()]);
            }else{
                $data['success'] = "Image Uploaded.";
                echo json_encode($data);
            }

        }else{

            $albumID = md5(uniqid(rand(), true));
            $find = Album::findFirst("category='".$_POST['category']."'");
            if ($find) {
                $main = 0;
            }
            else{
                $main = 1;
            }

            $newAlbum = new Album();
            $newAlbum->assign(array(
                'album_id'=> $albumID,
                'album_name'=> $_POST['albumname'],
                'category'=> $_POST['category'],
                'foldername'=> $_POST['foldername'],
                'date_created'=> date('Y-m-d H:i:s'),
                'main'=> $main
                ));

            if ($newAlbum->save()) {
                $generateid = md5(uniqid(rand(), true));
                $imgUpload = new Slider();
                $imgUpload->assign(array(
                    'imgID'=>$generateid,
                    'description'=>'Description Here',
                    'descsize'=>'20',
                    'desccolor'=>'#000000',
                    'title'=>'Title Here',
                    'titlecolor'=>'#FFFFFF',
                    'titlesize'=>'46',
                    'position'=>'left',
                    'box'=>'off',
                    'status'=>1,
                    'album_name'=>$_POST['albumname'],
                    'foldername'=>$_POST['foldername'],
                    'album_id'=>$albumID,
                    'img'=>$_POST['img'],
                    'sort'=>$_POST['filecount'],
                    'date_created'=> date('Y-m-d'),
                    'date_updated'=> date("Y-m-d H:i:s")
                    ));

                if (!$imgUpload->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                    echo json_encode(["error" => $imgUpload->getMessages()]);
                }else{
                    $data['success'] = "Image Uploaded.";
                    echo json_encode($data);
                }
            }else{
                $data['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $newAlbum->getMessages()]);
            }




        }
        echo json_encode($data);


    }



     public function albumslistAction($all) {

        $album = Album::find(array("order" => "num DESC"));

        foreach ($album as $album) {

            $db = \Phalcon\DI::getDefault()->get('db');
            $conditions = $db->prepare("SELECT * FROM slider WHERE album_id = '".$album->album_id."' AND sort=1");
            $conditions->execute();
            $item = $conditions->fetchAll(\PDO::FETCH_ASSOC);

            $find = Pagecategory::findFirst('id="'.$album->category.'"');
            $cat = $find->title;
            if ($cat==null) {
                $cat = 'Homepage';
            }
            $data[] = array(
                'num'=>$album->num,
                'album_name'=>$album->album_name,
                'category'=>$album->category,
                'cat'=>$cat,
                'album_id'=>$album->album_id,
                'main'=>$album->main,
                'date_created' => $album->date_created,
                'item' => $item
                );
        }
        echo json_encode($data);


    }


    public function categorylistAction() {
       $app = new CB();
       $sql2 = "SELECT * FROM pagecategory";
       $searchresult = $app->dbSelect($sql2);
       echo json_encode(array('data' => $searchresult));
   }


      public function setMainSliderAction($album_id,$category) {

        $album2 = Album::findFirst('main = 1 and category = "'.$category.'"');
        if($album2){

            $album2->main = 0;

            if (!$album2->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";

                $album = Album::findFirst("album_id='" . $album_id ."'");
                if($album2){

                    $album->main = 1;
                    if (!$album->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    } else {
                        $data['success'] = "Success";
                    }
                }
            }
            $data = 'sdasd';
        }
        else
        {
            $album = Album::findFirst("album_id='" . $album_id ."'");
                    $album->main = 1;
                    if (!$album->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    } else {
                        $data['success'] = "Success";
                    }

        }

        echo json_encode($data);

    }


     public function albumsimagesAction($albumid) {

        $album = Album::findFirst("album_id='" . $albumid ."'");

        $data = array();

        $db = \Phalcon\DI::getDefault()->get('db');
        $conditions = $db->prepare("SELECT * FROM slider WHERE album_id = '".$albumid."' ORDER BY sort ASC");
        $conditions->execute();
        $item = $conditions->fetchAll(\PDO::FETCH_ASSOC);

        $data = array(
            'num'=>$album->num,
            'album_name'=>$album->album_name,
            'album_id'=>$album->album_id,
            'foldername'=>$album->foldername,
            'date_created' => $album->date_created,
            'itemcount' => count($item),
            'item' => $item
            );
        echo json_encode($data);


    }

        public function editImageAction($imgID) {

        $db = \Phalcon\DI::getDefault()->get('db');
        $conditions = $db->prepare("SELECT * FROM slider WHERE imgID = '".$imgID."'");
        $conditions->execute();
        $data = $conditions->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode($data);
    }

    public function updateImageAction() {

        if($_POST){
            $image = Slider::findFirst("imgID='" . $_POST['imgID'] ."'");
            $image->title = $_POST['title'];
            $image->description = $_POST['description'];
            $image->titlecolor = $_POST['titlecolor'];
            $image->titlesize = $_POST['titlesize'];
            $image->desccolor = $_POST['desccolor'];
            $image->descsize = $_POST['descsize'];
            $image->position = $_POST['position'];
            $image->box = $_POST['box'];
            $image->display = $_POST['display'];
            $image->btnname = $_POST['btnname'];
            $image->btnlink = $_POST['btnlink'];
            if (!$image->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }

        }
    }

       public function updateAlbumItemAction() {

        if($_POST){
            $image = Slider::findFirst("imgID='" . $_POST['imgID'] ."'");
            $image->img = $_POST['img'];
            $image->title = $_POST['title'];
            $image->description = $_POST['description'];
            $image->titlecolor = $_POST['titlecolor'];
            $image->titlesize = $_POST['titlesize'];
            $image->desccolor = $_POST['desccolor'];
            $image->descsize = $_POST['descsize'];
            $image->position = $_POST['position'];
            $image->box = $_POST['box'];
            $image->display = $_POST['display'];
            $image->btnname = $_POST['btnname'];
            $image->btnlink = $_POST['btnlink'];
            if (!$image->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }

        }

    }

      public function deleteImageAction($imgID) {

        $dltPhoto = Slider::findFirst("imgID='" . $imgID ."'");
        $album_id = $dltPhoto->album_id;
        $data = array('error' => 'Not Found');
        if ($dltPhoto) {
            if($dltPhoto->delete()){
                $data = array('success' => 'Photo has Been deleted');

            }

            $images = Slider::find(array("album_id='" . $album_id ."'","order" => "sort ASC"));

            if ($images) {
                $x = 1;

                foreach($images as $images){
                    $images->sort = $x;
                    $x++;

                    if ($images->save()) {
                        $data = array('success' => 'Deleted');
                    }
                }
            }
        }
        echo json_encode($data);

    }

      public function deleteAlbumAction($album_id) {

        $dltAlbum = Album::findFirst("album_id='" . $album_id ."'");
        $data = array('error' => 'Not Found');
        if ($dltAlbum) {
            if($dltAlbum->delete()){
                $data = array('success' => 'Photo has Been deleted');

            }
        }

        $dltImages = Slider::find("album_id='" . $album_id ."'");
        if ($dltImages) {
            foreach($dltImages as $dltImages){

                if ($dltImages->delete()) {
                    $data = array('success' => 'Deleted');
                }
            }
        }

        echo json_encode($data);

    }


       public function sortSliderAction($imgID,$sort,$album_id) {


        $image = Slider::findFirst("sort='" . $sort ."' AND album_id='" . $album_id ."'");
        $image2 = Slider::findFirst("imgID='" . $imgID ."'");
        if($image){

            $image->sort = $image2->sort;

            if (!$image->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";

                $image2->sort = $sort;

                if (!$image2->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "Success";

                    $image3 = Slider::findFirst("sort = 0 AND album_id='" . $album_id ."'");
                    $count = Slider::find("album_id='" . $album_id ."'");
                    $total = count($count);
                    var_dump(count($count));
                    if($image3){
                        $image3->sort = 5;
                        if (!$image3->save()) {
                            $data['error'] = "Something went wrong saving the data, please try again.";
                        } else {
                            $data['success'] = "Success";
                        }
                    }

                }
            }

        }

    }

      public function addAlbumItemAction() {
        var_dump($_POST);

        $album = Album::findFirst("album_name='" . $_POST['album_name'] ."'");
        if($album){
            $generateid = md5(uniqid(rand(), true));
            // $max = Slider::maximum("album_name='" . $_POST['albumname'] ."'");
            $max = Slider::maximum(
                array(
                    "column"     => "sort",
                    "conditions" => "album_name = '". $_POST['album_name'] ."'"
                    )
                );
            $imgUpload = new Slider();

            if($_POST['titlecolor'] == undefined || $_POST['titlecolor'] == null)
            {
                $titlecolor = '#000000';
            }else{
                $titlecolor = $_POST['titlecolor'];
            }if($_POST['titlesize'] == undefined || $_POST['titlesize'] == null)
            {
                $titlesize = '46';
            }else{
                $titlesize = $_POST['titlecolor'];
            }
            if($_POST['desccolor'] == undefined || $_POST['desccolor'] == null)
            {
                $desccolor = '#000000';
            }else{
                $desccolor = $_POST['desccolor'];
            }
            if($_POST['descsize'] == undefined || $_POST['descsize'] == null)
            {
                $descsize = '20';
            }else{
                $descsize = $_POST['descsize'];
            }


            $imgUpload->assign(array(
                'imgID'=>$generateid,
                'description'=>$_POST['description'],
                'desccolor'=>$desccolor,
                'descsize'=>$descsize,
                'title'=>$_POST['title'],
                'titlecolor'=>$titlecolor,
                'titlesize'=>$titlesize,
                'position'=>$_POST['position'],
                'box'=>$_POST['box'],
                'status'=>1,
                'display'=>$_POST['display'],
                'btnname'=>$_POST['btnname'],
                'btnlink'=>$_POST['btnlink'],
                'album_name'=>$_POST['album_name'],
                'foldername'=>$_POST['foldername'],
                'album_id'=>$album->album_id,
                'img'=>$_POST['img'],
                'sort'=>$max + 1,
                'date_created'=> date('Y-m-d'),
                'date_updated'=> date("Y-m-d H:i:s")
                ));
            if (!$imgUpload->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $imgUpload->getMessages()]);
            }else{
                $data['success'] = "Image Uploaded.";
                echo json_encode($data);
            }

        }


    }

     public function mainslideAction() {

        $album = Album::findFirst('main=1 and category="HOMEPAGE"');
        $data = array();
        if ($album) {
            $image = Slider::find("album_id='".$album->album_id."' and status=1 order by sort ASC");
           // $image=Slider::find(array("album_id"=>$album->album_id,"status"=>1));
            $slides = json_encode($image->toArray(), JSON_NUMERIC_CHECK);
            $data = array(
                'slides' => $slides
                );
        }
        echo json_encode($data);
    }

    public function healingslideAction() {

        $album = Album::findFirst('main=1 and category="46B1DF6C-E304-48C1-8145-6CC586B3C3C8"');
        $data = array();
        if ($album) {
           $image = Slider::find("album_id='".$album->album_id."' and status=1 order by sort ASC");
            $slides = json_encode($image->toArray(), JSON_NUMERIC_CHECK);
            $data = array(
                'slides' => $slides
                );
        }
        echo json_encode($data);
    }

     public function readingslideAction() {

        $album = Album::findFirst('main=1 and category="READINGS"');
        $data = array();
        if ($album) {
            $image = Slider::find("album_id='".$album->album_id."' and status=1 order by sort ASC");
            $slides = json_encode($image->toArray(), JSON_NUMERIC_CHECK);
            $data = array(
                'slides' => $slides
                );
        }
        echo json_encode($data);
    }

     public function acupunctureslideAction() {

        $album = Album::findFirst('main=1 and category="ACUPUNCTURE"');
        $data = array();
        if ($album) {
            $image = Slider::find("album_id='".$album->album_id."' and status=1 order by sort ASC");
            $slides = json_encode($image->toArray(), JSON_NUMERIC_CHECK);
            $data = array(
                'slides' => $slides
                );
        }
        echo json_encode($data);
    }

     public function retreatslideAction() {

        $album = Album::findFirst('main=1 and category="RETREATS"');
        $data = array();
        if ($album) {
             $image = Slider::find("album_id='".$album->album_id."' and status=1 order by sort ASC");
            $slides = json_encode($image->toArray(), JSON_NUMERIC_CHECK);
            $data = array(
                'slides' => $slides
                );
        }
        echo json_encode($data);
    }
     public function workshopslideAction() {

        $album = Album::findFirst('main=1 and category="WORKSHOPS"');
        $data = array();
        if ($album) {
            $image = Slider::find("album_id='".$album->album_id."' and status=1 order by sort ASC");
            $slides = json_encode($image->toArray(), JSON_NUMERIC_CHECK);
            $data = array(
                'slides' => $slides
                );
        }
        echo json_encode($data);
    }

     public function setstatusAction($imgid,$status) {
        $data = array();
        $test = Slider::findFirst('imgID="' . $imgid . '"');
        $test->status = $status;
            if (!$test->save()) {
                $data['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
            }

            echo json_encode($data);
    }



}
