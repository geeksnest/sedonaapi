<?php

namespace Controllers;

use \Models\Menu as Menu;
use \Models\Footercolumn as Specialpagecolumncontent;
use \Models\Footerrow as Specialpagerow;
use \Models\Footertext as Textmodule;
use \Models\Footerimage as Moduleimage;
use \Models\Footertestimonial as Moduletestimonial;
use \Models\Footercontact as Modulecontact;
use \Models\Footernews as Modulenews;
use \Models\Footerdivider as Moduledivider;
use \Models\Footerbutton as Modulebutton;
use \Models\Testimonials as Testimonials;
use \Models\News as News;
use \Models\Author as Author;
use \Models\Newscategory as Newscategory;
use \Models\Newscat as Newscat;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use \Controllers\ControllerBase as CB;



class FooterController extends \Phalcon\Mvc\Controller {

     public function saveAction(){
            //RETURN transaction IF DATA doesnt save 
            function _ifsave($transaction, $data, $msg){
                $array = [];
                foreach ($data->getMessages() as $message){
                    echo $array[] = $message;
                }
                $transaction->rollback($msg);
            }

        $request = new \Phalcon\Http\Request();
        function _deletemodule($transaction)
        {
              $workshopvenue = Specialpagerow::find();
              if($workshopvenue != 0){
                      foreach ($workshopvenue as $workshopvenue) {
                        $workshopvenue->setTransaction($transaction);
                        if (!$workshopvenue->delete()) {
                            $array = [];
                            foreach ($workshopvenue->getMessages() as $message) {
                                $array[] = $message;
                            }
                            $transaction->rollback('Cannot save service.');
                        }
                        $rowid=$workshopvenue->row;
                        $subconditions = 'rowid="' . $rowid . '"';
                        $specialpagecolumncontent = Specialpagecolumncontent::find(array($subconditions));
                        foreach ($specialpagecolumncontent as $specialpagecolumncontent) {
                            $specialpagecolumncontent->setTransaction($transaction);
                            if($specialpagecolumncontent->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot save service prices.');
                            }
                        }

                        $textmodule = Textmodule::find(array($subconditions));
                        foreach ($textmodule as $textmodule) {
                            $textmodule->setTransaction($transaction);
                            if($textmodule->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot save service prices.');
                            }
                        }

                        $moduleimage = Moduleimage::find(array($subconditions));
                        foreach ($moduleimage as $moduleimage) {
                            $moduleimage->setTransaction($transaction);
                            if($moduleimage->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot save service prices.');
                            }
                        }
                        $moduletestimonial = Moduletestimonial::find(array($subconditions));
                        foreach ($moduletestimonial as $moduletestimonial) {
                            $moduletestimonial->setTransaction($transaction);
                            if($moduletestimonial->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot save service prices.');
                            }
                        }
                        $modulecontact = Modulecontact::find(array($subconditions));
                        foreach ($modulecontact as $modulecontact) {
                            $modulecontact->setTransaction($transaction);
                            if($modulecontact->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot save service prices.');
                            }
                        }
                        $modulenews = Modulenews::find(array($subconditions));
                        foreach ($modulenews as $modulenews) {
                            $modulenews->setTransaction($transaction);
                            if($modulenews->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot save service prices.');
                            }
                        }
                         $moduledivider = Moduledivider::find(array($subconditions));
                        foreach ($moduledivider as $moduledivider) {
                            $moduledivider->setTransaction($transaction);
                            if($moduledivider->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot save service prices.');
                            }
                        }

                        $modulebutton = Modulebutton::find(array($subconditions));
                        foreach ($modulebutton as $modulebutton) {
                            $modulebutton->setTransaction($transaction);
                            if($modulebutton->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot delete button.');
                            }
                        }

                    }
                }
        }
      
        if($request->isPost()){
            //TRY
            try{
                $transactionManager = new TransactionManager();
                $transaction        = $transactionManager->get();
                            //Function Specialpagecolumncontent
                            function __specialpagecolumncontent($transaction, $rowid, $columnposition, $modules){
                                 $columncontent  = new Specialpagecolumncontent();
                                 $columncontent->setTransaction($transaction);
                                 $columncontent->assign(array(
                                    'rowid' =>     $rowid,
                                    'columnposition' => $columnposition,
                                    'module' => $modules
                                    ));
                                 (!$columncontent->save()? _ifsave($transaction, $columncontent, "Cannot save columncontent") : " ");
                            }
                            //Function text
                            function __text($transaction, $rowid, $colid, $content){
                                $TEXT  = new Textmodule();
                                $TEXT->setTransaction($transaction);
                                $TEXT->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'content' => $content
                                    ));
                                (!$TEXT->save()? _ifsave($transaction, $TEXT, "Cannot save TEXT") : " "); 
                            } 
                            //Function IMage
                            function __image($transaction, $rowid, $colid, $image,$link){
                               $IMAGE  = new Moduleimage();
                               $IMAGE->setTransaction($transaction);
                               $IMAGE->assign(array(
                                'rowid' => $rowid,
                                'colid' => $colid,
                                'image' => $image,
                                'link'  => $link
                                ));
                               (!$IMAGE->save()? _ifsave($transaction, $IMAGE, "Cannot save IMAGE") : " ");  
                            }
                            //Function TESTIMONIAL
                            function __testimonial($transaction, $rowid, $colid, $category, $limitcount){
                                $TESTIMONIAL  = new Moduletestimonial();
                                $TESTIMONIAL->setTransaction($transaction);
                                $TESTIMONIAL->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'category' => $category,
                                    'limitcount'  => $limitcount
                                    ));
                                (!$TESTIMONIAL->save()? _ifsave($transaction, $TESTIMONIAL, "Cannot save TESTIMONIAL") : " ");  
                            } 
                            //Function Contact
                            function __contact($transaction, $rowid, $colid, $title, $phone, $email, $hours, $address){
                                $CONTACT  = new Modulecontact();
                                $CONTACT->setTransaction($transaction);
                                $CONTACT->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'title' => $title,
                                    'phone' => $phone,
                                    'email' => $email,
                                    'hours' => $hours,
                                    'address' => $address
                                    ));
                                (!$CONTACT->save()? _ifsave($transaction, $CONTACT, "Cannot save CONTACT") : " ");
                            } 
                            //Function NEWS
                            function __news($transaction, $rowid, $colid, $mostviewed, $popular, $limitcount){
                                $NEWS  = new Modulenews();
                                $NEWS->setTransaction($transaction);
                                $NEWS->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'mostviewed' => $mostviewed,
                                    'popular'  => $popular,
                                    'limitcount' => $limitcount
                                    ));
                                (!$NEWS->save()? _ifsave($transaction, $NEWS, "Cannot save NEWS") : " ");
                            }
                            //Function DEVIDER
                            function __devider($transaction, $rowid, $colid, $height, $color){
                                $DIVIDER  = new Moduledivider();
                                $DIVIDER->setTransaction($transaction);
                                $DIVIDER->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'height' => $height,
                                    'color'  => $color
                                    ));
                                (!$DIVIDER->save()? _ifsave($transaction, $DIVIDER, "Cannot save DIVIDER") : " ");
                            }
                            //Function BUTTON
                            function __button($transaction, $rowid, $colid, $btnname, $btnlink, $bgcolor, $fcolor,$padtop,$padside,$font,$position){
                                $BUTTON  = new Modulebutton();
                                $BUTTON->setTransaction($transaction);
                                $BUTTON->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'btnname' => $btnname,
                                    'btnlink'  => $btnlink,
                                    'bgcolor' => $bgcolor,
                                    'fcolor' => $fcolor,
                                    'padtop' => $padtop,
                                    'padside' => $padside,
                                    'font' => $font,
                                    'position' => $position
                                    ));
                                (!$BUTTON->save()? _ifsave($transaction, $BUTTON, "Cannot save BUTTON") : " ");
                            }

                    //start foreach
                    $count       = $request->getPost('footer');
                
                    if($count) {
                                _deletemodule($transaction);
                    }      
                  
                    foreach ($count as $val){
                        if($val != ''){
                             // echo json_encode($val['column']);
                            // foreach ($val['row'] as $key => $value) {
                            //      echo json_encode($value['col']);
                            // }
                            $row = new Specialpagerow();
                            $row->setTransaction($transaction);
                            $guid = new \Utilities\Guid\Guid();
                            $rowid = $guid->GUID();
                            $row->assign(array(
                                'row' => $rowid,
                                'columntype' => $val['column'],
                                'rowcolor' => $val['rowcolor'],
                                'rowfontcolor' => $val['rowfcolor']
                                ));
                            (!$row->save()? _ifsave($transaction, $row, "Cannot save Row") : " "); 
                             foreach ($val['row'] as $key => $value) {
                                __specialpagecolumncontent($transaction, $rowid, $key+1, $value['col']);
                                 //MODULE TEXT
                                ($value['col']=='TEXT' ? __text($transaction, $rowid, $key+1 ,$value['colcontent']) :''); 
                                    //MODULE IMAGE
                                ($value['col']=='IMAGE' ? __image($transaction, $rowid, $key+1, $value['colimg'], $value['colimglink']) : '');
                                    //MODULE TESTIMONIAL
                                ($value['col']=='TESTIMONIAL' ? __testimonial($transaction, $rowid, $key+1, $value['coltestimonialcat'], $value['coltestimonial']) : '');
                                    //MODULE CONTACT
                                ($value['col']=='CONTACT' ? __contact($transaction, $rowid, $key+1, $value['coltitle'], $value['colphone'],$value['colemail'],$value['colhours'],$value['coladdress']) : '');
                                    //MODULE NEWS
                                ($value['col']=='NEWS' ? __news($transaction, $rowid, $key+1, "dummy", "dummy", $value['colnewslimit']) : '');
                                     //MODULE DIVIDER
                                ($value['col']=='DIVIDER' ? __devider($transaction, $rowid, $key+1, $value['colheight'], $value['colcolor']) : '');   
                                 //MODULE BUTTON
                                ($value['col']=='BUTTON' ? __button($transaction, $rowid, $key+1, $value['colbtnname'], $value['colbtnlink'], $value['colbgcolor'], $value['colfcolor'], $value['colpadtop'], $value['colpadside'], $value['colfont'], $value['colposition']) : '');   
                            }
                       
                        }

                    }//end foreach
                 $transaction->commit();
            //CATCH   
            }catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }
            $response['200'] = "Success.";
            $response['data'] = array('GEEK');
            die(json_encode($response));
        }
    }


 public function infoAction() {
        $app = new CB();
        //MODULES
        $row = Specialpagerow::find();
        if(count($row) != 0){

             function columnmodule($rowid,$columnposition){
                $column = Specialpagecolumncontent::findFirst("rowid='".$rowid."' AND columnposition='".$columnposition."'");
                    if($column->module == 'TEXT'){
                         $textmodule = Textmodule::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'content' => $textmodule->content,
                        );
                    }
                    elseif($column->module == 'IMAGE'){
                         $imagemodule = Moduleimage::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'image' => $imagemodule->image,
                            'link' => $imagemodule->link
                        );
                    }

                    elseif($column->module == 'TESTIMONIAL'){
                         $Moduletestimonial = Moduletestimonial::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'category' => $Moduletestimonial->category,
                            'limitcount' => $Moduletestimonial->limitcount
                        );
                    }

                    elseif($column->module == 'CONTACT'){
                         $Modulecontact = Modulecontact::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'title' => $Modulecontact->title,
                            'email' => $Modulecontact->email,
                            'phone' => $Modulecontact->phone,
                            'hours' => $Modulecontact->hours,
                            'address' => $Modulecontact->address
                        );
                    }

                    elseif($column->module == 'NEWS'){
                         $Modulenews = Modulenews::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'mostviewed' => $Modulenews->mostviewed,
                            'popular' => $Modulenews->popular,
                            'limitcount' => $Modulenews->limitcount
                        );
                    }

                    elseif($column->module == 'DIVIDER'){
                         $Moduledivider = Moduledivider::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'height' => $Moduledivider->height,
                            'color' => $Moduledivider->color
                        );
                    }
                    elseif($column->module == 'BUTTON'){
                         $Modulebutton = Modulebutton::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'btnname' => $Modulebutton->btnname,
                            'btnlink' => $Modulebutton->btnlink,
                            'bgcolor' => $Modulebutton->bgcolor,
                            'fcolor' => $Modulebutton->fcolor,
                            'padtop' => $Modulebutton->padtop,
                            'padside' => $Modulebutton->padside,
                            'font' => $Modulebutton->font,
                            'position' => $Modulebutton->position
                        );
                    }
                    return $datacol;
            }

        foreach ($row as $value) {
                $datarow[] = array(
                    'columntype' => $value->columntype,
                    'rowcolor' => $value->rowcolor,
                    'rowfontcolor' => $value->rowfontcolor
                );
                if($value->columntype == '1'){
                    $datacol[] = array(columnmodule($value->row,'1'));
                }
                elseif($value->columntype == '2'){
                    $datacol[] = array(columnmodule($value->row,'1'),columnmodule($value->row,'2'));
                }
                elseif($value->columntype == '3'){
                 $datacol[] = array(columnmodule($value->row,'1'),columnmodule($value->row,'2'),columnmodule($value->row,'3'));

                }
                elseif($value->columntype == '4'){
                $datacol[] = array(columnmodule($value->row,'1'),columnmodule($value->row,'2'),columnmodule($value->row,'3'),columnmodule($value->row,'4'));

                }
            }
        }else{
           $datarow = 'NODATA';
           $datacol[] = "";
           $datacol2[] = "";
           $datacol3[] = "";
           $datacol4[] = "";
        }
        echo json_encode(array('datarow'=>$datarow,'datacol'=>$datacol),JSON_NUMERIC_CHECK);
}


//     public function footerpageAction(){
//         $row = Specialpagerow::find();
//         function column($row,$columnposition){
//              $column = Specialpagecolumncontent::findFirst("rowid='".$row."' AND columnposition='".$columnposition."'");
//                     if($column->module == 'TEXT'){
//                          $textmodule = Textmodule::findFirst("rowid='".$row."' AND colid='".$columnposition."'");
//                          $datacol = array(
//                             'module' => $column->module,
//                             'columnposition' =>$column->columnposition,
//                             'content' => $textmodule->content,
//                         );
//                     }

//                     elseif($column->module == 'IMAGE'){
//                          $imagemodule = Moduleimage::findFirst("rowid='".$row."' AND colid='".$columnposition."'");
//                          $datacol = array(
//                             'module' => $column->module,
//                             'columnposition' =>$column->columnposition,
//                             'image' => $imagemodule->image,
//                             'link' => $imagemodule->link
//                         );
//                     }

//                     elseif($column->module == 'TESTIMONIAL'){
//                          $Moduletestimonial = Moduletestimonial::findFirst("rowid='".$row."' AND colid='".$columnposition."'");
//                           $datatestimonial = Testimonials::find("pageid='".$Moduletestimonial->category."' ORDER BY date desc LIMIT $Moduletestimonial->limitcount");
//                          foreach ($datatestimonial as $v) {
//                             $testimonial[] = array(
//                                 'name' => $v->name,
//                                 'message' => $v->message,
//                                 'date' => $v->date,
//                                 'picture' => $v->picture
//                                 );
//                          }
//                          $datacol = array(
//                             'module' => $column->module,
//                             'columnposition' =>$column->columnposition,
//                             'category' => $Moduletestimonial->category,
//                             'limitcount' => $Moduletestimonial->limitcount,
//                             'testimonial' => $testimonial
//                         );
//                     }

//                     elseif($column->module == 'CONTACT'){
//                          $Modulecontact = Modulecontact::findFirst("rowid='".$row."' AND colid='".$columnposition."'");
//                          $datacol = array(
//                             'module' => $column->module,
//                             'columnposition' =>$column->columnposition,
//                             'title' => $Modulecontact->title,
//                             'email' => $Modulecontact->email
//                         );
//                     }

//                     elseif($column->module == 'NEWS'){
//                          $Modulenews = Modulenews::findFirst("rowid='".$row."' AND colid='".$columnposition."'");
//                          $app = new CB();

//                          $db = \Phalcon\DI::getDefault()->get('db');
//                          $stmt = $db->prepare("SELECT news.title, news.newsid, news.newsslugs, news.imagethumb, news.videothumb, news.date, author.name FROM news LEFT JOIN author ON news.author=author.authorid WHERE status = 1 ORDER BY date DESC LIMIT $Modulenews->limitcount");

//                          $stmt->execute();
//                          $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

//                          foreach($searchresult as $sr=>$val){
//                             $categories = $app->dbSelect("SELECT newscategory.categoryid, newscategory.categoryname FROM newscategory INNER JOIN newscat ON newscategory.categoryid = newscat.catid WHERE newscat.newsid ='".$val['newsid']."' ");
//                             $searchresult[$sr]['category'] = $categories;
//                          }
//                          $datacol = array(
//                             'module' => $column->module,
//                             'columnposition' =>$column->columnposition,
//                             'mostviewed' => $Modulenews->mostviewed,
//                             'popular' => $Modulenews->popular,
//                             'limitcount' => $Modulenews->limitcount,
//                             'news'=>$searchresult
//                         );
//                     }

//                     elseif($column->module == 'DIVIDER'){
//                          $Moduledivider = Moduledivider::findFirst("rowid='".$row."' AND colid='".$columnposition."'");
//                          $datacol = array(
//                             'module' => $column->module,
//                             'columnposition' =>$column->columnposition,
//                             'height' => $Moduledivider->height,
//                             'color' => $Moduledivider->color
//                         );
//                     }
//                     return $datacol;
//         }

//         foreach ($row as $value){
//             $datarow[] = $value->columntype;
//             $rowcolor[] = $value->rowcolor;
//             $rowfontcolor[] = $value->rowfontcolor;
//             if($value->columntype == '1')
//             {
//                     $datacol[] = column($value->row,'1');
//                     $datacol2[] = array("module"=>"AWAN");
//                     $datacol3[] = array("module"=>"AWAN");
//                     $datacol4[] = array("module"=>"AWAN");
//             }
//             elseif($value->columntype == '2')
//             {
//                     $datacol[] = column($value->row,'1');
//                     $datacol2[] = column($value->row,'2');
//                     $datacol3[] = array("module"=>"AWAN");
//                     $datacol4[] = array("module"=>"AWAN");
//             }
//             elseif($value->columntype == '3')
//             {
//                     $datacol[] = column($value->row,'1');
//                     $datacol2[] = column($value->row,'2');
//                     $datacol3[] = column($value->row,'3');
//                     $datacol4[] = array("module"=>"AWAN");
//             }
//             elseif($value->columntype == '4')
//             {
//                     $datacol[] = column($value->row,'1');
//                     $datacol2[] = column($value->row,'2');
//                     $datacol3[] = column($value->row,'3');
//                     $datacol4[] = column($value->row,'4');
//             }
//         }
//         echo json_encode(array("datarow"=>$datarow,"rowcolor"=>$rowcolor,"rowfontcolor"=>$rowfontcolor,"col1"=>$datacol,"col2"=>$datacol2,"col3"=>$datacol3,"col4"=>$datacol4));
// }



public function footerAction(){
            function columnmodule($rowid,$columnposition){
            $column = Specialpagecolumncontent::findFirst("rowid='".$rowid."' AND columnposition='".$columnposition."'");
            switch ($column->module) {
                case 'TEXT':
                   $textmodule = Textmodule::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'content' => $textmodule->content,
                        );
                    break;
                    case 'IMAGE':
                    $imagemodule = Moduleimage::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'image' => $imagemodule->image,
                            'link' => $imagemodule->link
                        );
                    break;
                    case 'TESTIMONIAL':
                    $Moduletestimonial = Moduletestimonial::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'category' => $Moduletestimonial->category,
                            'limitcount' => $Moduletestimonial->limitcount
                        );
                    break;
                    case 'CONTACT':
                    $Modulecontact = Modulecontact::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'title' => $Modulecontact->title,
                            'phone' => $Modulecontact->phone,
                            'email' => $Modulecontact->email,
                            'hours' => $Modulecontact->hours,
                            'address' => $Modulecontact->address,
                            'str' => explode("\n", $Modulecontact->address)
                        );
                    break;
                    case 'NEWS':
                     $Modulenews = Modulenews::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'mostviewed' => $Modulenews->mostviewed,
                            'popular' => $Modulenews->popular,
                            'limitcount' => $Modulenews->limitcount
                        );
                    break;
                    case 'DIVIDER':
                         $Moduledivider = Moduledivider::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'height' => $Moduledivider->height,
                            'color' => $Moduledivider->color
                        );
                    break;
                    case 'BUTTON':
                         $Modulebutton = Modulebutton::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'btnname' => $Modulebutton->btnname,
                            'btnlink' => $Modulebutton->btnlink,
                            'bgcolor' => $Modulebutton->bgcolor,
                            'fcolor' => $Modulebutton->fcolor,
                            'padtop' => $Modulebutton->padtop,
                            'padside' => $Modulebutton->padside,
                            'font' => $Modulebutton->font,
                            'position' => $Modulebutton->position
                        );
                    break;
                
                default:
                    $datacol = array(
                            'module' => $column->module
                        );
                    break;
            }

                    return $datacol;
        }
        //MODULES
        $row = Specialpagerow::find();
        foreach ($row as $value) {
            switch($value->columntype){
            case '1':
            $datacol[] = array('type'=>$value->columntype,'color'=>$value->rowcolor,columnmodule($value->row,'1'));
            break;
            case '2':
             $datacol[] = array('type'=>$value->columntype,'color'=>$value->rowcolor,columnmodule($value->row,'1'),columnmodule($value->row,'2'));
            break;
            case '3':
             $datacol[] = array('color'=>$value->rowcolor,columnmodule($value->row,'1'),columnmodule($value->row,'2'),columnmodule($value->row,'3'));
            break;
            case '4':
             $datacol[] = array('color'=>$value->rowcolor,columnmodule($value->row,'1'),columnmodule($value->row,'2'),columnmodule($value->row,'3'),columnmodule($value->row,'4'));
            break;
            default:
             $datacol[] = array('color'=>$value->rowcolor,columnmodule($value->row,'1'));
            break;
        }

        
}
        echo json_encode(array('datacol'=>$datacol),JSON_NUMERIC_CHECK);
}


  public function loadnewsAction() {
                             $app = new CB();
                             $db = \Phalcon\DI::getDefault()->get('db');
                             $stmt = $db->prepare("SELECT news.title, news.newsid, news.newsslugs, news.imagethumb, news.videothumb, news.date, author.name FROM news LEFT JOIN author ON news.author=author.authorid WHERE status = 1 ORDER BY date DESC");

                             $stmt->execute();
                             $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                             foreach($searchresult as $sr=>$val){
                                $categories = $app->dbSelect("SELECT newscategory.categoryid, newscategory.categoryname FROM newscategory INNER JOIN newscat ON newscategory.categoryid = newscat.catid WHERE newscat.newsid ='".$val['newsid']."' ");
                                $searchresult[$sr]['category'] = $categories;
                             }
                             
          echo json_encode($searchresult);
    
    }
        public function loadtestiAction() {
            $app = new CB();
            $sql = "SELECT * FROM testimonials order by testimonials.date desc";
            $datatestimonial = $app->dbSelect($sql);

            foreach ($datatestimonial as $key => $v) {
                $testimonial[] = array(
                    'name' => $datatestimonial[$key]['name'],
                    'message' => $datatestimonial[$key]['message'],
                    'date' => $datatestimonial[$key]['date'],
                    'picture' => $datatestimonial[$key]['picture'],
                    'pageid' => $datatestimonial[$key]['pageid']
                    );
            }
            $news = News::find(array('status' => 1, 'order' => 'date DESC'));
            $dates = [];
            foreach ($news as $news)
            {
                if(!in_array(date('F Y', strtotime($news->date)), $dates)){
                    $archive[] = array(
                        'month' => date('F', strtotime($news->date)),
                        'year' => date('Y', strtotime($news->date))
                        );
                    array_push($dates, date('F Y', strtotime($news->date)));
                }
            }
                            //menu
            $sql = "SELECT * FROM menu group by menuID";
            $searchresult = $app->dbSelect($sql);

            $sql2 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 0 ORDER BY menustructure.num ASC";
            $searchresult2 = $app->dbSelect($sql2);
            $sql3 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 1 ";
            $searchresult3 = $app->dbSelect($sql3);
            $sql4 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 2 ";
            $searchresult4 = $app->dbSelect($sql4);
            $sql5 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 3 ";
            $searchresult5 = $app->dbSelect($sql5);
            $sql6 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 4 ";
            $searchresult6 = $app->dbSelect($sql6);

            foreach ($searchresult2 as $key => $value) {
                foreach ($searchresult3 as $kk => $value) {
                    if ($searchresult3[$kk]['parent']==$searchresult2[$key]['submenuID']) {
                        if ($searchresult3[$kk]['child']=="1")
                        {
                            $childid1[]=array(
                                'submenuID'=>$searchresult3[$kk]['submenuID'],
                                'menuID'=>$searchresult3[$kk]['menuID'],
                                'child'=>$searchresult3[$kk]['child'],
                                'submenuID'=>$searchresult3[$kk]['submenuID'],
                                'parent'=>$searchresult3[$kk]['parent'],
                                'order'=>$searchresult3[$kk]['order'],
                                'subname'=>$searchresult3[$kk]['subname'],
                                'sublink'=>$searchresult3[$kk]['sublink'],
                                'newtab'=>$searchresult3[$kk]['newtab'],
                                'sort'=>$searchresult3[$kk]['sort']
                                );

                        }

                    }

                }
            }

            foreach ($searchresult4 as $key4 => $value) {
                foreach ($childid1 as $key1 => $value) {
                   
                 if ($searchresult4[$key4]['parent']==$childid1[$key1]['submenuID']) {
                    $childid2[]=array(
                        'submenuID'=>$searchresult4[$key4]['submenuID'],
                        'menuID'=>$searchresult4[$key4]['menuID'],
                        'child'=>$searchresult4[$key4]['child'],
                        'submenuID'=>$searchresult4[$key4]['submenuID'],
                        'parent'=>$searchresult4[$key4]['parent'],
                        'order'=>$searchresult4[$key4]['order'],
                        'subname'=>$searchresult4[$key4]['subname'],
                        'sublink'=>$searchresult4[$key4]['sublink'],
                        'newtab'=>$searchresult4[$key4]['newtab'],
                        'sort'=>$searchresult4[$key4]['sort']
                        );

                }
            }

        }

        foreach ($searchresult5 as $key5 => $value) {
            foreach ($childid2 as $key2 => $value) {
                if ($searchresult5[$key5]['parent']==$childid2[$key2]['submenuID']) {
                    $childid3[]=array(
                        'submenuID'=>$searchresult5[$key5]['submenuID'],
                        'menuID'=>$searchresult5[$key5]['menuID'],
                        'child'=>$searchresult5[$key5]['child'],
                        'submenuID'=>$searchresult5[$key5]['submenuID'],
                        'parent'=>$searchresult5[$key5]['parent'],
                        'order'=>$searchresult5[$key5]['order'],
                        'subname'=>$searchresult5[$key5]['subname'],
                        'sublink'=>$searchresult5[$key5]['sublink'],
                        'newtab'=>$searchresult5[$key5]['newtab'],
                        'sort'=>$searchresult5[$key5]['sort']
                        );
                }

            }
        }
        
        foreach ($searchresult6 as $key6 => $value) {
            foreach ($childid3 as $key3 => $value) {
                if ($searchresult6[$key6]['parent']==$childid3[$key3]['submenuID']) {
                    $childid4[]=array(
                        'submenuID'=>$searchresult6[$key6]['submenuID'],
                        'menuID'=>$searchresult6[$key6]['menuID'],
                        'child'=>$searchresult6[$key6]['child'],
                        'submenuID'=>$searchresult6[$key6]['submenuID'],
                        'parent'=>$searchresult6[$key6]['parent'],
                        'order'=>$searchresult6[$key6]['order'],
                        'subname'=>$searchresult6[$key6]['subname'],
                        'sublink'=>$searchresult6[$key6]['sublink'],
                        'newtab'=>$searchresult6[$key6]['newtab'],
                        'sort'=>$searchresult6[$key6]['sort']
                        );
                }

            }
        }
        $children[] = array(
            'child1' => $childid1,
            'child2' => $childid2,
            'child3' => $childid3,
            'child4' => $childid4
            );
        echo json_encode(array('testi'=>$testimonial,'archive'=>$archive,'children'=>$children,'shortcode'=>$searchresult2));
        
    }
}
