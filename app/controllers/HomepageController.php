<?php

namespace Controllers;

use \Models\Homepage as Homepage;
use \Models\Contentimg as Image;
use \Models\Pagecategory as Pagecategory;
use \Controllers\ControllerBase as CB;

class HomepageController extends \Phalcon\Mvc\Controller {

    public function createAction(){

        $request = new \Phalcon\Http\Request();
        //var_dump($request->getPost('username'));
        if($request->isPost()){
            $title = $request->getPost('title');
            $description = $request->getPost('body');
            $buttonname = $request->getPost('btnname');
            $buttonlink = $request->getPost('btnlink');
            $backgroundimg = $request->getPost('backgroundimg');
            $banneropt = $request->getPost('banneropt');
            $bannersize = $request->getPost('bannersize');
            $bgcolor = $request->getPost('bgcolor');
            $titlecolor = $request->getPost('titlecolor');
            $titlesize = $request->getPost('titlesize');
            $descriptioncolor = $request->getPost('descriptioncolor');
            $descriptionsize = $request->getPost('descriptionsize');
            $position = $request->getPost('position');
            $box = $request->getPost('box');
            $display = $request->getPost('display');
            $classname = CB::randomclassname();

            $category = $request->getPost('category');

            $sortnumber = Homepage::maximum(array("column"=> "sortnumber"));

            $guid = new \Utilities\Guid\Guid();
            $usave = new Homepage();
            $usave->assign(array(
            'id' => $guid->GUID(),
            'title'   => $title,
            'titlecolor'   => $titlecolor,
            'titlesize'   => $titlesize,
            'descriptioncolor'   => $descriptioncolor,
            'descriptionsize'   => $descriptionsize,
            'description'   => $description,
            'buttonname'   => $buttonname,
            'buttonlink'   => $buttonlink,
            'textposition'   => $position,
            'box'           => $box,
            'backgroundimg'   => $backgroundimg,
            'datecreated'   => date("Y-m-d H:i:s"),
            'dateupdated'   => date("Y-m-d H:i:s"),
            'bgcolor' => $bgcolor,
            'category'      =>$category,
            'classname' => $classname,
            'banneroption' => $banneropt,
            'bannersize'=> $bannersize,
            'display' => $display,
            'sortnumber' => $sortnumber+1,
            ));

            if(!$usave->create()){
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            }else{
                echo json_encode(array('msg' => 'New Homepage has been successfuly created.', 'type'=>'success'));
            }
        }else{
            echo json_encode(array('msg' => 'No post data.', 'type'=>'error'));
        }
    }

     public function sortAction(){

        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $id = $request->getPost('id');

            foreach ($id as $key => $value) {
             $usave = Homepage::findFirst('id="' . $value['id'] . '"');
             $usave->sortnumber         = $key;
             $usave->save();
            }


        }

    }

    public function listAction($num, $page, $keyword) {
        if ($keyword == null || $keyword == undefined) {
            $content = Homepage::find(array("order"=>"sortnumber asc"));
        } else {
            $conditions = "title LIKE '%" . $keyword . "%'
            or buttonname LIKE '%" . $keyword . "%'
            or buttonlink LIKE '%" . $keyword . "%'";
            $content= Homepage::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $content,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'title' => $m->title,
                'btnlink' => $m->buttonlink,
                'btnname' => $m->buttonname,
                'textposition' => $m->textposition
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }

    public function deleteAction($id){
        $dlt = Homepage::findFirst('id="' . $id . '"');
        if ($dlt) {
            if($dlt->delete()){
                $data = array('msg' => 'Content has been successfully Deleted!', 'type'=>'success');
            }else {
                $data = array('type' => 'error', 'msg' => 'Error deleting user.');
            }
        }
        echo json_encode($data);
    }
    public function Infoction($id) {
        $getInfo = Homepage::findFirst('id="'. $id .'"');
        $data = array(
            'id' =>  $getInfo->id,
            'title' =>  $getInfo->title,
            'body' =>  $getInfo->description,
            'btnname' =>  $getInfo->buttonname,
            'btnlink' =>  $getInfo->buttonlink,
            'position' =>  $getInfo->textposition,
            'backgroundimg' =>  $getInfo->backgroundimg,
            'box' =>  $getInfo->box,
            'bgcolor' => $getInfo->bgcolor,
            'titlecolor' =>  $getInfo->titlecolor,
            'titlesize' =>  $getInfo->titlesize,
            'descriptioncolor' =>  $getInfo->descriptioncolor,
            'descriptionsize' =>  $getInfo->descriptionsize,
            'category' => $getInfo->category,
            'banneroption'=>$getInfo->banneroption,
            'bannersize'=>$getInfo->bannersize,
            'display'=>$getInfo->display
            );
        echo json_encode($data,JSON_NUMERIC_CHECK);
    }
    public function UpdateAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $id         = $request->getPost('id');
            $title   = $request->getPost('title');
            $bgcolor   = $request->getPost('bgcolor');
            $titlecolor   = $request->getPost('titlecolor');
            $titlesize   = $request->getPost('titlesize');
            $descriptioncolor   = $request->getPost('descriptioncolor');
            $descriptionsize   = $request->getPost('descriptionsize');
            $description      = $request->getPost('body');
            $btnname       = $request->getPost('btnname');
            $btnlink     = $request->getPost('btnlink');
            $textposition     = $request->getPost('position');
            $box     = $request->getPost('box');
            $backgroundimg     = $request->getPost('backgroundimg');
            $category = $request->getPost('category');
            $banneroption = $request->getPost('banneroption');
            $bannersize = $request->getPost('bannersize');
            $display = $request->getPost('display');

            //SAVE
            $usave = Homepage::findFirst('id="' . $id . '"');
            $usave->title         = $title;
            $usave->description   = $description;
            $usave->buttonname       = $btnname;
            $usave->buttonlink       = $btnlink;
            $usave->textposition  = $textposition;
            $usave->backgroundimg = $backgroundimg;
            $usave->bgcolor = $bgcolor;
            $usave->titlesize = $titlesize;
            $usave->titlecolor = $titlecolor;
            $usave->descriptionsize = $descriptionsize;
            $usave->descriptioncolor = $descriptioncolor;
            $usave->box = $box;
            $usave->category = $category;
            $usave->banneroption = $banneroption;
            $usave->bannersize = $bannersize;
            $usave->display = $display;
            if(!$usave->save()){
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('type' => 'danger', 'msg' => $errors));
            }else{
                echo json_encode(array('type' => 'success', 'msg' => 'Content info has been successfuly updated.'));
            }
        }
    }

     public function showallAction() {
        $getInfo = Homepage::find(array("order"=>"sortnumber asc"));
        foreach ($getInfo as $value) {
           $data[] = array(
            'id' =>  $value->id,
            'title' =>  $value->title,
            'bgcolor' => $value->bgcolor,
            'titlecolor' =>  $value->titlecolor,
            'titlesize' =>  $value->titlesize,
            'descriptioncolor' =>  $value->descriptioncolor,
            'descriptionsize' =>  $value->descriptionsize,
            'body' =>  $value->description,
            'btnname' =>  $value->buttonname,
            'btnlink' =>  $value->buttonlink,
            'position' =>  $value->textposition,
            'box' =>  $value->box,
            'backgroundimg' =>  $value->backgroundimg,
            'category' => $value->category,
            'classname' => $value->classname,
            'banneroption'=>$value->banneroption,
            'bannersize'=>$value->bannersize,
            'display'=>$value->display
            );
        }
        echo json_encode($data);
    }


    //IMAGE UPLOAD
    public function saveimageAction() {

        $filename = $_POST['imgfilename'];

        $picture = new Image();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }

    }
    //IMAGE DELETE
    public function deleteimageAction($imgid) {
        $img = Image::findFirst('id="'. $imgid.'"');
        $filename = $img->filename;
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }


    public function listimageAction() {

        $getimages = Image::find(array("order" => "id DESC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
        foreach ($getimages as $getimages)
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
        }
        echo json_encode($data);

    }

    public function categoriesAction() {
      $categories = Pagecategory::find(array('columns'=>"id, title"));
      $data['categories'] = $categories->toArray();
      echo json_encode($data);
    }

    public function servicesAction() {

      $query = "SELECT pages.imagethumb, pages.thumbdesc, pages.imagethumbsubtitle, pages.pageslugs,  pagecategory.title as cattitle,
                pages_style.textalign, pages_style.bgcolor, pages_style.color, pages_style.box, pages_style.classname
                  FROM pages
                    LEFT JOIN pagecategory ON pagecategory.id = pages.category
                    LEFT JOIN pages_style ON pages.pageid = pages_style.pageid
                  WHERE category != 'AURA' AND pages.featured = 'true' AND pages.status=1";
      $data['services'] = CB::dbSelect($query);

      $query_aura = "SELECT pages.imagethumb, pages.thumbdesc, pages.imagethumbsubtitle, pages.pageslugs,  pages.category as cattitle,
                pages_style.textalign, pages_style.bgcolor, pages_style.color, pages_style.box, pages_style.classname
                  FROM pages
                  LEFT JOIN pages_style ON pages.pageid = pages_style.pageid
                  WHERE category = 'AURA' AND pages.featured = 'true' AND pages.status=1";
      $data['aura'] = CB::dbSelect($query_aura);

      echo json_encode($data);
    }

}
