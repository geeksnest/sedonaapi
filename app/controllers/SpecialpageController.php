<?php

namespace Controllers;

use \Models\Menu as Menu;
use \Models\Pagesidebar as Pagesidebar;
use \Models\Specialpage as Specialpage;
use \Models\Specialpagecolumncontent as Specialpagecolumncontent;
use \Models\Specialpagerow as Specialpagerow;
use \Models\Textmodule as Textmodule;
use \Models\Moduleimage as Moduleimage;
use \Models\Moduletestimonial as Moduletestimonial;
use \Models\Modulecontact as Modulecontact;
use \Models\Modulenews as Modulenews;
use \Models\Moduledivider as Moduledivider;
use \Models\Modulebutton as Modulebutton;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use \Controllers\ControllerBase as CB;
use \Models\Testimonials as Testimonials;
use \Models\Pagesidebarimg as Sidebarimg;
use \Models\Pagesidebartestemonial as PageTestemonial;
use \Models\Pagesidebarlink as Pagesidebarlink;
use \Models\Pagesidebarnews as Pagesidebarnews;
use \Models\Pagesidebarmenu as Pagesidebarmenu;
use \Models\News as News;
use \Models\Author as Author;
use \Models\Newscategory as Newscategory;
use \Models\Newscat as Newscat;
use \Models\Pagecategory as Pagecategory;


class SpecialpageController extends \Phalcon\Mvc\Controller {

//total_hours_wasted_here=30 and counting

    public function getPageAction($pageslugs) {
        $conditions = count(Specialpage::find(array("slugs='" .$pageslugs. "'")));
        $page = count(Pagecategory::find(array("slugs='" .$pageslugs. "'")));
        $tot = $conditions+$page;
        echo json_encode($tot);
    }

     public function saveAction(){
            //RETURN transaction IF DATA doesnt save 
            function _ifsave($transaction, $data, $msg){
                $array = [];
                foreach ($data->getMessages() as $message){
                    echo $array[] = $message;
                }
                $transaction->rollback($msg);
            }

                //SAVE SIDEBAR IMAGE FUNCTION 
            function _saveimg($transaction, $sidebarid, $image, $url){
            $_adddata = new Sidebarimg();
            $_adddata->assign(array(
                'sidebarid' => $sidebarid,
                'image'     => $image,
                'url'       => $url));
                (!$_adddata->save()? _ifsave($transaction, $_adddata, "Cant Save Image") : " "); //Check if data is saving else //Call Funtion _ifsave(); Parameters: transaction, get error to DATA to be save , message
            }

            //SAVE TESTIMONIAL FUNCTION 
            function _testimonial($transaction, $sidebarid, $limitlist){
                $_adddata = new PageTestemonial();
                $_adddata->assign(array(
                    'sidebarid' => $sidebarid,
                    'limit'     => $limitlist));
                (!$_adddata->save()? _ifsave($transaction, $_adddata, "Cant Save Testimonial") : " "); //Check if data is saving else //Call Funtion _ifsave(); Parameters: transaction, get error to DATA to be save , message
            }

            //SAVE SIDEBAR IMAGE FUNCTION 
            function _link($transaction, $sidebarid, $label, $url){
                $_adddata = new Pagesidebarlink();
                $_adddata->assign(array(
                    'sidebarid' => $sidebarid,
                    'label'     => $label,
                    'url'       => $url));
                (!$_adddata->save()? _ifsave($transaction, $_adddata, "Cant Save Links") : " ");  //Check if data is saving else //Call Funtion _ifsave(); Parameters: transaction, get error to DATA to be save , message
            }

            //SAVE SIDEBAR NEWS FUNCTION 
            function _news($transaction, $sidebarid, $newslist){
                $_adddata = new Pagesidebarnews();
                $_adddata->assign(array(
                    'sidebarid' => $sidebarid,
                    'limit'     => $newslist,
                    'views' => '1',
                    'popular'  => '1',
                    ));
                (!$_adddata->save()? _ifsave($transaction, $_adddata, "Cant Save news") : " ");  //Check if data is saving else //Call Funtion _ifsave(); Parameters: transaction, get error to DATA to be save , message
            }
            function _menu($transaction, $sidebarid, $menuID){
                $_adddata = new Pagesidebarmenu();
                $_adddata->assign(array(
                    'sidebarid' => $sidebarid,
                    'menuID'   => $menuID
                    ));
                (!$_adddata->save()? _ifsave($transaction, $_adddata, "Cant Save menu") : " ");  //Check if data is saving else //Call Funtion _ifsave(); Parameters: transaction, get error to DATA to be save , message
            }
            //SIDEBAR FUNCTION 
            function _sidebar($id ,$position ,$transaction, $content){
            //SAVE SIDEBAR INFO's
            foreach ($content as $key => $value) {
                    $sidebar = new Pagesidebar();
                    $sidebar->assign(array(
                        'location'      => $position,
                        'pageid'        => $id,
                        'sidebar'       => $value['sidebar'],
                        'datecreated'   => date("Y-m-d H:i:s"),
                        'dateupdated'   => date("Y-m-d H:i:s")));
                    if (!$sidebar->save()){
                        _ifsave($transaction, $sidebar, "Cannot save sidebar");//Call Funtion _ifsave(); Parameters: transaction, get error to DATA to be save , message
                    }
                    else{
                        if($value['sidebar'] == 'Image'){
                             _saveimg($transaction, $sidebar->id, $value['img'], $value['imglink']); //Call Funtion _saveimg(); Parameters: transaction, Sidebar Last save id, Image, Url
                         }elseif($value['sidebar'] == 'Testimonial'){
                            _testimonial($transaction, $sidebar->id, $value['limittest']); //Call Funtion _testimonial(); Parameters: transaction, Sidebar Last save id, testimonial limit
                        }elseif($value['sidebar'] == 'Link'){
                            _link($transaction, $sidebar->id, $value['label'], $value['url']); //Call Funtion _link(); Parameters: transaction, Sidebar Last save id, Label, Url
                        }elseif($value['sidebar'] == 'News'){
                            _news($transaction, $sidebar->id, $value['limitnews']); //Call Funtion _saveimg(); Parameters: transaction, Sidebar Last save id, popular, views, newslist
                        }elseif($value['sidebar'] == 'Menu'){
                            _menu($transaction, $sidebar->id, $value['menuID']); //Call Funtion _saveimg(); Parameters: transaction, Sidebar Last save id, popular, views, newslist
                        }
                    }
            }

        }// END SIDEBAR FUNCTION 


        $request = new \Phalcon\Http\Request();

        var_dump($request->isPost());
        if($request->isPost()){
            //TRY
            try{
                $title          = $request->getPost('title');
                $slugs          = $request->getPost('slugs');
                $metatitle      = $request->getPost('metatitle');
                $metatags       = $request->getPost('metatags');
                $metadesc       = $request->getPost('metadesc');
                $sidebarleft    = $request->getPost('sidebarleft');
                $sidebarright   = $request->getPost('sidebarright');
                $type           = $request->getPost('type');
                $status         = ($request->getPost('status') == 'true' ? 1 : 0);
                $body           = ($type == 'Normal' ? $request->getPost('body') : '');
                $sortleft       = $request->getPost('sidedataL');
                $sortright      = $request->getPost('sidedataR');
                $count          = $request->getPost('column');
                //$count      = json_decode($count);

                $banner               = $request->getPost('banner');
                $imagethumb           = ($banner == 'true' ? $request->getPost('imagethumb') : '');
                $imagethumbsubtitle   = ($banner == 'true' ? $request->getPost('imagethumbsubtitle') : '');
                $thumbdesc            = ($banner == 'true' ? $request->getPost('thumbdesc') : '');
                $align                = ($banner == 'true' ? $request->getPost('align') : '');
                $bgcolor              = ($banner == 'true' ? $request->getPost('bgcolor') : '');
                $color                = ($banner == 'true' ? $request->getPost('color') : '');
                $box                  = ($banner == 'true' ? $request->getPost('box') : '');
                $btnname              = ($banner == 'true' ? $request->getPost('btnname') : '');
                $btnlink              = ($banner == 'true' ? $request->getPost('btnlink') : '');
                $trans                = ($box == 'true' ? $request->getPost('trans') : '');
                $boxcolor             = ($box == 'true' ? $request->getPost('boxcolor') : '');
                $edge                 = ($box == 'true' ? $request->getPost('edge') : '');

                $transactionManager = new TransactionManager();
                $transaction        = $transactionManager->get();
                $guid               = new \Utilities\Guid\Guid();
                $id                 = $guid->GUID();
                $page               = new Specialpage();
                $page->setTransaction($transaction);
                $page->assign(array(
                    'pageid'        => $id,
                    'title'         => $title,
                    'slugs'         => $slugs,
                    'metatitle'     => $metatitle,
                    'metakeyword'   => $metatags,
                    'metadesc'      => $metadesc,
                    'body'          => $body,
                    'leftsidebar'   => $sidebarleft,
                    'rightsidebar'  => $sidebarright,
                    'pagetype'      => $type,
                    'status'        => $status,
                    'banner'        => $banner,
                    'imagethumb'        => $imagethumb,
                    'imagethumbsubtitle'        => $imagethumbsubtitle,
                    'thumbdesc'        => $thumbdesc,
                    'align'        => $align,
                    'bgcolor'        => $bgcolor,
                    'color'        => $color,
                    'box'        => $box,
                    'btnname'       => $btnname,
                    'btnlink'       => $btnlink,
                    'boxtrans'      => $trans,
                    'boxcolor'      => $boxcolor,
                    'boxedge'       => $edge,
                    'datecreated'   => date("Y-m-d H:i:s"),
                    'dateupdated'   => date("Y-m-d H:i:s")
                    ));
                if (!$page->save()) {
                    _ifsave($transaction, $page, "Can't Save Sidebar");
                }

                if($sidebarleft == "true"){
                    _sidebar($id, 'LEFT', $transaction, $sortleft); 
                }

                if($sidebarright== "true"){
                    _sidebar($id, 'RIGHT', $transaction, $sortright);   
                }


                if($type=='Special'){

                            //Function Specialpagecolumncontent
                            function __specialpagecolumncontent($transaction, $rowid, $columnposition, $modules){
                                 $columncontent  = new Specialpagecolumncontent();
                                 $columncontent->setTransaction($transaction);
                                 $columncontent->assign(array(
                                    'rowid' =>     $rowid,
                                    'columnposition' => $columnposition,
                                    'module' => $modules
                                    ));
                                 (!$columncontent->save()? _ifsave($transaction, $columncontent, "Cannot save columncontent") : " ");
                            }
                            //Function text
                            function __text($transaction, $rowid, $colid, $content){
                                $TEXT  = new Textmodule();
                                $TEXT->setTransaction($transaction);
                                $TEXT->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'content' => $content
                                    ));
                                (!$TEXT->save()? _ifsave($transaction, $TEXT, "Cannot save TEXT") : " "); 
                            } 
                            //Function IMage
                            function __image($transaction, $rowid, $colid, $image,$link){
                               $IMAGE  = new Moduleimage();
                               $IMAGE->setTransaction($transaction);
                               $IMAGE->assign(array(
                                'rowid' => $rowid,
                                'colid' => $colid,
                                'image' => $image,
                                'link'  => $link
                                ));
                               (!$IMAGE->save()? _ifsave($transaction, $IMAGE, "Cannot save IMAGE") : " ");  
                            }
                            //Function TESTIMONIAL
                            function __testimonial($transaction, $rowid, $colid, $category, $limitcount){
                                $TESTIMONIAL  = new Moduletestimonial();
                                $TESTIMONIAL->setTransaction($transaction);
                                $TESTIMONIAL->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'category' => $category,
                                    'limitcount'  => $limitcount
                                    ));
                                (!$TESTIMONIAL->save()? _ifsave($transaction, $TESTIMONIAL, "Cannot save TESTIMONIAL") : " ");  
                            } 
                            //Function Contact
                            function __contact($transaction, $rowid, $colid, $title,$phone,$email,$hours,$address){
                                $CONTACT  = new Modulecontact();
                                $CONTACT->setTransaction($transaction);
                                $CONTACT->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'title' => $title,
                                    'phone' => $phone,
                                    'email' => $email,
                                    'hours' => $hours,
                                    'address' => $address
                                    ));
                                (!$CONTACT->save()? _ifsave($transaction, $CONTACT, "Cannot save CONTACT") : " ");
                            } 
                            //Function NEWS
                            function __news($transaction, $rowid, $colid, $mostviewed, $popular, $limitcount){
                                $NEWS  = new Modulenews();
                                $NEWS->setTransaction($transaction);
                                $NEWS->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'mostviewed' => 'null',
                                    'popular'  => 'null',
                                    'limitcount' => $limitcount
                                    ));
                                (!$NEWS->save()? _ifsave($transaction, $NEWS, "Cannot save NEWS") : " ");
                            }
                            //Function DEVIDER
                            function __devider($transaction, $rowid, $colid, $height, $color){
                                $DIVIDER  = new Moduledivider();
                                $DIVIDER->setTransaction($transaction);
                                $DIVIDER->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'height' => $height,
                                    'color'  => $color
                                    ));
                                (!$DIVIDER->save()? _ifsave($transaction, $DIVIDER, "Cannot save DIVIDER") : " ");
                            }

                            //Function BUTTON
                            function __button($transaction, $rowid, $colid, $btnname, $btnlink, $bgcolor, $fcolor,$padtop,$padside,$font,$position){
                                $BUTTON  = new Modulebutton();
                                $BUTTON->setTransaction($transaction);
                                $BUTTON->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'btnname' => $btnname,
                                    'btnlink'  => $btnlink,
                                    'bgcolor' => $bgcolor,
                                    'fcolor' => $fcolor,
                                    'padtop' => $padtop,
                                    'padside' => $padside,
                                    'font' => $font,
                                    'position' => $position
                                    ));
                                (!$BUTTON->save()? _ifsave($transaction, $BUTTON, "Cannot save BUTTON") : " ");
                            }


                    //start foreach
                     $colcount = $request->getPost('colcount');

                    foreach ($count as $val){
                            $row = new Specialpagerow();
                            $row->setTransaction($transaction);
                            $guid = new \Utilities\Guid\Guid();
                            $rowid = $guid->GUID();
                            $row->assign(array(
                                'id' =>     $rowid,
                                'pageid' => $id,
                                'columntype' => $val['colcount'],
                                ));
                            (!$row->save()? _ifsave($transaction, $row, "Cannot save Row") : " ");

                            foreach ($val['row'] as $key => $value) {
                                __specialpagecolumncontent($transaction, $rowid, $key+1, $value['col']);
                                 //MODULE TEXT
                                ($value['col']=='TEXT' ? __text($transaction, $rowid, $key+1 ,$value['colcontent']) :''); 
                                    //MODULE IMAGE
                                ($value['col']=='IMAGE' ? __image($transaction, $rowid, $key+1, $value['colimg'], $value['colimglink']) : '');
                                    //MODULE TESTIMONIAL
                                ($value['col']=='TESTIMONIAL' ? __testimonial($transaction, $rowid, $key+1, $value['coltestimonialcat'], $value['coltestimonial']) : '');
                                    //MODULE CONTACT
                                ($value['col']=='CONTACT' ? __contact($transaction, $rowid, $key+1, $value['coltitle'], $value['colphone'],$value['colemail'],$value['colhours'],$value['coladdress']) : '');
                                    //MODULE NEWS
                                ($value['col']=='NEWS' ? __news($transaction, $rowid, $key+1, "dummy", "dummy", $value['colnewslimit']) : '');
                                     //MODULE DIVIDER
                                ($value['col']=='DIVIDER' ? __devider($transaction, $rowid, $key+1, $value['colheight'], $value['colcolor']) : '');   
                                 
                                 //MODULE BUTTON
                                ($value['col']=='BUTTON' ? __button($transaction, $rowid, $key+1, $value['colbtnname'], $value['colbtnlink'], $value['colbgcolor'], $value['colfcolor'], $value['colpadtop'], $value['colpadside'], $value['colfont'], $value['colposition']) : '');   
                            
                            }
                    }//end foreach

                }//End IF Special Page

                $transaction->commit();

            //CATCH   
            }catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }
            $response['200'] = "Success.";
            $response['data'] = array('id' => $page->pageid);
            die(json_encode($response));
        }
    }


  public function updateAction(){
            //RETURN transaction IF DATA doesnt save 
            function _ifsave($transaction, $data, $msg){
                $array = [];
                foreach ($data->getMessages() as $message){
                    echo $array[] = $message;
                }
                $transaction->rollback($msg);
            }

                //SAVE SIDEBAR IMAGE FUNCTION 
            function _saveimg($transaction, $sidebarid, $image, $url){
            $_adddata = new Sidebarimg();
            $_adddata->assign(array(
                'sidebarid' => $sidebarid,
                'image'     => $image,
                'url'       => $url));
                (!$_adddata->save()? _ifsave($transaction, $_adddata, "Cant Update Image") : " "); //Check if data is saving else //Call Funtion _ifsave(); Parameters: transaction, get error to DATA to be save , message
            }

            //SAVE TESTIMONIAL FUNCTION 
            function _testimonial($transaction, $sidebarid, $limitlist){
                $_adddata = new PageTestemonial();
                $_adddata->assign(array(
                    'sidebarid' => $sidebarid,
                    'limit'     => $limitlist));
                (!$_adddata->save()? _ifsave($transaction, $_adddata, "Cant Update Testimonial") : " "); //Check if data is saving else //Call Funtion _ifsave(); Parameters: transaction, get error to DATA to be save , message
            }

            //SAVE SIDEBAR IMAGE FUNCTION 
            function _link($transaction, $sidebarid, $label, $url){
                $_adddata = new Pagesidebarlink();
                $_adddata->assign(array(
                    'sidebarid' => $sidebarid,
                    'label'     => $label,
                    'url'       => $url));
                (!$_adddata->save()? _ifsave($transaction, $_adddata, "Cant Update Links") : " ");  //Check if data is saving else //Call Funtion _ifsave(); Parameters: transaction, get error to DATA to be save , message
            }

            //SAVE SIDEBAR NEWS FUNCTION 
            function _news($transaction, $sidebarid, $newslist){
                $_adddata = new Pagesidebarnews();
                $_adddata->assign(array(
                    'sidebarid' => $sidebarid,
                    'popular'   =>  '1',
                    'views'     => '1',
                    'limit'     => $newslist
                    ));
                (!$_adddata->save()? _ifsave($transaction, $_adddata, "Cant Update news") : " ");  //Check if data is saving else //Call Funtion _ifsave(); Parameters: transaction, get error to DATA to be save , message
            }
             function _menu($transaction, $sidebarid, $menuID){
                $_adddata = new Pagesidebarmenu();
                $_adddata->assign(array(
                    'sidebarid' => $sidebarid,
                    'menuID'   => $menuID
                    ));
                (!$_adddata->save()? _ifsave($transaction, $_adddata, "Cant Save menu") : " ");  //Check if data is saving else //Call Funtion _ifsave(); Parameters: transaction, get error to DATA to be save , message
            }
            //SIDEBAR FUNCTION 
            function _sidebar($id ,$position ,$transaction, $content){
            //SAVE SIDEBAR INFO's
                foreach ($content as $key => $value) {
                        $sidebar = new Pagesidebar();
                        $sidebar->assign(array(
                            'location'      => $position,
                            'pageid'        => $id,
                            'sidebar'       => $value['sidebar'],
                            'datecreated'   => date("Y-m-d H:i:s"),
                            'dateupdated'   => date("Y-m-d H:i:s")));
                        if (!$sidebar->save()){
                            _ifsave($transaction, $sidebar, "Cannot save sidebar");//Call Funtion _ifsave(); Parameters: transaction, get error to DATA to be save , message
                        }
                        else{
                            if($value['sidebar'] == 'Image'){
                                 _saveimg($transaction, $sidebar->id, $value['img'], $value['imglink']); //Call Funtion _saveimg(); Parameters: transaction, Sidebar Last save id, Image, Url
                             }elseif($value['sidebar'] == 'Testimonial'){
                                _testimonial($transaction, $sidebar->id, $value['limittest']); //Call Funtion _testimonial(); Parameters: transaction, Sidebar Last save id, testimonial limit
                            }elseif($value['sidebar'] == 'Link'){
                                _link($transaction, $sidebar->id, $value['label'], $value['url']); //Call Funtion _link(); Parameters: transaction, Sidebar Last save id, Label, Url
                            }elseif($value['sidebar'] == 'News'){
                                _news($transaction, $sidebar->id, $value['limitnews']); //Call Funtion _saveimg(); Parameters: transaction, Sidebar Last save id, popular, views, newslist
                            }elseif($value['sidebar'] == 'Menu'){
                                _menu($transaction, $sidebar->id, $value['menuID']); //Call Funtion _saveimg(); Parameters: transaction, Sidebar Last save id, popular, views, newslist
                            }
                        }
                }
            }// END SIDEBAR FUNCTION 

        function _deletesidebar($id,$transaction,$location)
        {
             $workshopvenue = Pagesidebar::find("pageid='".$id."' and location= '".$location."' ");
                     foreach ($workshopvenue as $workshopvenue) {
                        $workshopvenue->setTransaction($transaction);
                        if (!$workshopvenue->delete()) {
                            $transaction->rollback('Cannot save service.');
                        }
                        $psbimg = Sidebarimg::find(array("sidebarid='".$workshopvenue->id."'"));
                        if ($psbimg) {
                         foreach ($psbimg as $psbimg) {
                            if (!$psbimg->delete()) {
                                $transaction->rollback('Cannot delete image.');
                                }
                            }
                        }
                        $psblink = Pagesidebarlink::find(array("sidebarid='".$workshopvenue->id."'"));
                        if ($psblink) {
                            foreach ($psblink as $psblink) {
                                                                if($psblink->delete()) {
                                    echo json_encode(array("result" => "Successfully Deleted!"));
                                } else {
                                    echo json_encode(array("result" => "There was an error!"));
                                    $transaction->rollback('Cannot delete link.');
                                }
                            }
                        }
                        $psbnews = Pagesidebarnews::find(array("sidebarid='".$workshopvenue->id."'"));
                        if ($psbnews) {
                            foreach ($psbnews as $psbnews) {
                               // if (!$psbnews->delete()) {
                               //  $transaction->rollback('Cannot save service.');
                               //  }
                                                                if($psbnews->delete()) {
                                    echo json_encode(array("result" => "Successfully Deleted!"));
                                } else {
                                    echo json_encode(array("result" => "There was an error!"));
                                    $transaction->rollback('Cannot delete news.');
                                }

                            }
                        }
                        $psbtesti = PageTestemonial::find(array("sidebarid='".$workshopvenue->id."'"));
                        if ($psbtesti) {
                            foreach ($psbtesti as $psbtesti) {
                                //  if (!$psbtesti->delete()) {
                                // $transaction->rollback('Cannot save service.');
                                // }
                                if($psbtesti->delete()) {
                                    echo json_encode(array("result" => "Successfully Deleted!"));
                                } else {
                                    echo json_encode(array("result" => "There was an error!"));
                                    $transaction->rollback('Cannot delete testimonial.');
                                }

                            }
                        }
                        $psbmenu = Pagesidebarmenu::find(array("sidebarid='".$workshopvenue->id."'"));
                        if ($psbmenu) {
                            foreach ($psbmenu as $psbmenu) {
                                if($psbmenu->delete()) {
                                    echo json_encode(array("result" => "Successfully Deleted!"));
                                } else {
                                    echo json_encode(array("result" => "There was an error!"));
                                    $transaction->rollback('Cannot delete menu.');
                                }

                            }
                        }

                }
        }
        function _deletemodule($pageid,$transaction)
        {
              $workshopvenue = Specialpagerow::find(array("pageid='".$pageid."'"));
                      foreach ($workshopvenue as $workshopvenue) {
                        $workshopvenue->setTransaction($transaction);
                        if (!$workshopvenue->delete()) {
                            $array = [];
                            foreach ($workshopvenue->getMessages() as $message) {
                                $array[] = $message;
                            }
                            $transaction->rollback('Cannot save service.');
                        }
                        $rowid=$workshopvenue->id;
                        $subconditions = 'rowid="' . $rowid . '"';
                        $specialpagecolumncontent = Specialpagecolumncontent::find(array($subconditions));
                        foreach ($specialpagecolumncontent as $specialpagecolumncontent) {
                            $specialpagecolumncontent->setTransaction($transaction);
                            if($specialpagecolumncontent->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot delete row.');
                            }
                        }

                        $textmodule = Textmodule::find(array($subconditions));
                        foreach ($textmodule as $textmodule) {
                            $textmodule->setTransaction($transaction);
                            if($textmodule->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot delete text.');
                            }
                        }

                        $moduleimage = Moduleimage::find(array($subconditions));
                        foreach ($moduleimage as $moduleimage) {
                            $moduleimage->setTransaction($transaction);
                            if($moduleimage->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot delete image.');
                            }
                        }
                        $moduletestimonial = Moduletestimonial::find(array($subconditions));
                        foreach ($moduletestimonial as $moduletestimonial) {
                            $moduletestimonial->setTransaction($transaction);
                            if($moduletestimonial->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot delete testimonial.');
                            }
                        }
                        $modulecontact = Modulecontact::find(array($subconditions));
                        foreach ($modulecontact as $modulecontact) {
                            $modulecontact->setTransaction($transaction);
                            if($modulecontact->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot delete contact.');
                            }
                        }
                        $modulenews = Modulenews::find(array($subconditions));
                        foreach ($modulenews as $modulenews) {
                            $modulenews->setTransaction($transaction);
                            if($modulenews->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot delete news.');
                            }
                        }
                         $moduledivider = Moduledivider::find(array($subconditions));
                        foreach ($moduledivider as $moduledivider) {
                            $moduledivider->setTransaction($transaction);
                            if($moduledivider->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot delete divider.');
                            }
                        }

                        $modulebutton = Modulebutton::find(array($subconditions));
                        foreach ($modulebutton as $modulebutton) {
                            $modulebutton->setTransaction($transaction);
                            if($modulebutton->delete()) {
                                // echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot delete button.');
                            }
                        }

                    }

        }


        $request = new \Phalcon\Http\Request();

        var_dump($request->isPost());
        if($request->isPost()){
            //TRY
            try{
                $title          = $request->getPost('title');
                $slugs          = $request->getPost('slugs');
                $metatitle      = $request->getPost('metatitle');
                $metatags       = $request->getPost('metakeyword');
                $metadesc       = $request->getPost('metadesc');
                $sidebarleft    = $request->getPost('sidebarleft');
                $sidebarright   = $request->getPost('sidebarright');
                $type           = $request->getPost('pagetype');
                $status         = ($request->getPost('status') == 'true' ? 1 : 0);
                $body           = ($type == 'Normal' ? $request->getPost('body') : '');
                $sortleft       = $request->getPost('sidedataL');
                $sortright      = $request->getPost('sidedataR');
                $count          = $request->getPost('column');

                $banner         = $request->getPost('banner');
                $imagethumb           = ($banner == 'true' ? $request->getPost('imagethumb') : '');
                $imagethumbsubtitle   = ($banner == 'true' ? $request->getPost('imagethumbsubtitle') : '');
                $thumbdesc            = ($banner == 'true' ? $request->getPost('thumbdesc') : '');
                $align                = ($banner == 'true' ? $request->getPost('align') : '');
                $bgcolor              = ($banner == 'true' ? $request->getPost('bgcolor') : '');
                $color                = ($banner == 'true' ? $request->getPost('color') : '');
                $box                  = ($banner == 'true' ? $request->getPost('box') : '');
                $btnname              = ($banner == 'true' ? $request->getPost('btnname') : '');
                $btnlink              = ($banner == 'true' ? $request->getPost('btnlink') : '');
                $trans                = ($box == 'true' ? $request->getPost('boxtrans') : '');
                $boxcolor             = ($box == 'true' ? $request->getPost('boxcolor') : '');
                $edge                 = ($box == 'true' ? $request->getPost('boxedge') : '');

                $transactionManager = new TransactionManager();
                $transaction        = $transactionManager->get();
                $id                 = $request->getPost('pageid');
                $page= Specialpage::findFirst(array("pageid='".$id."'"));
                $page->setTransaction($transaction);
                $page->assign(array(
                    'pageid'        => $id,
                    'title'         => $title,
                    'slugs'         => $slugs,
                    'metatitle'     => $metatitle,
                    'metakeyword'   => $metatags,
                    'metadesc'      => $metadesc,
                    'body'          => ($type == 'Normal' ? $body : ""),
                    'leftsidebar'   => $sidebarleft,
                    'rightsidebar'  => $sidebarright,
                    'pagetype'      => $type,
                    'banner'        => $banner,
                    'imagethumb'        => $imagethumb,
                    'imagethumbsubtitle'        => $imagethumbsubtitle,
                    'thumbdesc'        => $thumbdesc,
                    'align'        => $align,
                    'bgcolor'        => $bgcolor,
                    'color'        => $color,
                    'box'        => $box,
                    'btnname'       =>$btnname,
                    'btnlink'       =>$btnlink,
                    'boxtrans'      => $trans,
                    'boxcolor'      => $boxcolor,
                    'boxedge'       => $edge,
                    'status'        => $status,
                    'datecreated'   => date("Y-m-d H:i:s"),
                    'dateupdated'   => date("Y-m-d H:i:s")
                    ));
                if (!$page->save()) {
                    _ifsave($transaction, $page, "Can't Update Sidebar");
                }

                if($sidebarleft!='false'){
                    _deletesidebar($id,$transaction,'LEFT');
                    _sidebar($id, 'LEFT', $transaction, $sortleft);  
                }
                else{
                    _deletesidebar($id,$transaction,'LEFT');
                }
                if($sidebarright!=false){
                    _deletesidebar($id,$transaction,'RIGHT');
                    _sidebar($id, 'RIGHT', $transaction, $sortright);  
                }
                else{
                    _deletesidebar($id,$transaction,'RIGHT');
                }
                

                if($type=='Normal'){
                     _deletemodule($id,$transaction);
                }
                elseif($type=='Special'){
                            //Function Specialpagecolumncontent
                            function __specialpagecolumncontent($transaction, $rowid, $columnposition, $modules){
                                 $columncontent  = new Specialpagecolumncontent();
                                 $columncontent->setTransaction($transaction);
                                 $columncontent->assign(array(
                                    'rowid' =>     $rowid,
                                    'columnposition' => $columnposition,
                                    'module' => $modules
                                    ));
                                 (!$columncontent->save()? _ifsave($transaction, $columncontent, "Cannot save columncontent") : " ");
                            }
                            //Function text
                            function __text($transaction, $rowid, $colid, $content){
                                $TEXT  = new Textmodule();
                                $TEXT->setTransaction($transaction);
                                $TEXT->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'content' => $content
                                    ));
                                (!$TEXT->save()? _ifsave($transaction, $TEXT, "Cannot save TEXT") : " "); 
                            } 
                            //Function IMage
                            function __image($transaction, $rowid, $colid, $image,$link){
                               $IMAGE  = new Moduleimage();
                               $IMAGE->setTransaction($transaction);
                               $IMAGE->assign(array(
                                'rowid' => $rowid,
                                'colid' => $colid,
                                'image' => $image,
                                'link'  => $link
                                ));
                               (!$IMAGE->save()? _ifsave($transaction, $IMAGE, "Cannot save IMAGE") : " ");  
                            }
                            //Function TESTIMONIAL
                            function __testimonial($transaction, $rowid, $colid, $category, $limitcount){
                                $TESTIMONIAL  = new Moduletestimonial();
                                $TESTIMONIAL->setTransaction($transaction);
                                $TESTIMONIAL->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'category' => $category,
                                    'limitcount'  => $limitcount
                                    ));
                                (!$TESTIMONIAL->save()? _ifsave($transaction, $TESTIMONIAL, "Cannot save TESTIMONIAL") : " ");  
                            } 
                            //Function Contact
                            function __contact($transaction, $rowid, $colid, $title,$phone,$email,$hours,$address){
                                $CONTACT  = new Modulecontact();
                                $CONTACT->setTransaction($transaction);
                                $CONTACT->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'title' => $title,
                                    'phone' => $phone,
                                    'email' => $email,
                                    'hours' => $hours,
                                    'address' => $address
                                    ));
                                (!$CONTACT->save()? _ifsave($transaction, $CONTACT, "Cannot save CONTACT") : " ");
                            }
                            //Function NEWS
                            function __news($transaction, $rowid, $colid, $mostviewed, $popular, $limitcount){
                                $NEWS  = new Modulenews();
                                $NEWS->setTransaction($transaction);
                                $NEWS->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'mostviewed' => $mostviewed,
                                    'popular'  => $popular,
                                    'limitcount' => $limitcount
                                    ));
                                (!$NEWS->save()? _ifsave($transaction, $NEWS, "Cannot save NEWS") : " ");
                            }
                            //Function DEVIDER
                            function __devider($transaction, $rowid, $colid, $height, $color){
                                $DIVIDER  = new Moduledivider();
                                $DIVIDER->setTransaction($transaction);
                                $DIVIDER->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'height' => $height,
                                    'color'  => $color
                                    ));
                                (!$DIVIDER->save()? _ifsave($transaction, $DIVIDER, "Cannot save DIVIDER") : " ");
                            }
                             //Function BUTTON
                            function __button($transaction, $rowid, $colid, $btnname, $btnlink, $bgcolor, $fcolor,$padtop,$padside,$font,$position){
                                $BUTTON  = new Modulebutton();
                                $BUTTON->setTransaction($transaction);
                                $BUTTON->assign(array(
                                    'rowid' => $rowid,
                                    'colid' => $colid,
                                    'btnname' => $btnname,
                                    'btnlink'  => $btnlink,
                                    'bgcolor' => $bgcolor,
                                    'fcolor' => $fcolor,
                                    'padtop' => $padtop,
                                    'padside' => $padside,
                                    'font' => $font,
                                    'position' => $position
                                    ));
                                (!$BUTTON->save()? _ifsave($transaction, $BUTTON, "Cannot save BUTTON") : " ");
                            }

                    //start foreach
                     $colcount = $request->getPost('colcount');
                     if ($count) {
                                _deletemodule($id,$transaction);
                        }       
                     foreach ($count as $val){
                            $row = new Specialpagerow();
                            $row->setTransaction($transaction);
                            $guid = new \Utilities\Guid\Guid();
                            $rowid = $guid->GUID();
                            $row->assign(array(
                                'id' =>     $rowid,
                                'pageid' => $id,
                                'columntype' => $val['colcount'],
                                ));
                            (!$row->save()? _ifsave($transaction, $row, "Cannot save Row") : " ");

                            foreach ($val['row'] as $key => $value) {
                                __specialpagecolumncontent($transaction, $rowid, $key+1, $value['col']);
                                 //MODULE TEXT
                                ($value['col']=='TEXT' ? __text($transaction, $rowid, $key+1 ,$value['colcontent']) :''); 
                                    //MODULE IMAGE
                                ($value['col']=='IMAGE' ? __image($transaction, $rowid, $key+1, $value['colimg'], $value['colimglink']) : '');
                                    //MODULE TESTIMONIAL
                                ($value['col']=='TESTIMONIAL' ? __testimonial($transaction, $rowid, $key+1, $value['coltestimonialcat'], $value['coltestimonial']) : '');
                                    //MODULE CONTACT
                                ($value['col']=='CONTACT' ? __contact($transaction, $rowid, $key+1, $value['coltitle'], $value['colphone'],$value['colemail'],$value['colhours'],$value['coladdress']) : '');
                                    //MODULE NEWS
                                ($value['col']=='NEWS' ? __news($transaction, $rowid, $key+1, "dummy", "dummy", $value['colnewslimit']) : '');
                                     //MODULE DIVIDER
                                ($value['col']=='DIVIDER' ? __devider($transaction, $rowid, $key+1, $value['colheight'], $value['colcolor']) : '');   
                                    //MODULE BUTTON
                                ($value['col']=='BUTTON' ? __button($transaction, $rowid, $key+1, $value['colbtnname'], $value['colbtnlink'], $value['colbgcolor'], $value['colfcolor'], $value['colpadtop'], $value['colpadside'], $value['colfont'], $value['colposition']) : '');   

                            }
                    }//end foreach

                }//End IF Special Page
                 $transaction->commit();
            //CATCH   
            }catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }
            $response['200'] = "Success.";
            $response['data'] = array('id' => $page->pageid);
            die(json_encode($request->getPost('leftlinkurl')));
        }

} //ENDING

 public function listAction($page,$num,$keyword) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT * From specialpage';
        $sqlCount = 'SELECT COUNT(*) FROM specialpage';
        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlconcat = " WHERE specialpage.title LIKE '%" . $keyword . "%' or specialpage.pagetype LIKE '%" . $keyword . "%' or specialpage.slugs LIKE '%" . $keyword . "%'";
            $sql .= $sqlconcat;
            $sqlCount .= $sqlconcat;
        }
        else{
             $sqlconcat = " order by specialpage.datecreated desc";
             $sql .= $sqlconcat;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }

        $sql .= " LIMIT " . $offsetfinal . ",10";
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);
     echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
 }

public function infoAction($id) {
        $app = new CB();
        $sql = 'SELECT * From specialpage LEFT JOIN pagesidebar on specialpage.pageid=pagesidebar.pageid
        where specialpage.pageid = "'.$id.'" ';
        $normal = $app->dbSelect($sql);

        //SIDEBAR
        $lsb = Pagesidebar::find("pageid='".$id."' and location='LEFT'");
        $rsb = Pagesidebar::find("pageid='".$id."' and location='RIGHT'");
        foreach ($lsb as $value) {
            switch($value->sidebar){
            case 'Image':
            $column = Sidebarimg::findFirst("sidebarid='".$value->id."'");
            $leftsidebardata[]=array(
                'sidebar' => 'Image',
                'img' => $column->image,
                'imglink' =>$column->url
                );
            break;
            case 'Link':
            $column = Pagesidebarlink::findFirst("sidebarid='".$value->id."'");
            $leftsidebardata[]=array(
                'sidebar' => 'Link',
                'label' => $column->label,
                'url' =>$column->url
                );
            break;
            case 'News':
            $column = Pagesidebarnews::findFirst("sidebarid='".$value->id."'");
            $leftsidebardata[]=array(
                'sidebar' => 'News',
                'popular' => $column->popular,
                'views' =>$column->views,
                'limitnews' =>$column->limit
                );
            break;
            case 'Testimonial':
            $column = PageTestemonial::findFirst("sidebarid='".$value->id."'");
                 $leftsidebardata[]=array(
                            'sidebar' => 'Testimonial',
                            'limittest' => $column->limit
                        );
            break;
            case 'Menu':
            $column = Pagesidebarmenu::findFirst("sidebarid='".$value->id."'");
            $column2 = Menu::findFirst("menuID='".$column->menuID."'");
            $leftsidebardata[]=array(
                'sidebar' => 'Menu',
                'menuID' => $column->menuID,
                'name'=>$column2->name
                );
            break;
                default:
                  $leftsidebardata[] = array('sidebar' => $value->sidebar);
            break;
            }
        }
        foreach ($rsb as $value) {
            switch($value->sidebar){
            case 'Image':
            $column = Sidebarimg::findFirst("sidebarid='".$value->id."'");
            $rightsidebardata[]=array(
                'sidebar' => 'Image',
                'img' => $column->image,
                'imglink' =>$column->url
                );
            break;
            case 'Link':
            $column = Pagesidebarlink::findFirst("sidebarid='".$value->id."'");
            $rightsidebardata[]=array(
                'sidebar' => 'Link',
                'label' => $column->label,
                'url' =>$column->url
                );
            break;
            case 'News':
            $column = Pagesidebarnews::findFirst("sidebarid='".$value->id."'");
            $rightsidebardata[]=array(
                'sidebar' => 'News',
                'popular' => $column->popular,
                'views' =>$column->views,
                'limitnews' =>$column->limit
                );
            break;
            case 'Testimonial':
            $column = PageTestemonial::findFirst("sidebarid='".$value->id."'");
                 $rightsidebardata[]=array(
                            'sidebar' => 'Testimonial',
                            'limittest' => $column->limit
                        );
            break;
            case 'Menu':
            $column = Pagesidebarmenu::findFirst("sidebarid='".$value->id."'");
            $column2 = Menu::findFirst("menuID='".$column->menuID."'");
            $rightsidebardata[]=array(
                'sidebar' => 'Menu',
                'menuID' => $column->menuID,
                'name'=>$column2->name
                );
            break;
                default:
                  $rightsidebardata[] = array('sidebar' => $value->sidebar);
            break;
            }
        }

        //col1 fnction

        function columnmodule($rowid,$columnposition){
            $column = Specialpagecolumncontent::findFirst("rowid='".$rowid."' AND columnposition='".$columnposition."'");
                    switch ($column->module) {
                case 'TEXT':
                   $textmodule = Textmodule::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'content' => $textmodule->content,
                        );
                    break;
                    case 'IMAGE':
                    $imagemodule = Moduleimage::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'image' => $imagemodule->image,
                            'link' => $imagemodule->link
                        );
                    break;
                    case 'TESTIMONIAL':
                    $Moduletestimonial = Moduletestimonial::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'category' => $Moduletestimonial->category,
                            'limitcount' => $Moduletestimonial->limitcount
                        );
                    break;
                    case 'CONTACT':
                    $Modulecontact = Modulecontact::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'title' => $Modulecontact->title,
                            'phone' => $Modulecontact->phone,
                            'email' => $Modulecontact->email,
                            'hours' => $Modulecontact->hours,
                            'address' => $Modulecontact->address,
                            'str' => explode("\n", $Modulecontact->address)
                        );
                    break;
                    case 'NEWS':
                     $Modulenews = Modulenews::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'mostviewed' => $Modulenews->mostviewed,
                            'popular' => $Modulenews->popular,
                            'limitcount' => $Modulenews->limitcount
                        );
                    break;
                    case 'DIVIDER':
                         $Moduledivider = Moduledivider::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'height' => $Moduledivider->height,
                            'color' => $Moduledivider->color
                        );
                    break;
                    case 'BUTTON':
                         $Modulebutton = Modulebutton::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'module' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'btnname' => $Modulebutton->btnname,
                            'btnlink' => $Modulebutton->btnlink,
                            'bgcolor' => $Modulebutton->bgcolor,
                            'fcolor' => $Modulebutton->fcolor,
                            'padtop' => $Modulebutton->padtop,
                            'padside' => $Modulebutton->padside,
                            'font' => $Modulebutton->font,
                            'position' => $Modulebutton->position
                        );
                    break;
                
                default:
                    $datacol = array(
                            'module' => $column->module
                        );
                    break;
            }

                    return $datacol;
        }
        //MODULES
        $row = Specialpagerow::find("pageid='".$id."'");
        foreach ($row as $value) {
            $datarow[] = array(
                            'columntype' => $value->columntype
                        );
            if($value->columntype == '1'){
                    $datacol[] = array(columnmodule($value->id,'1'));

            }
            elseif($value->columntype == '2'){
                    $datacol[] = array(columnmodule($value->id,'1'),columnmodule($value->id,'2'));

            }
            elseif($value->columntype == '3'){
                 $datacol[] = array(columnmodule($value->id,'1'),columnmodule($value->id,'2'),columnmodule($value->id,'3'));

            }
            elseif($value->columntype == '4'){
                  $datacol[] = array(columnmodule($value->id,'1'),columnmodule($value->id,'2'),columnmodule($value->id,'3'),columnmodule($value->id,'4'));

            }
        }
        echo json_encode(array('normal'=>$normal,'leftsidebardata'=>$leftsidebardata,'rightsidebardata'=>$rightsidebardata,
        'datarow'=>$datarow,'datacol'=>$datacol),JSON_NUMERIC_CHECK);
            // echo json_encode($leftsidebardata);
}


public function deleteAction($id){
            try {
                $transactionManager = new TransactionManager();
                $transaction = $transactionManager->get();
                $conditions = 'pageid="' . $id . '"';
                $specialpage = Specialpage::findFirst(array($conditions));
                $specialpage->setTransaction($transaction);

                if (!$specialpage->delete()) {
                    $array = [];
                    foreach ($specialpage->getMessages() as $message) {
                        $array[] = $message;
                    }
                    $transaction->rollback('Cannot delete.');
                }

                $workshopvenue = Pagesidebar::find(array($conditions));
                foreach ($workshopvenue as $workshopvenue) {
                    $workshopvenue->setTransaction($transaction);
                    if(!$workshopvenue->delete()) {
                        echo json_encode(array("result" => "Error!"));
                         $transaction->rollback('Cannot delete sidebar.');
                    }
                    $psbimg = Sidebarimg::find(array("sidebarid='".$workshopvenue->id."'"));
                        if ($psbimg) {
                         foreach ($psbimg as $psbimg) {
                            if (!$psbimg->delete()) {
                                $transaction->rollback('Cannot delete image.');
                                }
                            }
                        }
                        $psblink = Pagesidebarlink::find(array("sidebarid='".$workshopvenue->id."'"));
                        if ($psblink) {
                            foreach ($psblink as $psblink) {
                                                                if($psblink->delete()) {
                                    echo json_encode(array("result" => "Successfully Deleted!"));
                                } else {
                                    echo json_encode(array("result" => "There was an error!"));
                                    $transaction->rollback('Cannot delete link.');
                                }
                            }
                        }
                        $psbnews = Pagesidebarnews::find(array("sidebarid='".$workshopvenue->id."'"));
                        if ($psbnews) {
                            foreach ($psbnews as $psbnews) {
                               if($psbnews->delete()) {
                                    echo json_encode(array("result" => "Successfully Deleted!"));
                                } else {
                                    echo json_encode(array("result" => "There was an error!"));
                                    $transaction->rollback('Cannot delete news.');
                                }

                            }
                        }
                        $psbtesti = PageTestemonial::find(array("sidebarid='".$workshopvenue->id."'"));
                        if ($psbtesti) {
                            foreach ($psbtesti as $psbtesti) {
                                if($psbtesti->delete()) {
                                    echo json_encode(array("result" => "Successfully Deleted!"));
                                } else {
                                    echo json_encode(array("result" => "There was an error!"));
                                    $transaction->rollback('Cannot delete testimonial.');
                                }

                            }
                        }
                        $psbmenu = Pagesidebarmenu::find(array("sidebarid='".$workshopvenue->id."'"));
                        if ($psbmenu) {
                            foreach ($psbmenu as $psbmenu) {
                                if($psbmenu->delete()) {
                                    echo json_encode(array("result" => "Successfully Deleted!"));
                                } else {
                                    echo json_encode(array("result" => "There was an error!"));
                                    $transaction->rollback('Cannot delete menu.');
                                }

                            }
                        } 
                    // else {
                    //     echo json_encode(array("result" => "There was an error!"));
                    //     $transaction->rollback('Cannot save service prices.');
                    // }
                }

                $specialpagerow = Specialpagerow::find(array($conditions));
                foreach ($specialpagerow as $specialpagerow) {
                    $specialpagerow->setTransaction($transaction);
                    if($specialpagerow->delete()) {
                        echo json_encode(array("result" => "Successfully Deleted!"));
                    } else {
                        echo json_encode(array("result" => "There was an error!"));
                        $transaction->rollback('Cannot save service prices.');
                    }

                      $rowid=$specialpagerow->id;
               $subconditions = 'rowid="' . $rowid . '"';

               $specialpagecolumncontent = Specialpagecolumncontent::find(array($subconditions));
                foreach ($specialpagecolumncontent as $specialpagecolumncontent) {
                    $specialpagecolumncontent->setTransaction($transaction);
                    if($specialpagecolumncontent->delete()) {
                        echo json_encode(array("result" => "Successfully Deleted!"));
                    } else {
                        echo json_encode(array("result" => "There was an error!"));
                        $transaction->rollback('Cannot save service prices.');
                    }
                }

                $textmodule = Textmodule::find(array($subconditions));
                foreach ($textmodule as $textmodule) {
                    $textmodule->setTransaction($transaction);
                    if($textmodule->delete()) {
                        echo json_encode(array("result" => "Successfully Deleted!"));
                    } else {
                        echo json_encode(array("result" => "There was an error!"));
                        $transaction->rollback('Cannot save service prices.');
                    }
                }

                 $moduleimage = Moduleimage::find(array($subconditions));
                foreach ($moduleimage as $moduleimage) {
                    $moduleimage->setTransaction($transaction);
                    if($moduleimage->delete()) {
                        echo json_encode(array("result" => "Successfully Deleted!"));
                    } else {
                        echo json_encode(array("result" => "There was an error!"));
                        $transaction->rollback('Cannot save service prices.');
                    }
                }
                $moduletestimonial = Moduletestimonial::find(array($subconditions));
                foreach ($moduletestimonial as $moduletestimonial) {
                    $moduletestimonial->setTransaction($transaction);
                    if($moduletestimonial->delete()) {
                        echo json_encode(array("result" => "Successfully Deleted!"));
                    } else {
                        echo json_encode(array("result" => "There was an error!"));
                        $transaction->rollback('Cannot save service prices.');
                    }
                }
                 $modulecontact = Modulecontact::find(array($subconditions));
                foreach ($modulecontact as $modulecontact) {
                    $modulecontact->setTransaction($transaction);
                    if($modulecontact->delete()) {
                        echo json_encode(array("result" => "Successfully Deleted!"));
                    } else {
                        echo json_encode(array("result" => "There was an error!"));
                        $transaction->rollback('Cannot save service prices.');
                    }
                }
                 $modulenews = Modulenews::find(array($subconditions));
                        foreach ($modulenews as $modulenews) {
                            $modulenews->setTransaction($transaction);
                            if($modulenews->delete()) {
                                echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot save service prices.');
                            }
                        }
                         $moduledivider = Moduledivider::find(array($subconditions));
                        foreach ($moduledivider as $moduledivider) {
                            $moduledivider->setTransaction($transaction);
                            if($moduledivider->delete()) {
                                echo json_encode(array("result" => "Successfully Deleted!"));
                            } else {
                                echo json_encode(array("result" => "There was an error!"));
                                $transaction->rollback('Cannot save service prices.');
                            }
                        }

                }
             
                $transaction->commit();
            }catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die(json_encode(array('401' => $e->getMessage())) );
            }
            $response['200'] = "Success.";
            $response['data'] = array('id' => $page->pageid);
            die(json_encode($response));

}

public function updatestatusAction($status,$pageid,$keyword) {
        $data = array();
        $page = Specialpage::findFirst('pageid="' . $pageid . '"');
        $page->status = $status;
        $page->dateupdated = date('Y-m-d H:i:s');
            if (!$page->save()) {
                $data['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
            }

            echo json_encode($data);
    }
public function getmenuAction() {
        $page = Menu::find();
        $page = json_encode($page->toArray(), JSON_NUMERIC_CHECK);
         echo $page;
    }
    public function loadnewsAction() {
                             $app = new CB();
                             $db = \Phalcon\DI::getDefault()->get('db');
                             $stmt = $db->prepare("SELECT news.title, news.newsid, news.newsslugs, news.imagethumb, news.videothumb, news.date, author.name FROM news LEFT JOIN author ON news.author=author.authorid WHERE status = 1 ORDER BY date DESC");

                             $stmt->execute();
                             $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                             foreach($searchresult as $sr=>$val){
                                $categories = $app->dbSelect("SELECT newscategory.categoryid, newscategory.categoryname FROM newscategory INNER JOIN newscat ON newscategory.categoryid = newscat.catid WHERE newscat.newsid ='".$val['newsid']."' ");
                                $searchresult[$sr]['category'] = $categories;
                             }
                             
          echo json_encode($searchresult);
    
    }
        public function loadtestiAction() {
            $app = new CB();
            $sql = "SELECT * FROM testimonials order by testimonials.date desc";
            $datatestimonial = $app->dbSelect($sql);

            foreach ($datatestimonial as $key => $v) {
                $testimonial[] = array(
                    'name' => $datatestimonial[$key]['name'],
                    'message' => $datatestimonial[$key]['message'],
                    'date' => $datatestimonial[$key]['date'],
                    'picture' => $datatestimonial[$key]['picture'],
                    'pageid' => $datatestimonial[$key]['pageid']
                    );
            }
            $news = News::find(array('status' => 1, 'order' => 'date DESC'));
            $dates = [];
            foreach ($news as $news)
            {
                if(!in_array(date('F Y', strtotime($news->date)), $dates)){
                    $archive[] = array(
                        'month' => date('F', strtotime($news->date)),
                        'year' => date('Y', strtotime($news->date))
                        );
                    array_push($dates, date('F Y', strtotime($news->date)));
                }
            }
                            //menu
            $sql = "SELECT * FROM menu group by menuID";
            $searchresult = $app->dbSelect($sql);

            $sql2 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 0 ORDER BY menustructure.num ASC";
            $searchresult2 = $app->dbSelect($sql2);
            $sql3 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 1 ";
            $searchresult3 = $app->dbSelect($sql3);
            $sql4 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 2 ";
            $searchresult4 = $app->dbSelect($sql4);
            $sql5 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 3 ";
            $searchresult5 = $app->dbSelect($sql5);
            $sql6 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 4 ";
            $searchresult6 = $app->dbSelect($sql6);

            foreach ($searchresult2 as $key => $value) {
                foreach ($searchresult3 as $kk => $value) {
                    if ($searchresult3[$kk]['parent']==$searchresult2[$key]['submenuID']) {
                        if ($searchresult3[$kk]['child']=="1")
                        {
                            $childid1[]=array(
                                'submenuID'=>$searchresult3[$kk]['submenuID'],
                                'menuID'=>$searchresult3[$kk]['menuID'],
                                'child'=>$searchresult3[$kk]['child'],
                                'submenuID'=>$searchresult3[$kk]['submenuID'],
                                'parent'=>$searchresult3[$kk]['parent'],
                                'order'=>$searchresult3[$kk]['order'],
                                'subname'=>$searchresult3[$kk]['subname'],
                                'sublink'=>$searchresult3[$kk]['sublink'],
                                'newtab'=>$searchresult3[$kk]['newtab'],
                                'sort'=>$searchresult3[$kk]['sort']
                                );

                        }

                    }

                }
            }

            foreach ($searchresult4 as $key4 => $value) {
                foreach ($childid1 as $key1 => $value) {
                   
                 if ($searchresult4[$key4]['parent']==$childid1[$key1]['submenuID']) {
                    $childid2[]=array(
                        'submenuID'=>$searchresult4[$key4]['submenuID'],
                        'menuID'=>$searchresult4[$key4]['menuID'],
                        'child'=>$searchresult4[$key4]['child'],
                        'submenuID'=>$searchresult4[$key4]['submenuID'],
                        'parent'=>$searchresult4[$key4]['parent'],
                        'order'=>$searchresult4[$key4]['order'],
                        'subname'=>$searchresult4[$key4]['subname'],
                        'sublink'=>$searchresult4[$key4]['sublink'],
                        'newtab'=>$searchresult4[$key4]['newtab'],
                        'sort'=>$searchresult4[$key4]['sort']
                        );

                }
            }

        }

        foreach ($searchresult5 as $key5 => $value) {
            foreach ($childid2 as $key2 => $value) {
                if ($searchresult5[$key5]['parent']==$childid2[$key2]['submenuID']) {
                    $childid3[]=array(
                        'submenuID'=>$searchresult5[$key5]['submenuID'],
                        'menuID'=>$searchresult5[$key5]['menuID'],
                        'child'=>$searchresult5[$key5]['child'],
                        'submenuID'=>$searchresult5[$key5]['submenuID'],
                        'parent'=>$searchresult5[$key5]['parent'],
                        'order'=>$searchresult5[$key5]['order'],
                        'subname'=>$searchresult5[$key5]['subname'],
                        'sublink'=>$searchresult5[$key5]['sublink'],
                        'newtab'=>$searchresult5[$key5]['newtab'],
                        'sort'=>$searchresult5[$key5]['sort']
                        );
                }

            }
        }
        
        foreach ($searchresult6 as $key6 => $value) {
            foreach ($childid3 as $key3 => $value) {
                if ($searchresult6[$key6]['parent']==$childid3[$key3]['submenuID']) {
                    $childid4[]=array(
                        'submenuID'=>$searchresult6[$key6]['submenuID'],
                        'menuID'=>$searchresult6[$key6]['menuID'],
                        'child'=>$searchresult6[$key6]['child'],
                        'submenuID'=>$searchresult6[$key6]['submenuID'],
                        'parent'=>$searchresult6[$key6]['parent'],
                        'order'=>$searchresult6[$key6]['order'],
                        'subname'=>$searchresult6[$key6]['subname'],
                        'sublink'=>$searchresult6[$key6]['sublink'],
                        'newtab'=>$searchresult6[$key6]['newtab'],
                        'sort'=>$searchresult6[$key6]['sort']
                        );
                }

            }
        }
        $children[] = array(
            'child1' => $childid1,
            'child2' => $childid2,
            'child3' => $childid3,
            'child4' => $childid4
            );
        echo json_encode(array('testi'=>$testimonial,'archive'=>$archive,'children'=>$children,'shortcode'=>$searchresult2));
        
    }


//FRONT END
public function specialpageAction($pageslugs){
        $app = new CB;
        $page = Specialpage::findFirst("slugs ='".$pageslugs."'");
        $data1 = array(
                'status' => $page->status
            );
            echo json_encode(array("data"=>$data1));
            // break;
    }


public function specialAction($pageslugs) {
        $p = Specialpage::findFirst("slugs='".$pageslugs."'");
        $id = $p->pageid;
        $app = new CB();
        $sql = 'SELECT * From specialpage where specialpage.slugs = "'.$pageslugs.'" ';
        $normal = $app->dbSelect($sql);
       
        $lsb = Pagesidebar::find("pageid='".$id."' and location='LEFT'");
        $rsb = Pagesidebar::find("pageid='".$id."' and location='RIGHT'");
        foreach ($lsb as $value) {
            switch($value->sidebar){
            case 'Image':
            $column = Sidebarimg::findFirst("sidebarid='".$value->id."'");
            $leftsidebardata[]=array(
                'sidebar' => 'Image',
                'img' => $column->image,
                'imglink' =>$column->url
                );
            break;
            case 'Link':
            $column = Pagesidebarlink::findFirst("sidebarid='".$value->id."'");
            $leftsidebardata[]=array(
                'sidebar' => 'Link',
                'label' => $column->label,
                'url' =>$column->url
                );
            break;
            case 'News':
            $column = Pagesidebarnews::findFirst("sidebarid='".$value->id."'");
            $leftsidebardata[]=array(
                'sidebar' => 'News',
                'popular' => $column->popular,
                'views' =>$column->views,
                'limitnews' =>$column->limit
                );
            break;
            case 'Testimonial':
            $column = PageTestemonial::findFirst("sidebarid='".$value->id."'");
                 $leftsidebardata[]=array(
                            'sidebar' => 'Testimonial',
                            'limittest' => $column->limit
                        );
            break;
            case 'Menu':
            $column = Pagesidebarmenu::findFirst("sidebarid='".$value->id."'");
            $column2 = Menu::findFirst("menuID='".$column->menuID."'");
            $leftsidebardata[]=array(
                'sidebar' => 'Menu',
                'menuID' => $column->menuID,
                'name'=>$column2->name
                );
            break;
                default:
                  $leftsidebardata[] = array('sidebar' => $value->sidebar);
            break;
            }
          
        }
        foreach ($rsb as $value) {
            switch($value->sidebar){
            case 'Image':
            $column = Sidebarimg::findFirst("sidebarid='".$value->id."'");
            $rightsidebardata[]=array(
                'sidebar' => 'Image',
                'img' => $column->image,
                'imglink' =>$column->url
                );
            break;
            case 'Link':
            $column = Pagesidebarlink::findFirst("sidebarid='".$value->id."'");
            $rightsidebardata[]=array(
                'sidebar' => 'Link',
                'label' => $column->label,
                'url' =>$column->url
                );
            break;
            case 'News':
            $column = Pagesidebarnews::findFirst("sidebarid='".$value->id."'");
            $rightsidebardata[]=array(
                'sidebar' => 'News',
                'popular' => $column->popular,
                'views' =>$column->views,
                'limitnews' =>$column->limit
                );
            break;
            case 'Testimonial':
            $column = PageTestemonial::findFirst("sidebarid='".$value->id."'");
                 $rightsidebardata[]=array(
                            'sidebar' => 'Testimonial',
                            'limittest' => $column->limit
                        );
            break;
            case 'Menu':
            $column = Pagesidebarmenu::findFirst("sidebarid='".$value->id."'");
            $column2 = Menu::findFirst("menuID='".$column->menuID."'");
            $rightsidebardata[]=array(
                'sidebar' => 'Menu',
                'menuID' => $column->menuID,
                'name'=>$column2->name
                );
            break;
                default:
                  $rightsidebardata[] = array('sidebar' => $value->sidebar);
            break;
            }
        }

        // //col1 fnction

        function columnmodule($rowid,$columnposition){
            $column = Specialpagecolumncontent::findFirst("rowid='".$rowid."' AND columnposition='".$columnposition."'");
            switch ($column->module) {
                case 'TEXT':
                   $textmodule = Textmodule::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'content' => $textmodule->content,
                        );
                    break;
                    case 'IMAGE':
                    $imagemodule = Moduleimage::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'image' => $imagemodule->image,
                            'link' => $imagemodule->link
                        );
                    break;
                    case 'TESTIMONIAL':
                    $Moduletestimonial = Moduletestimonial::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'category' => $Moduletestimonial->category,
                            'limitcount' => $Moduletestimonial->limitcount
                        );
                    break;
                    case 'CONTACT':
                    $Modulecontact = Modulecontact::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'title' => $Modulecontact->title,
                            'phone' => $Modulecontact->phone,
                            'email' => $Modulecontact->email,
                            'hours' => $Modulecontact->hours,
                            'address' => $Modulecontact->address,
                            'str' => explode("\n", $Modulecontact->address)
                        );
                    break;
                    case 'NEWS':
                     $Modulenews = Modulenews::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'mostviewed' => $Modulenews->mostviewed,
                            'popular' => $Modulenews->popular,
                            'limitcount' => $Modulenews->limitcount
                        );
                    break;
                    case 'DIVIDER':
                         $Moduledivider = Moduledivider::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'height' => $Moduledivider->height,
                            'color' => $Moduledivider->color
                        );
                    break;
                    case 'BUTTON':
                         $Modulebutton = Modulebutton::findFirst("rowid='".$rowid."' AND colid='".$columnposition."'");
                         $datacol = array(
                            'col' => $column->module,
                            'columnposition' =>$column->columnposition,
                            'btnname' => $Modulebutton->btnname,
                            'btnlink' => $Modulebutton->btnlink,
                            'bgcolor' => $Modulebutton->bgcolor,
                            'fcolor' => $Modulebutton->fcolor,
                            'padtop' => $Modulebutton->padtop,
                            'padside' => $Modulebutton->padside,
                            'font' => $Modulebutton->font,
                            'position' => $Modulebutton->position
                        );
                    break;
                
                default:
                    $datacol = array(
                            'module' => $column->module
                        );
                    break;
            }

                    return $datacol;
        }
        //MODULES
        $row = Specialpagerow::find("pageid='".$id."'");
        foreach ($row as $value) {
            switch($value->columntype){
            case '1':
            $datacol[] = array('type'=>$value->columntype,columnmodule($value->id,'1'));
            break;
            case '2':
             $datacol[] = array('type'=>$value->columntype,columnmodule($value->id,'1'),columnmodule($value->id,'2'));
            break;
            case '3':
             $datacol[] = array('type'=>$value->columntype,columnmodule($value->id,'1'),columnmodule($value->id,'2'),columnmodule($value->id,'3'));
            break;
            case '4':
             $datacol[] = array('type'=>$value->columntype,columnmodule($value->id,'1'),columnmodule($value->id,'2'),columnmodule($value->id,'3'),columnmodule($value->id,'4'));
            break;
            default:
             $datacol[] = array('type'=>$value->columntype,columnmodule($value->id,'1'));
            break;
        }

        
}
        echo json_encode(array('normal'=>$normal,'leftsidebardata'=>$leftsidebardata,'rightsidebardata'=>$rightsidebardata,
        'datacol'=>$datacol),JSON_NUMERIC_CHECK);
            
            // echo json_encode($leftsidebardata);
}


}
