<?php

namespace Controllers;


use \Models\Booking as Booking;
use \Models\Bookingreply as Bookingreply;
use \Models\Reservations as Reservations;
use \Models\Schedules as Schedules;
use \Models\Members as Members;
use \Models\Invoices as Invoices;
use \Models\Pages as Pages;
use \Models\Pagecategory as Pagecategory;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use \Models\News as News;

class ReportsController extends \Phalcon\Mvc\Controller {

 public function salesreportAction($num, $page, $startdate, $enddate,$status, $service, $subservice,$prices,$payment,$members){
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT reservations.paymenttype, schedules.id as schedid, schedules.startdatetime,  schedules.enddatetime,  schedules.scheduledate, schedules.status, members.firstname, members.lastname, members.email, schedules.updated_at, schedules.created_at, schedules.hourday, schedules.description, reservations.invoiceno,  pages.title, schedules.created_at, pagecategory.colorlegend,pagecategory.id  FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category=pagecategory.id INNER JOIN serviceprices ON serviceprices.serviceid = pages.pageid ';
        $sqlCount = 'SELECT COUNT(*) FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category=pagecategory.id ';
        $sqlWhereKey = '';
        $sqlWhereDate = '';
        $sqlWhereStatus = '';
        $where ='';

   
        // $sql .= $where;
        // $sqlCount .= $where;

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }

       
        // getting the query
if ($startdate==undefined and $status == undefined and $service==undefined and $subservice==undefined and $prices == undefined and $payment == undefined and $members == undefined)
    {
        $sql .= " GROUP BY schedules.id ";
        $sql .= " ORDER BY schedules.created_at DESC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        $searchresult = $app->dbSelect($sql);
        $totalreportdirty = $app->dbSelect($sqlCount);
     }

  else if ($status == undefined and $service==undefined and $subservice==undefined and $prices == undefined and $payment == undefined and $members == undefined)
    {
    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
    $searchresult = $app->dbSelect($sql);
    $sql .= " GROUP BY schedules.id ";
    $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
    }

    else if ($startdate == undefined and $service==undefined and $payment == undefined and $members == undefined)
    {
        $sql .= "where schedules.status = '".$status."'";
        $sql .= " GROUP BY schedules.id ";
        $sql .= " ORDER BY schedules.created_at DESC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        $searchresult = $app->dbSelect($sql);
        $sqlCount .= "where schedules.status = '".$status."'";
        $totalreportdirty = $app->dbSelect($sqlCount);
    }

    else if ($service!= undefined and $startdate == undefined and $status==undefined and $payment == undefined and $members == undefined)
    {
        if ($subservice == undefined)
        {
            $sql.= "where pagecategory.id = '".$service."'";
            $sql .= " GROUP BY schedules.id ";
           $sql .= " ORDER BY schedules.created_at DESC ";
           $sql .= " LIMIT " . $offsetfinal . ",10";
           $searchresult = $app->dbSelect($sql);
           $sqlCount .= "where pagecategory.id = '".$service."'";
           $totalreportdirty = $app->dbSelect($sqlCount);
        }
        else
        {
            if($prices == undefined)
            {
            $sql .= "where pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
            $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .="where pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
            }
            else
            {
            $sql .= "where pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
            $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .="where pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
            }

        }
   
    }
    else if ($startdate == undefined and $status==undefined and $service==undefined and $members == undefined)
    {
    $sql .= "where reservations.paymenttype = '".$payment."'";
    $sql .= " GROUP BY schedules.id ";
    $sql .= " ORDER BY schedules.created_at DESC ";
    $sql .= " LIMIT " . $offsetfinal . ",10";
    $searchresult = $app->dbSelect($sql);
    $sqlCount .="where reservations.paymenttype = '".$payment."'";
    $totalreportdirty = $app->dbSelect($sqlCount);
    }
    else if ($startdate == undefined and $status==undefined and $service==undefined and $payment == undefined)
    {
    $sql .= "where members.memberid = '".$members."'";
    $sql .= " GROUP BY schedules.id ";
    $sql .= " ORDER BY schedules.created_at DESC ";
    $sql .= " LIMIT " . $offsetfinal . ",10";
    $searchresult = $app->dbSelect($sql);
    $sqlCount .= "where members.memberid = '".$members."'";
    $totalreportdirty = $app->dbSelect($sqlCount);
    }



    //DATE not empty
    else if ($startdate!=undefined)
    {
        //status not empty
        if ($status != undefined and $service == undefined and $payment == undefined and $members == undefined)
        {
            $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
            $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
        }
        //service not empty
        else if ($service != undefined and $status == undefined and $payment == undefined and $members == undefined)
        {
           if ($subservice == undefined)
            {
            $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
            $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .=  "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }

         }
         //payment not empty
         else if ($payment != undefined and $service == undefined and $status == undefined and $members == undefined)
         {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and reservations.paymenttype = '".$payment."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and reservations.paymenttype = '".$payment."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
         }
         //members not empty
         else if ($members != undefined and $service == undefined and $payment == undefined and $status == undefined)
         {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and members.memberid = '".$members."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and members.memberid = '".$members."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
         }

         //Status and service not empty
         else if ($status != undefined and $service != undefined and $payment == undefined and $members == undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
            $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .=  "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }


         }

         //Status, and payment not empty
         else if ($status != undefined and $service == undefined and $payment != undefined and $members == undefined)
         {
                $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .=  "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
         }

         //Status, and members not empty
         else if ($status != undefined and $service == undefined and $payment == undefined and $members != undefined)
         {
                $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .=  "where members.memberid = '".$members."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
         }

         //Status, service, and payment not empty
         else if ($status != undefined and $service != undefined and $payment != undefined and $members == undefined)
         {
             if ($subservice == undefined)
             {
                $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .=  "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
            }
            else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                     $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }

         }

         // all are not empty
        else if ($status != undefined and $service != undefined and $payment != undefined and $members != undefined)
         {
             if ($subservice == undefined)
             {
                $sql .= "where members.memberid = '".$members."' and reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .=  "where members.memberid = '".$members."' and reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
            }
            else
            {
                if($prices == undefined)
                {
                    $sql .= "where members.memberid = '".$members."' and reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where members.memberid = '".$members."' and reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where members.memberid = '".$members."' and reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where members.memberid = '".$members."' and reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }

         }

         //service,payment not empty
         else if ($status == undefined and $service != undefined and $payment != undefined and $members == undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where reservations.paymenttype = '".$payment."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
             $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .=  "where reservations.paymenttype = '".$payment."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                     $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where reservations.paymenttype = '".$payment."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where reservations.paymenttype = '".$payment."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }


         }

         //service,member not empty
         else if ($status == undefined and $service != undefined and $payment == undefined and $members != undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where members.memberid = '".$members."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
            $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where members.memberid = '".$members."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where members.memberid = '".$members."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where members.memberid = '".$members."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where members.memberid = '".$members."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where members.memberid = '".$members."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }


         }

         //status, service, member not empty
         else if ($status != undefined and $service != undefined and $payment == undefined and $members != undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."'";
                $sql .= " GROUP BY schedules.id ";
              $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and  members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $sql .= " GROUP BY schedules.id ";
                   $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and  members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and  members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and  members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }


         }


    } //End date condition


//start status condition
else if ($status!=undefined)
    {
        //date not empty
        if ($startdate != undefined and $service == undefined and $payment == undefined and $members == undefined)
        {
            $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
        }
        //service not empty
        else if ($service != undefined and $startdate == undefined and $payment == undefined and $members == undefined)
        {
           if ($subservice == undefined)
            {
            $sql .= "where schedules.status = '".$status."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .=  "where schedules.status = '".$status."' and pagecategory.id = '".$service."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=   "where schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=   "where schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }

         }
         //payment not empty
         else if ($payment != undefined and $service == undefined and $startdate == undefined and $members == undefined)
         {
                    $sql .= "where schedules.status = '".$status."' and reservations.paymenttype = '".$payment."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where schedules.status = '".$status."' and reservations.paymenttype = '".$payment."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
         }
         //members not empty
         else if ($members != undefined and $service == undefined and $payment == undefined and $startdate == undefined)
         {
                    $sql .= "where schedules.status = '".$status."' and members.memberid = '".$members."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where schedules.status = '".$status."' and members.memberid = '".$members."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
         }

         //date and service not empty
         else if ($startdate != undefined and $service != undefined and $payment == undefined and $members == undefined)
         {

            if ($subservice == undefined)
            {
                $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }


         }

         //date, and payment not empty
         else if ($startdate != undefined and $service == undefined and $payment != undefined and $members == undefined)
         {
                $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
         }

         //date, and members not empty
         else if ($startdate != undefined and $service == undefined and $payment == undefined and $members != undefined)
         {
                $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .=  "where members.memberid = '".$members."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
         }

//member and payment
        else if ($startdate == undefined and $service == undefined and $payment != undefined and $members != undefined)
         {
                $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and reservations.paymenttype = '".$payment."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .=  "where members.memberid = '".$members."' and schedules.status = '".$status."' and reservations.paymenttype = '".$payment."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
         }


         //date, service, and payment not empty
         else if ($startdate != undefined and $service != undefined and $payment != undefined and $members == undefined)
         {
             if ($subservice == undefined)
             {
                $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .=  "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
            }
            else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }

         }

         //service,payment not empty
         else if ($startdate == undefined and $service != undefined and $payment != undefined and $members == undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and pagecategory.id = '".$service."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                          $sql .= " GROUP BY schedules.id ";
                     $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }


         }

         //service,member not empty
         else if ($startdate == undefined and $service != undefined and $payment == undefined and $members != undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                     $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }


         }

         //service,payment, member not empty
         else if ($startdate == undefined and $service != undefined and $payment != undefined and $members != undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and schedules.status >= '".$status."' and pagecategory.id = '".$service."'";
                $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and schedules.status >= '".$status."' and pagecategory.id = '".$service."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }


         }


    } //Status condition


//start service condition
else if ($service!=undefined)
    {
        if ($payment != undefined and $members == undefined)
        {
              if ($subservice == undefined)
            {
            $sql .= "where reservations.paymenttype = '".$payment."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .= "where reservations.paymenttype = '".$payment."' and pagecategory.id = '".$service."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                        $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .="where reservations.paymenttype = '".$payment."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .="where reservations.paymenttype = '".$payment."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }



        }

        else if($members != undefined and $payment == undefined)
        {
              if ($subservice == undefined)
            {
            $sql .= "where members.memberid = '".$members."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .="where members.memberid = '".$members."' and pagecategory.id = '".$service."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where members.memberid = '".$members."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .="where members.memberid = '".$members."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where  members.memberid = '".$members."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .="where  members.memberid = '".$members."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }

        }

         else if($members != undefined and $payment !=undefined)
        {
              if ($subservice == undefined)
            {
            $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .="where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and pagecategory.id = '".$service."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .="where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .="where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
                }

            }

        }


    } //end service condition


//start payment condition
    else if ($payment!=undefined)
    {
            if($members != undefined)
        {
             $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' ";
                  $sql .= " GROUP BY schedules.id ";
             $sql .= " ORDER BY schedules.created_at DESC ";
             $sql .= " LIMIT " . $offsetfinal . ",10";
             $searchresult = $app->dbSelect($sql);
             $sqlCount .="where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' ";
             $totalreportdirty = $app->dbSelect($sqlCount);
             
        }


    }
       
        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));

}


        public function servicelistAction()
    {
        $getservice = pagecategory::find();
        if(count($getservice) == 0){
            $data['error']=array('NODATA');
        }else{
        foreach ($getservice as $get) 
        {
            $data[] = array(
                'title'=>$get->title,
                'id'=>$get->id
                );
        }
      
    }
        echo json_encode($data);
    }

     public function subserviceAction($serv)
    {
          $app = new CB();
          $sql = 'Select pagecategory.id, pagecategory.title, pages.title as pt, pages.pageid as p_id, pages.category from pagecategory inner join pages on pagecategory.id = pages.category ';
          $sql .= "where pagecategory.id = '".$serv."' ";
          $result = $app->dbSelect($sql);
          foreach ($result as $get) 
            {

            $data[] = array(
                'pt'=>$get['pt'],
                'p_id'=>$get['p_id']
                );
            }
       
        echo json_encode($data);
    }

    public function pricelistAction($subserv)
    {
          $app = new CB();
          $sql = 'Select * from serviceprices ';
          $sql .= "where serviceid = '".$subserv."' ";
          $result = $app->dbSelect($sql);
          foreach ($result as $get) 
            {

            $data[] = array(
                'price'=>$get['price'],
                'serviceid'=>$get['serviceid']
                );
            }
       
        echo json_encode($data);
    }

    public function memberslistAction()
    {
        $getmembers = members::find();
        if(count($getmembers) == 0){
            $data['error']=array('NODATA');
        }else{
        foreach ($getmembers as $get) 
        {
            $data[] = array(
                'firstname'=>$get->firstname,
                'lastname'=>$get->lastname,
                'memberid'=>$get->memberid,
                );
        }
    }
        echo json_encode($data);
    }



 public function salesreportdummyAction(){
        $app = new CB();
        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT reservations.paymenttype, schedules.id as schedid, schedules.startdatetime,  schedules.enddatetime,  schedules.scheduledate, schedules.status, members.firstname, members.lastname, members.email, schedules.updated_at, schedules.created_at, schedules.hourday, schedules.description, reservations.invoiceno,  pages.title, schedules.created_at, pagecategory.colorlegend,pagecategory.id  FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category=pagecategory.id INNER JOIN serviceprices ON serviceprices.serviceid = pages.pageid ';
        $sql .= " GROUP BY schedules.id ";
        $sql .= " ORDER BY schedules.created_at DESC ";
        $searchresult = $app->dbSelect($sql);
        echo json_encode($searchresult);
}


 public function searchreportdummyAction($startdate, $enddate,$status, $service, $subservice,$prices,$payment,$members){
$app = new CB();
$sql = 'SELECT reservations.paymenttype, schedules.id as schedid, schedules.startdatetime,  schedules.enddatetime,  schedules.scheduledate, schedules.status, members.firstname, members.lastname, members.email, schedules.updated_at, schedules.created_at, schedules.hourday, schedules.description, reservations.invoiceno,  pages.title, schedules.created_at, pagecategory.colorlegend,pagecategory.id  FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category=pagecategory.id INNER JOIN serviceprices ON serviceprices.serviceid = pages.pageid ';
  if ($status == undefined and $service==undefined and $subservice==undefined and $prices == undefined and $payment == undefined and $members == undefined)
    {
    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
    $sql .= " GROUP BY schedules.id ";
    $sql .= " ORDER BY schedules.created_at DESC ";
    $searchresult = $app->dbSelect($sql);
 
    }

    else if ($startdate == undefined and $service==undefined and $payment == undefined and $members == undefined)
    {
        $sql .= "where schedules.status = '".$status."'";
        $sql .= " GROUP BY schedules.id ";
        $sql .= " ORDER BY schedules.created_at DESC ";
        $searchresult = $app->dbSelect($sql);
    }

    else if ($service!= undefined and $startdate == undefined and $status==undefined and $payment == undefined and $members == undefined)
    {
        if ($subservice == undefined)
        {
        $sql.= "where pagecategory.id = '".$service."'";
        $sql .= " GROUP BY schedules.id ";
           $sql .= " ORDER BY schedules.created_at DESC ";
           $searchresult = $app->dbSelect($sql);
     
        }
        else
        {
            if($prices == undefined)
            {
            $sql .= "where pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
            $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
       
            }
            else
            {
            $sql .= "where pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
            $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
            }

        }
   
    }
    else if ($startdate == undefined and $status==undefined and $service==undefined and $members == undefined)
    {
    $sql .= "where reservations.paymenttype = '".$payment."'";
    $sql .= " GROUP BY schedules.id ";
    $sql .= " ORDER BY schedules.created_at DESC ";
    $searchresult = $app->dbSelect($sql);
    }
    else if ($startdate == undefined and $status==undefined and $service==undefined and $payment == undefined)
    {
    $sql .= "where members.memberid = '".$members."'";
    $sql .= " GROUP BY schedules.id ";
    $sql .= " ORDER BY schedules.created_at DESC ";
    $searchresult = $app->dbSelect($sql);
    }

    //DATE not empty
    else if ($startdate!=undefined)
    {
        //status not empty
        if ($status != undefined and $service == undefined and $payment == undefined and $members == undefined)
        {
            $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
            $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
 
        }
        //service not empty
        else if ($service != undefined and $status == undefined and $payment == undefined and $members == undefined)
        {
           if ($subservice == undefined)
            {
            $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
            $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }
                else
                {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }

            }

         }
         //payment not empty
         else if ($payment != undefined and $service == undefined and $status == undefined and $members == undefined)
         {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and reservations.paymenttype = '".$payment."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
         }
         //members not empty
         else if ($members != undefined and $service == undefined and $payment == undefined and $status == undefined)
         {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and members.memberid = '".$members."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

         }
         //Status and service not empty
         else if ($status != undefined and $service != undefined and $payment == undefined and $members == undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                  $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }
                else
                {
                    $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                     $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }
            }
         }

         //Status, and payment not empty
         else if ($status != undefined and $service == undefined and $payment != undefined and $members == undefined)
         {
                $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);
         }

         //Status, and members not empty
         else if ($status != undefined and $service == undefined and $payment == undefined and $members != undefined)
         {
                $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                    $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);

         }

         //Status, service, and payment not empty
         else if ($status != undefined and $service != undefined and $payment != undefined and $members == undefined)
         {
             if ($subservice == undefined)
             {
                $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);
            }
            else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
               $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                }

            }

         }

         // all are not empty
        else if ($status != undefined and $service != undefined and $payment != undefined and $members != undefined)
         {
             if ($subservice == undefined)
             {
                $sql .= "where members.memberid = '".$members."' and reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
            }
            else
            {
                if($prices == undefined)
                {
                    $sql .= "where members.memberid = '".$members."' and reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
               $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }
                else
                {
                    $sql .= "where members.memberid = '".$members."' and reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }

            }

         }

         //service,payment not empty
         else if ($status == undefined and $service != undefined and $payment != undefined and $members == undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where reservations.paymenttype = '".$payment."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
      $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
     $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
     $sql .= " GROUP BY schedules.id ";
                     $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }

            }


         }

         //service,member not empty
         else if ($status == undefined and $service != undefined and $payment == undefined and $members != undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where members.memberid = '".$members."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
     $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where members.memberid = '".$members."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
     $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }
                else
                {
                    $sql .= "where members.memberid = '".$members."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
     $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }

            }


         }

         //status, service, member not empty
         else if ($status != undefined and $service != undefined and $payment == undefined and $members != undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."'";
     $sql .= " GROUP BY schedules.id ";
              $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and  members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
     $sql .= " GROUP BY schedules.id ";
                   $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }
                else
                {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and  members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
     $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }

            }


         }


    } //End date condition


//start status condition
else if ($status!=undefined)
    {
        //date not empty
        if ($startdate != undefined and $service == undefined and $payment == undefined and $members == undefined)
        {
            $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
        }
        //service not empty
        else if ($service != undefined and $startdate == undefined and $payment == undefined and $members == undefined)
        {
           if ($subservice == undefined)
            {
            $sql .= "where schedules.status = '".$status."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }
                else
                {
                    $sql .= "where schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }

            }

         }
         //payment not empty
         else if ($payment != undefined and $service == undefined and $startdate == undefined and $members == undefined)
         {
                    $sql .= "where schedules.status = '".$status."' and reservations.paymenttype = '".$payment."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
         }
         //members not empty
         else if ($members != undefined and $service == undefined and $payment == undefined and $startdate == undefined)
         {
                    $sql .= "where schedules.status = '".$status."' and members.memberid = '".$members."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
         }

         //date and service not empty
         else if ($startdate != undefined and $service != undefined and $payment == undefined and $members == undefined)
         {

            if ($subservice == undefined)
            {
                $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }
                else
                {
                    $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }

            }


         }

         //date, and payment not empty
         else if ($startdate != undefined and $service == undefined and $payment != undefined and $members == undefined)
         {
                $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);
         }

         //date, and members not empty
         else if ($startdate != undefined and $service == undefined and $payment == undefined and $members != undefined)
         {
                $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);
         }

//member and payment
        else if ($startdate == undefined and $service == undefined and $payment != undefined and $members != undefined)
         {
                $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and reservations.paymenttype = '".$payment."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);

         }


         //date, service, and payment not empty
         else if ($startdate != undefined and $service != undefined and $payment != undefined and $members == undefined)
         {
             if ($subservice == undefined)
             {
                $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);
            }
            else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }

            }

         }

         //service,payment not empty
         else if ($startdate == undefined and $service != undefined and $payment != undefined and $members == undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);

            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                          $sql .= " GROUP BY schedules.id ";
                     $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }

            }


         }

         //service,member not empty
         else if ($startdate == undefined and $service != undefined and $payment == undefined and $members != undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }
                else
                {
                    $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                          $sql .= " GROUP BY schedules.id ";
                     $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }

            }


         }

         //service,payment, member not empty
         else if ($startdate == undefined and $service != undefined and $payment != undefined and $members != undefined)
         {

            if ($subservice == undefined)
            {
            $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and schedules.status >= '".$status."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);

            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
 
                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and schedules.status = '".$status."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }

            }


         }


    } //Status condition


//start service condition
else if ($service!=undefined)
    {
        if ($payment != undefined and $members == undefined)
        {
              if ($subservice == undefined)
            {
            $sql .= "where reservations.paymenttype = '".$payment."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                        $sql .= " GROUP BY schedules.id ";
                   $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }

            }



        }

        else if($members != undefined and $payment == undefined)
        {
              if ($subservice == undefined)
            {
            $sql .= "where members.memberid = '".$members."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where members.memberid = '".$members."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);

                }
                else
                {
                    $sql .= "where  members.memberid = '".$members."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }

            }

        }

         else if($members != undefined and $payment !=undefined)
        {
              if ($subservice == undefined)
            {
            $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and pagecategory.id = '".$service."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
            }
             else
            {
                if($prices == undefined)
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }
                else
                {
                    $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' and pagecategory.id = '".$service."' and pages.pageid = '".$subservice."' and serviceprices.price='".$prices."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
                }

            }

        }


    } //end service condition


//start payment condition
    else if ($payment!=undefined)
    {
            if($members != undefined)
        {
             $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' ";
             $sql .= " GROUP BY schedules.id ";
             $sql .= " ORDER BY schedules.created_at DESC ";
             $searchresult = $app->dbSelect($sql);
             
        }
    }
        echo json_encode($searchresult);

}


 public function memberreportAction($num, $page, $startdate, $enddate,$status, $payment,$members){
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT reservations.paymenttype, schedules.id as schedid, schedules.startdatetime,  schedules.enddatetime,  schedules.scheduledate, schedules.status, members.firstname, members.lastname, members.email, schedules.updated_at, schedules.created_at, schedules.hourday, schedules.description, reservations.invoiceno,  pages.title, schedules.created_at, pagecategory.colorlegend,pagecategory.id  FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category=pagecategory.id INNER JOIN serviceprices ON serviceprices.serviceid = pages.pageid ';
        $sqlCount = 'SELECT COUNT(*) FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category=pagecategory.id ';
        $sqlWhereKey = '';
        $sqlWhereDate = '';
        $sqlWhereStatus = '';
        $where ='';

   
        // $sql .= $where;
        // $sqlCount .= $where;

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }

       
        // getting the query
if ($startdate==undefined and $status == undefined and $payment == undefined and $members == undefined)
    {
        $sql .= " GROUP BY schedules.id ";
        $sql .= " ORDER BY schedules.created_at DESC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        $searchresult = $app->dbSelect($sql);
        $totalreportdirty = $app->dbSelect($sqlCount);
     }

  else if ($status == undefined and $payment == undefined and $members == undefined)
    {
    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
    $searchresult = $app->dbSelect($sql);
    $sql .= " GROUP BY schedules.id ";
    $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
    }

    else if ($startdate == undefined and $payment == undefined and $members == undefined)
    {
        $sql .= "where schedules.status = '".$status."'";
        $sql .= " GROUP BY schedules.id ";
        $sql .= " ORDER BY schedules.created_at DESC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        $searchresult = $app->dbSelect($sql);
        $sqlCount .= "where schedules.status = '".$status."'";
        $totalreportdirty = $app->dbSelect($sqlCount);
    }

    else if ($startdate == undefined and $status==undefined and $members == undefined)
    {
    $sql .= "where reservations.paymenttype = '".$payment."'";
    $sql .= " GROUP BY schedules.id ";
    $sql .= " ORDER BY schedules.created_at DESC ";
    $sql .= " LIMIT " . $offsetfinal . ",10";
    $searchresult = $app->dbSelect($sql);
    $sqlCount .="where reservations.paymenttype = '".$payment."'";
    $totalreportdirty = $app->dbSelect($sqlCount);
    }
    else if ($startdate == undefined and $status==undefined and $payment == undefined)
    {
    $sql .= "where members.memberid = '".$members."'";
    $sql .= " GROUP BY schedules.id ";
    $sql .= " ORDER BY schedules.created_at DESC ";
    $sql .= " LIMIT " . $offsetfinal . ",10";
    $searchresult = $app->dbSelect($sql);
    $sqlCount .= "where members.memberid = '".$members."'";
    $totalreportdirty = $app->dbSelect($sqlCount);
    }



    //DATE not empty
    else if ($startdate!=undefined)
    {
        //status not empty
        if ($status != undefined and $payment == undefined and $members == undefined)
        {
            $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
            $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
        }

         //payment not empty
         else if ($payment != undefined and $status == undefined and $members == undefined)
         {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and reservations.paymenttype = '".$payment."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and reservations.paymenttype = '".$payment."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
         }
         //members not empty
         else if ($members != undefined and $payment == undefined and $status == undefined)
         {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and members.memberid = '".$members."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and members.memberid = '".$members."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
         }


         //Status, and payment not empty
         else if ($status != undefined and $payment != undefined and $members == undefined)
         {
                $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .=  "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
         }

         //Status, and members not empty
         else if ($status != undefined and $payment == undefined and $members != undefined)
         {
                $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .=  "where members.memberid = '".$members."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
         }

         // all are not empty
        else if ($status != undefined and $payment != undefined and $members != undefined)
         {
            
                    $sql .= "where members.memberid = '".$members."' and reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' ";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .=  "where members.memberid = '".$members."' and reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);

         }


    } //End date condition


//start status condition
else if ($status!=undefined)
    {
        //date not empty
        if ($startdate != undefined and $payment == undefined and $members == undefined)
        {
            $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $sql .= " LIMIT " . $offsetfinal . ",10";
            $searchresult = $app->dbSelect($sql);
            $sqlCount .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
            $totalreportdirty = $app->dbSelect($sqlCount);
        }

         //payment not empty
         else if ($payment != undefined and $startdate == undefined and $members == undefined)
         {
                    $sql .= "where schedules.status = '".$status."' and reservations.paymenttype = '".$payment."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where schedules.status = '".$status."' and reservations.paymenttype = '".$payment."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
         }
         //members not empty
         else if ($members != undefined and $payment == undefined and $startdate == undefined)
         {
                    $sql .= "where schedules.status = '".$status."' and members.memberid = '".$members."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $sql .= " LIMIT " . $offsetfinal . ",10";
                    $searchresult = $app->dbSelect($sql);
                    $sqlCount .= "where schedules.status = '".$status."' and members.memberid = '".$members."'";
                    $totalreportdirty = $app->dbSelect($sqlCount);
         }

         //date, and payment not empty
         else if ($startdate != undefined and $payment != undefined and $members == undefined)
         {
                $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
         }

         //date, and members not empty
         else if ($startdate != undefined and $payment == undefined and $members != undefined)
         {
                $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .=  "where members.memberid = '".$members."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
         }

//member and payment
        else if ($startdate == undefined and $payment != undefined and $members != undefined)
         {
                $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and reservations.paymenttype = '".$payment."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $sql .= " LIMIT " . $offsetfinal . ",10";
                $searchresult = $app->dbSelect($sql);
                $sqlCount .=  "where members.memberid = '".$members."' and schedules.status = '".$status."' and reservations.paymenttype = '".$payment."'";
                $totalreportdirty = $app->dbSelect($sqlCount);
         }


    } //Status condition


//start payment condition
    else if ($payment!=undefined)
    {
            if($members != undefined)
        {
             $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' ";
                  $sql .= " GROUP BY schedules.id ";
             $sql .= " ORDER BY schedules.created_at DESC ";
             $sql .= " LIMIT " . $offsetfinal . ",10";
             $searchresult = $app->dbSelect($sql);
             $sqlCount .="where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' ";
             $totalreportdirty = $app->dbSelect($sqlCount);
             
        }


    }
       
        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));

}

//memberreport end!

 public function memberreportdummyAction($startdate, $enddate,$status, $payment,$members){
        $app = new CB();

        $sql = 'SELECT reservations.paymenttype, schedules.id as schedid, schedules.startdatetime,  schedules.enddatetime,  schedules.scheduledate, schedules.status, members.firstname, members.lastname, members.email, schedules.updated_at, schedules.created_at, schedules.hourday, schedules.description, reservations.invoiceno,  pages.title, schedules.created_at, pagecategory.colorlegend,pagecategory.id  FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category=pagecategory.id INNER JOIN serviceprices ON serviceprices.serviceid = pages.pageid ';
        $sqlCount = 'SELECT COUNT(*) FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category=pagecategory.id ';


if ($startdate==undefined and $status == undefined and $payment == undefined and $members == undefined)
    {
        $sql .= " GROUP BY schedules.id ";
        $sql .= " ORDER BY schedules.created_at DESC ";
        $searchresult = $app->dbSelect($sql);
     }

  else if ($status == undefined and $payment == undefined and $members == undefined)
    {
    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
    $searchresult = $app->dbSelect($sql);
    $sql .= " GROUP BY schedules.id ";
    $sql .= " ORDER BY schedules.created_at DESC ";
    $searchresult = $app->dbSelect($sql);
    }

    else if ($startdate == undefined and $payment == undefined and $members == undefined)
    {
        $sql .= "where schedules.status = '".$status."'";
        $sql .= " GROUP BY schedules.id ";
        $sql .= " ORDER BY schedules.created_at DESC ";
        $searchresult = $app->dbSelect($sql);

    }

    else if ($startdate == undefined and $status==undefined and $members == undefined)
    {
    $sql .= "where reservations.paymenttype = '".$payment."'";
    $sql .= " GROUP BY schedules.id ";
    $sql .= " ORDER BY schedules.created_at DESC ";
    $searchresult = $app->dbSelect($sql);
    }
    else if ($startdate == undefined and $status==undefined and $payment == undefined)
    {
    $sql .= "where members.memberid = '".$members."'";
    $sql .= " GROUP BY schedules.id ";
    $sql .= " ORDER BY schedules.created_at DESC ";
    $searchresult = $app->dbSelect($sql);

    }

    else if ($startdate!=undefined)
    {

        if ($status != undefined and $payment == undefined and $members == undefined)
        {
            $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
            $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);

        }

         else if ($payment != undefined and $status == undefined and $members == undefined)
         {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and reservations.paymenttype = '".$payment."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
         }

         else if ($members != undefined and $payment == undefined and $status == undefined)
         {
                    $sql .= "where schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' and members.memberid = '".$members."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
         }

         else if ($status != undefined and $payment != undefined and $members == undefined)
         {
                $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);
         }

         else if ($status != undefined and $payment == undefined and $members != undefined)
         {
                $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);
         }

        else if ($status != undefined and $payment != undefined and $members != undefined)
         {
            
                    $sql .= "where members.memberid = '".$members."' and reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."' ";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
         }

    } //End date condition


//start status condition
else if ($status!=undefined)
    {

        if ($startdate != undefined and $payment == undefined and $members == undefined)
        {
            $sql .= "where schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                 $sql .= " GROUP BY schedules.id ";
            $sql .= " ORDER BY schedules.created_at DESC ";
            $searchresult = $app->dbSelect($sql);
        }

         else if ($payment != undefined and $startdate == undefined and $members == undefined)
         {
                    $sql .= "where schedules.status = '".$status."' and reservations.paymenttype = '".$payment."'";
                    $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
         }

         else if ($members != undefined and $payment == undefined and $startdate == undefined)
         {
                    $sql .= "where schedules.status = '".$status."' and members.memberid = '".$members."'";
                         $sql .= " GROUP BY schedules.id ";
                    $sql .= " ORDER BY schedules.created_at DESC ";
                    $searchresult = $app->dbSelect($sql);
         }

         else if ($startdate != undefined and $payment != undefined and $members == undefined)
         {
                $sql .= "where reservations.paymenttype = '".$payment."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);
         }

         else if ($startdate != undefined and $payment == undefined and $members != undefined)
         {
                $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and schedules.startdatetime >= '".$startdate."' and schedules.enddatetime <= '".$enddate."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);

         }

        else if ($startdate == undefined and $payment != undefined and $members != undefined)
         {
                $sql .= "where members.memberid = '".$members."' and schedules.status = '".$status."' and reservations.paymenttype = '".$payment."'";
                     $sql .= " GROUP BY schedules.id ";
                $sql .= " ORDER BY schedules.created_at DESC ";
                $searchresult = $app->dbSelect($sql);
         }


    } 


//start payment condition
    else if ($payment!=undefined)
    {
            if($members != undefined)
        {
             $sql .= "where reservations.paymenttype = '".$payment."' and members.memberid = '".$members."' ";
             $sql .= " GROUP BY schedules.id ";
             $sql .= " ORDER BY schedules.created_at DESC ";
             $searchresult = $app->dbSelect($sql);
             
        }


    }
       
        echo json_encode($searchresult);

}






 public function totsumAction(){
        $app = new CB();
        $sql = 'Select sum(price) as totalsales from schedules ';
        $sql.='where schedules.status != "VOID"';
        $sql2 = 'Select sum(price) as totalsales from schedules ';
       
      
        $result = $app->dbSelect($sql);
          foreach ($result as $get) 
            {
            $data[] = array(
                'totalsales'=>$get['totalsales'],
            );

            }

             $result2 = $app->dbSelect($sql2);
          foreach ($result2 as $get2) 
            {
            $data2[] = array(
                'totalsales'=>$get2['totalsales'],
            );

            }

        echo json_encode($data);

    }

public function expectedAction(){
        $app = new CB();

        $sql2 = 'Select sum(price) as expected from schedules ';
       

             $result2 = $app->dbSelect($sql2);
          foreach ($result2 as $get2) 
            {
            $data[] = array(
                'expected'=>$get2['expected'],
            );

            }

        echo json_encode($data);

    }

 public function reservestatsAction(){
        $app = new CB();
        $sql = 'Select count(*) as count,pages.pageid as p_id, pages.title as pt,schedules.serviceid as s_id,schedules.id,schedules.status from pages inner join schedules on serviceid = pageid ';
        $sql.='where schedules.status = "RESERVED" Group by pages.title order by count desc Limit 10';

        $count2 = count(schedules::find("status='RESERVED'"));
        $result = $app->dbSelect($sql);
          foreach ($result as $get) 
            {
            $count=count(schedules::find("serviceid = '".$get['p_id']."' AND status = 'RESERVED'"));
            
            $data[] = array(
                'pt'=>$get['pt'],
                'status'=>$get['status'],
                'p_id'=>$get['p_id'],
                'count'=>$count,
                'count2'=>$count2
                );

            }

        echo json_encode($data);

    }



 public function reserve_statserviceAction(){
        $app = new CB();
        //  $sql = 'SELECT schedules.id, schedules.status, members.firstname, members.lastname, members.email, schedules.updated_at, schedules.created_at, schedules.hourday, schedules.description, reservations.invoiceno,  pages.title, schedules.created_at, pagecategory.colorlegend,pagecategory.id  FROM schedules INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category=pagecategory.id ';
        $sql = 'Select count(*) as count,pagecategory.id as pc_id,pagecategory.title as pc_title, pages.pageid as p_id, schedules.serviceid as s_id, schedules.id,schedules.status from pagecategory inner join pages on pagecategory.id=pages.category inner join schedules on schedules.serviceid = pages.pageid ';
        
        $sql.='where schedules.status = "RESERVED" Group by pagecategory.id order by count desc';

        $count2 = count(schedules::find("status = 'RESERVED'"));
        $result = $app->dbSelect($sql);
          foreach ($result as $get) 
            {
            $count=count(schedules::find("serviceid = '".$get['p_id']."' AND status = 'RESERVED'"));
            
            $data[] = array(
                'pc_title'=>$get['pc_title'],
                'status'=>$get['status'],
                'pc_id'=>$get['pc_id'],
                'count'=>$count,
                'count2'=>$count2
                );

            }

        echo json_encode($data);

    }

     public function Sales_ReservationAction(){
        $app = new CB();
        $sql = 'Select * from schedules where status !="VOID"';
        $result = $app->dbSelect($sql);

        $testing = 'select max(startdatetime) as maxdate from schedules';
        $testresult = $app->dbSelect($testing);
          foreach ($testresult as $get2) 
            {
            $testdata=$get2['maxdate'];
            }

        $months = array();
        $currentMonth = date("F Y", strtotime($testdata));
        $x = 12;
        $y = 0;
        while($x > 0) {
           $months[] = date('F Y', strtotime('-'.$y.' month', strtotime($currentMonth)));
           $months1[] = date('n', strtotime('-'.$y.' month', strtotime($currentMonth)));
           $year[] = date('Y', strtotime('-'.$y.' month', strtotime($currentMonth)));
           $y++;
           $x--;
       }


          foreach ($months as $key => $get) 
            {
             $test='Select SUM(price) as totalsales,count(*) as count from schedules where Month(scheduledate) = "'.$months1[$key].'" and Year(scheduledate) = "'.$year[$key].'" and status != "VOID"';
             $result1 = $app->dbSelect($test);
             foreach ($result1 as $get1) 
             {

            if ($months1[$key]!=null)
            {

             $data[] = array(
                'tot'=>$get1['totalsales'],
                'count'=>$get1['count'],
                'month' => $months1[$key],
                'year' => $year[$key]
                );
            }



        }

     }

    $total='Select SUM(price) as total from schedules where status != "VOID"';
    $res = $app->dbSelect($total);

    $overall='Select SUM(price) as total from schedules';
    $overallres = $app->dbSelect($overall);


 echo json_encode(array('data' => $data, 'months' =>$months,'total'=>$res,'overall'=>$overallres), JSON_NUMERIC_CHECK);


    }




  public function MonthlyReservationAction(){
        $app = new CB();
        $sql = "Select * from schedules";
        $result = $app->dbSelect($sql);
        $totalcount = count(schedules::find());
        $months = array();
        $currentMonth = (int)date('m');
        $currentyear = (int)date('Y');

        for ($x = $currentMonth; $x > $currentMonth - 12; $x--) {
            $months[] = date('F Y', mktime(0, 0, 0, $x, 1));
            $months1[] = date('n', mktime(0, 0, 0, $x, 1));
            $year[] = date('Y', mktime(0, 0, 0, $x, 1));
        }

          foreach ($months as $key => $get) 
            {
             $test='Select id, count(*) as count from schedules where Month(created_at) = "'.$months1[$key].'" and Year(created_at) = "'.$year[$key].'"';
             $totcount = count(schedules::find());
             $result1 = $app->dbSelect($test);
             foreach ($result1 as $get1) 
                {
                    if ($months1[$key]!=null)
                {
                   $data[] = array(
                    'totcount' => $get1['totcount'],
                    'count' => $get1['count'],
                    'month' => $months1[$key],
                    'year' => $year[$key]
                    );


               }
               
                }

            }


       
 echo json_encode(array('data' => $data, 'months' =>$months,'totalreservation'=>$totalcount), JSON_NUMERIC_CHECK);

    }




 public function MonthlyScheduleAction(){
        $app = new CB();
        $sql = 'Select * from schedules';
        $result = $app->dbSelect($sql);

        $testing = 'select max(startdatetime) as maxdate from schedules';
        $testresult = $app->dbSelect($testing);
          foreach ($testresult as $get2) 
            {
            $testdata=$get2['maxdate'];
            }

        $months = array();
        $currentMonth = date("F Y", strtotime($testdata));
        $x = 12;
        $y = 0;
        while($x > 0) {
           $months[] = date('F Y', strtotime('-'.$y.' month', strtotime($currentMonth)));
           $months1[] = date('n', strtotime('-'.$y.' month', strtotime($currentMonth)));
           $year[] = date('Y', strtotime('-'.$y.' month', strtotime($currentMonth)));
           $y++;
           $x--;
       }


       foreach ($months as $key => $get) 
       {

        $reserve = count(schedules::find('status = "RESERVED" and Month(scheduledate) = "'.$months1[$key].'" and Year(scheduledate) = "'.$year[$key].'"'));
        if ($months1[$key]!=null)
        {

           $reservedata[] = $reserve;
       }
   }

   foreach ($months as $key => $get) 
       {

        $pending = count(schedules::find('status = "PENDING" and Month(scheduledate) = "'.$months1[$key].'" and Year(scheduledate) = "'.$year[$key].'"'));
        if ($months1[$key]!=null)
        {

           $pendingdata[] =$pending;
           
       }
   }

    foreach ($months as $key => $get) 
       {

        $void = count(schedules::find('status = "VOID" and Month(scheduledate) = "'.$months1[$key].'" and Year(scheduledate) = "'.$year[$key].'"'));
        if ($months1[$key]!=null)
        {

           $voiddata[] = $void;
       }
   }
    foreach ($months as $key => $get) 
       {

        $done = count(schedules::find('status = "DONE" and Month(scheduledate) = "'.$months1[$key].'" and Year(scheduledate) = "'.$year[$key].'"'));
        if ($months1[$key]!=null)
        {

           $donedata[] = $done;
       }
   }
   foreach ($months as $key => $get) 
       {

        $unattended = count(schedules::find('status = "UNATTENDED" and Month(scheduledate) = "'.$months1[$key].'" and Year(scheduledate) = "'.$year[$key].'"'));
        if ($months1[$key]!=null)
        {

           $unattendeddata[] = $unattended;
       }
   }


 echo json_encode(array('reserve' => $reservedata, 'months' =>$months,'pending'=>$pendingdata,'voiddata'=>$voiddata,'done'=>$donedata,'unattended'=>$unattendeddata), JSON_NUMERIC_CHECK);


    }



     public function UserStatAction(){
        $app = new CB();
        
        $totalsummary = count(schedules::find());
        $totalsummaryoff = count(schedules::find("type='OFFLINE'"));
        $totalsummaryon = count(schedules::find("type='ONLINE'"));
        $months = array();
        $currentMonth = (int)date('m');

        for ($x = $currentMonth; $x > $currentMonth - 12; $x--) {
            $months[] = date('F Y', mktime(0, 0, 0, $x, 1));
            $months1[] = date('n', mktime(0, 0, 0, $x, 1));
            $year[] = date('Y', mktime(0, 0, 0, $x, 1));
        }

        foreach ($months as $key => $get) 
            {
        $total = count(schedules::find());
        $test='Select count(*) as count from schedules where type= "OFFLINE" and Month(created_at) = "'.$months1[$key].'" and Year(created_at) = "'.$year[$key].'"';
        $offline= $app->dbSelect($test);    
           
           foreach ($offline as $get1) 
                {
                    if ($months1[$key]!=null)
                {
                   $off[] = array(
                    'off' => $get1['count']
                    );

                   $tots[] = array(
                    'off' => $get1['count'],
                    'tot' => $total
                    );
                }

               }
     
            }
            //online

              foreach ($months as $key => $get) 
            {
                  $total = count(schedules::find());
            $test1='Select count(*) as count from schedules where type= "ONLINE" and Month(created_at) = "'.$months1[$key].'" and Year(created_at) = "'.$year[$key].'"';
            $online= $app->dbSelect($test1);    
           
           foreach ($online as $onn) 
                {
                    if ($months1[$key]!=null)
                {
                   $on[] = array(
                    'on' => $onn['count']
                    ); 

                    $toot[] = array(
                    'on' => $onn['count'],
                    'tot' => $total
                    ); 

                }

               }
     
            }

    echo json_encode(array('off' => $off, 'months' =>$months,'on'=>$on,'totaloff'=>$tots,'toot'=>$toot,'totalsummary'=>$totalsummary,'totalsummaryoff'=>$totalsummaryoff,'totalsummaryon'=>$totalsummaryon), JSON_NUMERIC_CHECK);
            // echo json_encode($on);


    }



     public function OverallUserStatAction(){
        $app = new CB();
        
        $sql = 'Select * from schedules';
        $result = $app->dbSelect($sql);

        $totalsummary = count(schedules::find());
        $totalsummaryoff = count(schedules::find("type='OFFLINE'"));
        $totalsummaryon = count(schedules::find("type='ONLINE'"));
     
       
                   $off[] = array(
                    'off' => $totalsummaryoff
                    );

                   $on[] = array(
                    'on' => $totalsummaryon
                    );
            //online

    echo json_encode(array('off' => $off, 'on'=>$on), JSON_NUMERIC_CHECK);
            // echo json_encode($on);


    }


public function TopUserAction($num, $page){
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;
         if($offsetfinal < 0){
            $offsetfinal = 0;
        }

     $sqlCount = 'Select COUNT(*) from members inner join schedules on members.memberid = schedules.memberid group by members.memberid';

     
        $sql = 'Select count(*) as countall, members.memberid, members.firstname, members.lastname, schedules.status, schedules.memberid from members inner join schedules on members.memberid = schedules.memberid Group by schedules.memberid order by countall desc';
       
        $sql .= " LIMIT " . $offsetfinal . ",10";
        $count=count(schedules::find());
       
       

         $result = $app->dbSelect($sql);
          foreach ($result as $get) 
            {

                $countpermem = count(schedules::find("memberid = '".$get['memberid']."'"));
                 $data[] = array(
                'count'=>$countpermem,
                'memberid'=>$get['memberid'],
                'firstname'=>$get['firstname'],
                'lastname'=>$get['lastname'],
                'totalcount' => $get['countall']
                );
            
            }

            $totalreportdirty = $app->dbSelect($sqlCount);
             
         

           echo json_encode(array('data'=>$data,'index'=>$page,'total_items' => $totalreportdirty));

    }


 public function TopServiceAction(){
       $app = new CB();
        $sql = 'Select count(*) as count,pagecategory.id as pc_id,pagecategory.title as pc_title, pages.pageid as p_id, schedules.serviceid as s_id, schedules.id,schedules.status from pagecategory inner join pages on pagecategory.id=pages.category inner join schedules on schedules.serviceid = pages.pageid ';
        $sql.='Group by pagecategory.id order by count desc';
        
        $count2 = count(schedules::find());
        $result = $app->dbSelect($sql);
          foreach ($result as $get) 
            {
            
               $data[] = array(
                'pc_title'=>$get['pc_title'],
                'pc_id'=>$get['pc_id']
                );

            $extradata[] = array(
                'count'=>$get['count']
                );

            }
            

            

echo json_encode(array('data'=>$data,'extradata'=>$extradata),JSON_NUMERIC_CHECK);


    }


public function TopSubserviceAction(){
      $app = new CB();
        $sql = 'Select count(*) as count,pages.pageid as p_id, pages.title as pt,schedules.serviceid as s_id,schedules.id,schedules.status from pages inner join schedules on serviceid = pageid ';
        $sql.='Group by pages.title order by count desc Limit 10';

        $count2 = count(schedules::find());
        $result = $app->dbSelect($sql);
          foreach ($result as $get) 
            {
            $count=count(schedules::find("serviceid = '".$get['p_id']."'"));
            
            $data[] = array(
                'pt'=>$get['pt'],
                'p_id'=>$get['p_id'],
                );
             $extradata[] = array(
                'count'=>$count
                );
              $superdata[] = array(
                'count'=>$count,
                'totcount'=>$count2
                );

            }

        echo json_encode(array('data'=>$data,'extradata'=>$extradata,'superdata'=>$superdata),JSON_NUMERIC_CHECK);


    }


     public function DailyScheduleAction(){
        $date = date("Y-m-d");
        $app = new CB();
        $sql = 'Select * from schedules where scheduledate = "'.$date.'" ';
        $result = $app->dbSelect($sql);
        $months[] = $date;
       foreach ($result as $key => $get) 
       {

        $reserve = count(schedules::find('status = "RESERVED" and scheduledate = "'.$date.'" '));
        $reservedata[] = $reserve;

        $pending = count(schedules::find('status = "PENDING" and scheduledate = "'.$date.'" '));
        $pendingdata[] =$pending;

        $void = count(schedules::find('status = "VOID" and scheduledate = "'.$date.'" '));
        $voiddata[] = $void;

        $done = count(schedules::find('status = "DONE" and scheduledate = "'.$date.'" '));
        $donedata[] = $done;

        $unattended = count(schedules::find('status = "UNATTENDED" and scheduledate = "'.$date.'"'));
        $unattendeddata[] = $unattended;
       }
 echo json_encode(array('reserve' => $reservedata, 'months' =>$months,'pending'=>$pendingdata,'voiddata'=>$voiddata,'done'=>$donedata,'unattended'=>$unattendeddata), JSON_NUMERIC_CHECK);
       
    }

     public function DailyReservationAction(){
        $date = date("Y-m-d");
        $app = new CB();
        $sql = 'Select * from schedules where date(created_at) = "'.($date).'" ';
        $result = $app->dbSelect($sql);
        $months[] = $date;
       foreach ($months as $key => $get) 
       {

        $reserve = count(schedules::find('status = "RESERVED" and date(created_at) = "'.($date).'" '));
        $reservedata[] = $reserve;

        $pending = count(schedules::find('status = "PENDING" and date(created_at) = "'.($date).'" '));
        $pendingdata[] =$pending;

        $void = count(schedules::find('status = "VOID" and date(created_at) = "'.($date).'" '));
        $voiddata[] = $void;

        $done = count(schedules::find('status = "DONE" and date(created_at) = "'.($date).'" '));
        $donedata[] = $done;

        $unattended = count(schedules::find('status = "UNATTENDED" and date(created_at) = "'.($date).'"'));
        $unattendeddata[] = $unattended;
       }
 echo json_encode(array('reserve' => $reservedata, 'months' =>$months,'pending'=>$pendingdata,'voiddata'=>$voiddata,'done'=>$donedata,'unattended'=>$unattendeddata), JSON_NUMERIC_CHECK);
       
       // echo json_encode($result);
    }


      public function TotalNewsAction(){
         $count = count(news::find());
         echo json_encode($count);
      }

   

}

