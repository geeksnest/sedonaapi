<?php

namespace Controllers;

class ExampleController extends \Phalcon\Mvc\Controller {


	public function pingAction() {
		var_dump($_POST);
	}


    public function testAction($id) {
        echo "test (id: $id)";
    }

    public function skipAction($name) {
        echo "auth skipped ($name)";
    }
}
