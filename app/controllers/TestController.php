<?php

namespace Controllers;


use \Controllers\ControllerBase as CB;
use Phalcon\Http\Request;
use \Models\Test as Test;
use \Models\Questions as Questions;
use \Models\Testresults as Testresults;
use \Models\Testrecommendation as Testrecommendation;
use \Models\Testmembers as Testmembers;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
class TestController extends \Controllers\ControllerBase {
    public function maxAction() {
        $app = new CB();
        $sql = 'select max(testid) as maxid from test';
        $result = $app->dbSelect($sql);
        echo json_encode($result);
    }

     public function savetestAction() {
         $app = new CB();
         $request = new Request();
         if ($request->isPost()) {
             $testid=$request->getPost('testid');
             $testname = $request->getPost('testname');
             $res=$request->getPost('response');
             $prob=$request->getPost('prob');

             $addtest = new Test();
             $addtest->testid   = $testid;
             $addtest->testname  = $testname;
              $addtest->status   = 1;
             if ($addtest->save() == true) {

               foreach($res as $key => $value)
               {
                 $add = new Questions();
                 $add->testid = $testid;
                 $add->question=$res[$key]['Question'];
                 $add->optionA =$res[$key]['A'];
                 $add->optionB = $res[$key]['B'];
                 $add->optionC = $res[$key]['C'];
                 $add->arrange = $key;
                 if ($add->save() == true) {
                // $data['result'] = 'success';
                 }

               }
        
                    foreach ($prob as $key => $value) {
                       $addresult = new Testresults();
                       $addresult->testid = $testid;
                       $addresult->choice=$prob[$key]['choice'];
                       $addresult->result =$prob[$key]['result'];
                       $addresult->description = $prob[$key]['description'];

                        foreach ($prob[$key]['services'] as $get => $value) {

                       $addrec = new Testrecommendation();
                       $addrec->testid = $testid;
                       $addrec->choice=$prob[$key]['choice'];
                       $addrec->services =$prob[$key]['services'][$get]['Services'];
                       $addrec->link = $prob[$key]['services'][$get]['Link'];
                       $addrec->products = $prob[$key]['products'][$get]['Products'];
                               if ($addrec->save() == true) {
                                      $data['result'] = 'success';
                               }
                        }
                              if ($addresult->save() == true) {
                                // $data['result'] = 'success';
                                       }
                    }

                    if (($add->save() == true) && ($addresult->save() == true) && ($addrec->save() == true)) {
                        $data['result'] = 'success';
                    }
                    else {
                        $data['result'] = 'success';

                    }


        }
        else {
            $errors = [];
            $data['result'] = 'error!';
            foreach ($addtest->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            $data['error'] = $errors;

        }


    }
       
        echo json_encode($data);

}


     public function testtakerAction() {
        $app = new CB();
        $sql = 'select max(testid) as maxid from test';
        $result = $app->dbSelect($sql);
        echo json_encode($result);

    }

       public function testnameAction($num,$page,$keyword) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;
        $sql = 'select * from test';
        $sqlCount = 'SELECT COUNT(*) FROM test';
        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlconcat = " WHERE test.testname LIKE '%" . $keyword . "%'";
            $sql .= $sqlconcat;
            $sqlCount .= $sqlconcat;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }

        $sql .= " LIMIT " . $offsetfinal . ",10";
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

     echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    
        }

       public function testnamefrontAction() {
        $app = new CB();
        $sql = 'select * from test where status=1';
        $result = $app->dbSelect($sql);
        echo json_encode($result);
    }

     public function testitemsAction($testid) {
        $app = new CB();
        $sql = 'SELECT test.testid, questions.testid,questions.question,questions.optionA,questions.optionB,questions.optionC from test inner join questions on test.testid = questions.testid where test.testid = "'.$testid.'" order by questions.arrange';
        $result = $app->dbSelect($sql);
        echo json_encode($result);

    }

      public function viewtestAction($testid) {
        $app = new CB();
        $sqql = 'SELECT * from test where test.testid="'.$testid.'"';
        $resultq = $app->dbSelect($sqql);
        $sql = 'SELECT * from test inner join questions on test.testid = questions.testid where test.testid="'.$testid.'" group by questions.question order by questions.arrange';
        $result = $app->dbSelect($sql);
        $sql2 = 'SELECT * from test inner join testresults on test.testid = testresults.testid where test.testid="'.$testid.'" group by testresults.choice';
        $result2 = $app->dbSelect($sql2);
            foreach($resultq as $get)
            {
              $test[] = array(
                'testid'=>$get['testid'],
                'testname'=>$get['testname']
                );
             }
         foreach($result as $get)
        {
            $data[] = array(
                'q_id' => $get['q_id'],
                'question' => $get['question'],
                'choiceA' => $get['optionA'],
                'choiceB' => $get['optionB'],
                'choiceC' => $get['optionC']
                );
        }
        foreach($result2 as $get)
        {
            $data2[] = array(
                'id' => $get['id'],
                'choice' => $get['choice'],
                'description' => $get['description'],
                'result' => $get['result']
                );
        }

            $sql3 = 'SELECT * from testrecommendation where testid="'.$testid.'"';
            $result3 = $app->dbSelect($sql3);

            foreach($result3 as $get1)
            {
                $data3[] = array(
                    'id' => $get1['r_id'],
                    'choice' => $get1['choice'],
                    'services' => $get1['services'],
                    'link' => $get1['link'],
                    'products' => $get1['products']
                    );
            }
        echo json_encode(array('q'=>$data,'result'=>$data2,'recommend' => $data3,'testname'=>$test));

    }


     public function viewQuestionAction($q_id) {
         $useredit = Questions::findFirst("q_id=$q_id");
        $useredit = json_encode($useredit->toArray(), JSON_NUMERIC_CHECK);
            echo $useredit;
    }

    public function viewTestnameAction($testid) {
         $useredit = Test::findFirst("testid=$testid");
        $useredit = json_encode($useredit->toArray(), JSON_NUMERIC_CHECK);
            echo $useredit;
    }

      public function EditQuestionAction() {
        $request = new Request();
        if ($request->isPost()) {
            $q_id =$request->getPost('q_id');
            $question= $request->getPost('question');
            $A= $request->getPost('A');
            $B= $request->getPost('B');
            $C= $request->getPost('C');

                            $add = Questions::findFirst("q_id=$q_id");
                            $add->question= $question;
                            $add->optionA= $A;
                            $add->optionB= $B;
                            $add->optionC= $C;

                            if ($add->save() == false) {
                                $data['result'] = "error";
                            } 

                            else {
                            
                                    $data['result'] = "success";
                            }
    echo json_encode($data);

    }
}

public function EditTestnameAction() {
        $request = new Request();
        if ($request->isPost()) {
            $testid =$request->getPost('testid');
            $testname= $request->getPost('testname');

                            $add = Test::findFirst("testid=$testid");
                            $add->testname= $testname;
                            if ($add->save() == false) {
                                $data['result'] = "error";
                            } 
                            else {
                            
                                    $data['result'] = "success";
                            }
    echo json_encode($data);

    }
}

public function DeleteQuestionAction($id){
        $dlt = Questions::findFirst('q_id="' . $id . '"');
        if ($dlt) {
            if($dlt->delete()){
               $data['result'] = 'success';
            }

            else {
             $data['result'] = 'error';
            }

          

        }

        echo json_encode($data);
    }


    public function DeleteTestAction($testid){
        $dlt1 = Questions::find('testid="' . $testid . '"');
        $dlt2 = Testrecommendation::find('testid="' . $testid . '"');
        $dlt3 = Testresults::find('testid="' . $testid . '"');
        $dlt = Test::find('testid="' . $testid . '"');
        if (($dlt) && ($dlt1) && ($dlt2) && ($dlt3)) {
            if(($dlt->delete()) && ($dlt1->delete()) && ($dlt2->delete()) && ($dlt3->delete()) ){
               $data['result'] = 'success';
            }

            else {
             $data['result'] = 'error';
            }

          

        }

        echo json_encode($data);
    }

public function viewResultAction($id) {
         $viewresult = Testresults::findFirst("id=$id");
        $viewresult = json_encode($viewresult->toArray(), JSON_NUMERIC_CHECK);
            echo $viewresult;
    }

    public function viewServicesAction($id) {
         $viewservices = Testrecommendation::findFirst('r_id = "'.$id.'" ');
        $viewservices = json_encode($viewservices->toArray(), JSON_NUMERIC_CHECK);
            echo $viewservices;
     
    }

      public function EditResultAction() {
        $request = new Request();
        if ($request->isPost()) {
            $id =$request->getPost('id');
            $result= $request->getPost('result');
            $description=$request->getPost('description');
            $probability=$request->getPost('probability');

                            $add = Testresults::findFirst("id=$id");
                            $add->result=$result;
                            $add->description= $description;

                            if ($add->save() == false) {
                                $data['result'] = "error";
                            } 

                            else {
                            
                                    $data['result'] = "success";
                            }
  

    }
      echo json_encode($data);
    
}

   public function EditServicesAction() {
        $request = new Request();
        if ($request->isPost()) {
            $id =$request->getPost('id');
            $services= $request->getPost('services');
            $link=$request->getPost('link');

                            $add = Testrecommendation::findFirst("r_id=$id");
                            $add->services=$services;
                            $add->link= $link;

                            if ($add->save() == false) {
                                $data['result'] = "error";
                            } 

                            else {
                            
                                    $data['result'] = "success";
                            }
  

    }
      echo json_encode($data);
    
}

   public function EditProductsAction() {
        $request = new Request();
        if ($request->isPost()) {
            $id =$request->getPost('id');
            $products= $request->getPost('products');

                            $add = Testrecommendation::findFirst("r_id=$id");
                            $add->products=$products;

                            if ($add->save() == false) {
                                $data['result'] = "error";
                            } 

                            else {
                            
                                    $data['result'] = "success";
                            }
  

    }
      echo json_encode($data);
    
}


public function displayresultAction() {
     $app = new CB();
                $request = new Request();
        if ($request->isPost()) {
            $answers =$request->getPost('answers');
            $testid =$request->getPost('passid');
        
                
                $pc = explode(" ",$answers);
                    $counts = array_count_values($answers);

                    if (($counts['A'] > $counts['B']) && ($counts['A'] > $counts['C']))
                    {
                        $viewresult = 'Select * from testresults where testid = "'.$testid.'" and choice = "A"';
                        $viewservices = 'Select * from testrecommendation where testid = "'.$testid.'" and choice = "A"';
                        $data = 'A is the highest';
                    }
                    else if(($counts['B'] > $counts['A']) && ($counts['B'] > $counts['C']))
                    {
                        $viewresult = 'Select * from testresults where testid = "'.$testid.'" and choice = "B"';
                        $viewservices = 'Select * from testrecommendation where testid = "'.$testid.'" and choice = "B"';
                        $data = 'B is the highest';
                    }
                    else if(($counts['C'] > $counts['A']) && ($counts['C'] > $counts['B']))
                    {
                        $viewresult = 'Select * from testresults where testid = "'.$testid.'" and choice = "C"';
                        $viewservices = 'Select * from testrecommendation where testid = "'.$testid.'" and choice = "C"';
                        $data = 'C is the highest';
                    }
                     else if($counts['A'] == $counts['B'])
                    {
                        $viewresult = 'Select * from testresults where testid = "'.$testid.'" and choice = "AB"';
                        $viewservices = 'Select * from testrecommendation where testid = "'.$testid.'" and choice = "AB"';
                        $data = 'AB is the highest';
                    }
                     else if($counts['A'] == $counts['C'])
                    {
                        $viewresult = 'Select * from testresults where testid = "'.$testid.'" and choice = "AC"';
                        $viewservices = 'Select * from testrecommendation where testid = "'.$testid.'" and choice = "AC"';
                        $data = 'AC is the highest';
                    }
                     else if($counts['B'] == $counts['C'])
                    {
                        $viewresult = 'Select * from testresults where testid = "'.$testid.'" and choice = "BC"';
                        $viewservices = 'Select * from testrecommendation where testid = "'.$testid.'" and choice = "BC"';
                        $data = 'BC is the highest';
                    }
                    else if(($counts['A'] == $counts['C']) && ($counts['A']==$counts['B']) )
                    {
                        $viewresult = 'Select * from testresults where testid = "'.$testid.'" and choice = "ABC"';
                        $viewservices = 'Select * from testrecommendation where testid = "'.$testid.'" and choice = "ABC"';
                        $data = 'ABC is the highest';
                    }
                    else
                    {
                        $data = 'impossible!';
                    }

                }
                 $result= $app->dbSelect($viewresult);
                 foreach ($result as $get) {
                     $res[] = array(
                        'result'=>$get['result'],
                         'description'=>$get['description']
                        );
                 }
                 $services= $app->dbSelect($viewservices);
                echo json_encode(array('result'=>$res,'services'=>$services),JSON_NUMERIC_CHECK);

    }



     public function sendresultAction() {
         // var_dump($_POST);
           $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $lname = $request->getPost('ln');
            $fname = $request->getPost('fn');
            $senderemail = $request->getPost('email');
            $message = $request->getPost('result');
            $result = $message['result'][0]['result'];
            $description = $message['result'][0]['description'];
             $app = new CB();


                    $add = new Testmembers();
                    $add->fname=$fname;
                    $add->lname=$lname;
                    $add->email=$senderemail;
                    $add->date = date('Y-m-d');

                    if ($add->save() == false) {
                        $data['result'] = "error";
                    } 

                    else {
                      $errors = [];
                      
                      foreach ($add->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    $data['result'] = $errors;
                    }


                foreach ($message['services'] as $key => $value) {
                   $serv.='<br>'.$message['services'][$key]['services'];
                   $prod.='<br>'.$message['services'][$key]['products'];
              
                }
                 
                $email = 'hazelcajalne@mailinator.com';
                $subject = 'Sedona Test Result';
                $body = 'Name: '.$fname. ' '. $lname . ' <br> Email: '. $senderemail . ' <br><br> Test Result: <br><br>'.$result. '<br><br>'. $description . '<br><br>Services<br>'.$serv.'<br><br>Products<br>'.$prod;
                $content = $app->mailTemplate($subject,$body);
                $all = $body.''.$serv.''.$prod;
                $app->sendMail($email,$subject,$content);

                $subject = "CC: Sedona Test Result";
                $body = 'Name: '.$fname. ' '. $lname . ' <br> Email: '. $senderemail . ' <br><br> Test Result: <br><br>'. $result . ' <br><br>'. $description . '<br><br>Services<br>'.$serv.'<br><br>Products<br>'.$prod;
                $content = $app->mailTemplate($subject,$body);
                $app->sendMail($senderemail,$subject,$content);


        }

     echo json_encode($body);



    }

     public function updatestatusAction($testid,$status) {
        $data = array();
        $test = Test::findFirst('testid="' . $testid . '"');
        $test->status = $status;
            if (!$test->save()) {
                $data['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
            }

            echo json_encode($data);
    }


     public function AddEditQAction() {
         $app = new CB();
         $request = new Request();
         if ($request->isPost()) {
             $testid=$request->getPost('testid');
             $option = $request->getPost('option');

               foreach($option as $key => $value)
               {
                 $add = new Questions();
                 $add->testid = $testid;
                 $add->question=$option[$key]['Question'];
                 $add->optionA =$option[$key]['A'];
                 $add->optionB = $option[$key]['B'];
                 $add->optionC = $option[$key]['C'];
                 if ($add->save() == true) {
                $data['result'] = 'success';
                 }
                 else
                 {
                    $data['result'] = 'error';
                 }

               }
        
       }

            echo json_encode($data);
    }

     public function AddEditResultAction() {
         $app = new CB();
         $request = new Request();
         if ($request->isPost()) {
             $testid=$request->getPost('testid');
             $prob = $request->getPost('prob');

               foreach($prob as $key => $value)
               {
                 $add = new Testresults();
                 $add->testid = $testid;
                 $add->choice=$prob[$key]['Probability'];
                 $add->result =$prob[$key]['Result'];
                 $add->description = $prob[$key]['Description'];
                 if ($add->save() == true) {
                $data['result'] = 'success';
                 }
                 else
                 {
                    $data['result'] = 'error';
                 }

               }
        
       }

            echo json_encode($data);
    }

    public function DeleteResultAction($id,$choice,$testid){
        $dlt = Testresults::findFirst('id="' . $id . '"');
        $dlt2 = Testrecommendation::find('testid="' . $testid . '" and choice="' . $choice . '"');
        if ($dlt) {
            if(($dlt->delete()) && ($dlt2->delete())) {
               $data['result'] = 'success';
            }

            else {
             $data['result'] = 'error';
            }

          

        }

        echo json_encode($data);
    }

      public function DeleteRecAction($id){
        $dlt = Testrecommendation::findFirst('r_id="' . $id . '"');
        $pass= $dlt->products;

        if ($pass==null)
        {
                   if ($dlt) {
                    if($dlt->delete()){
                     $data['result'] = 'success';
                 }

                 else {
                   $data['result'] = 'error';
               }

             }

        }
        else
        {

                 $add = Testrecommendation::findFirst("r_id=$id");
                 $add->services=null;
                 $add->link=null;
                   if ($add->save() == true) {
                            $data['result'] = 'success';
                        }
                        else
                        {
                            $data['result'] = 'error';
                        }

        }

       
        echo json_encode($data);
    }

     public function DeleteProdAction($id){
        $dlt = Testrecommendation::findFirst('r_id="' . $id . '"');
        $pass= $dlt->services;

        if ($pass==null)
        {
                   if ($dlt) {
                    if($dlt->delete()){
                     $data['result'] = 'success';
                 }

                 else {
                   $data['result'] = 'error';
               }

             }

        }
        else
        {

                 $add = Testrecommendation::findFirst("r_id=$id");
                 $add->products=null;
                   if ($add->save() == true) {
                            $data['result'] = 'success';
                        }
                        else
                        {
                            $data['result'] = 'error';
                        }

        }

       
        echo json_encode($data);
    }


 public function AddEditRecAction() {
         $app = new CB();
         $request = new Request();
         if ($request->isPost()) {
             $testid=$request->getPost('testid');
             $choice = $request->getPost('probability');
             $services = $request->getPost('services');
             $link = $request->getPost('link');

                           $add = new Testrecommendation();
                           $add->testid = $testid;
                           $add->choice=$choice;
                           $add->services =$services;
                           $add->link =$link;
                               if ($add->save() == true) {
                                $data['result'] = 'success';
                            }
                            else
                            {
                                $data['result'] = 'error';
                            }
        
       }

            echo json_encode($data);
    }

     public function AddEditProdAction() {
         $app = new CB();
         $request = new Request();
         if ($request->isPost()) {
             $testid=$request->getPost('testid');
             $choice = $request->getPost('probability');
             $products = $request->getPost('products');

                           $add = new Testrecommendation();
                           $add->testid = $testid;
                           $add->choice=$choice;
                           $add->products =$products;
                               if ($add->save() == true) {
                                $data['result'] = 'success';
                            }
                            else
                            {
                                $data['result'] = 'error';
                            }

        
       }

            echo json_encode($data);
    }


      public function testmemberlistAction($num,$page,$keyword) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;
        $sql = 'select * from testmembers';
        $sqlCount = 'SELECT COUNT(*) FROM testmembers';
        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlconcat = " WHERE testmembers.fname LIKE '%" . $keyword . "%' Or testmembers.lname LIKE '%" . $keyword . "%' or testmembers.email LIKE '%" . $keyword . "%' or testmembers.date LIKE '%" . $keyword . "%'";
            $sql .= $sqlconcat;
            $sqlCount .= $sqlconcat;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }

        $sql .= " LIMIT " . $offsetfinal . ",10";
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

     echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    
        }


         public function arrangeAction() {
    
         $request = new Request();
         if ($request->isPost()) {
             $list=$request->getPost('list');

           foreach ($list as $key => $value) {
               $q_id = $list[$key]['q_id'];
               $add = Questions::findFirst("q_id=$q_id");
               $add->arrange = $key;
               if ($add->save() == true) {
                    $data['result'] = 'success';
                }
                else
                {
                    $data['result'] = 'error';
                }
                
           }
              

            echo json_encode($data);
        }
    }


     public function DeleteMemAction($id){
        $dlt = Testmembers::findFirst('mem_id="' . $id . '"');
        if ($dlt) {
            if($dlt->delete()) {
               $data['result'] = 'success';
            }

            else {
             $data['result'] = 'error';
            }

          

        }

        echo json_encode($data);
    }

}
