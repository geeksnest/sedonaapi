<?php

namespace Controllers;

use Security\Jwt\JWT;
use \Models\Booking as Booking;
use \Models\Bookingreply as Bookingreply;
use \Models\Reservations as Reservations;
use \Models\Schedules as Schedules;
use \Models\Members as Members;
use \Models\Invoices as Invoices;
use \Models\Pages as Pages;
use \Models\Pagecategory as Pagecategory;
use \Models\Billinginfo as Billinginfo;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use PHPMailer as PHPMailer;

class BookingController extends \Phalcon\Mvc\Controller {

    public function sendbookingAction() {
         // var_dump($_POST);

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){


            $lname = $request->getPost('lname');
            $fname = $request->getPost('fname');
            $senderemail = $request->getPost('email');
            $contactno = $request->getPost('contactno');
            $service = $request->getPost('service');
            $message = $request->getPost('message');

            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();

            $book = new Booking();
            $book->assign(array(
                'id' => $id,
                'lname' => $lname,
                'fname' => $fname,
                'email' => $senderemail,
                'contactno' => $contactno,
                'service' => $service,
                'message' => $message,
                'status' => 'new',
                'created_at' =>  date("Y-m-d H:i:s"),
                'updated_at' =>  date("Y-m-d H:i:s")
                ));

            if (!$book->save()) {
                $errors = array();
                foreach ($book->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['success'] = "Success";

                $app = new CB();

                // $adminemail = 'contact@sedonahealingarts.com';
                $adminemail = 'geeksnest@mailinator.com';
                $subject = $service;
                $body = 'Name: '.$fname. ' '. $lname . ' <br> Email: '. $senderemail . ' <br> Contact Number: '. $contactno . ' <br><br> Message: <br><br>'. $message . '';
                $content = $app->mailTemplate($subject, $body);
                /*$app->sendMail($email,$subject,$content);

                $subject = "CC:".$service;
                $body = 'Name: '.$fname. ' '. $lname . ' <br> Email: '. $senderemail . ' <br> Contact Number: '. $contactno . ' <br><br> Message: <br><br>'. $message . '';
                $content = $app->mailTemplate($subject, $body);
                $app->sendMail($senderemail,$subject,$content);*/


                $sendtoadmin = new PHPMailer();

                $sendtoadmin->isSMTP();
                $sendtoadmin->Host = 'smtp.mandrillapp.com';
                $sendtoadmin->SMTPAuth = true;
                $sendtoadmin->Username = 'efrenbautistajr@gmail.com';
                $sendtoadmin->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';
                $sendtoadmin->Port = 587;
                $sendtoadmin->From = $senderemail;
                $sendtoadmin->FromName = 'Sedona Healing Arts Team';
                $sendtoadmin->addAddress($adminemail);
                $sendtoadmin->isHTML(true);
                $sendtoadmin->Subject = $subject;
                $sendtoadmin->Body = $content;


                $mail = new PHPMailer();

                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';
                $mail->Port = 587;
                $mail->From = $adminemail;
                $mail->FromName = 'Sedona Healing Arts Team';
                $mail->addAddress($senderemail);
                $mail->isHTML(true);
                $mail->Subject = 'Re -'.$subject;
                $mail->Body = $content;

                if (!$sendtoadmin->send()){
                    $data = array('error' => $sendtoadmin->ErrorInfo);
                    die(json_encode(array("error" =>  $sendtoadmin->ErrorInfo)));
                }
                elseif (!$mail->send()){
                    $data = array('error' => $mail->ErrorInfo);
                    die(json_encode(array("error" =>  $mail->ErrorInfo)));
                }
            }

        }

        echo json_encode($data);



    }

    public function listAction($num, $page, $keyword, $searchdate) {
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT * FROM booking';
        $sqlCount = 'SELECT COUNT(*) FROM booking';

        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlWhere = " WHERE fname LIKE '%" . $keyword . "%' OR lname LIKE '%" . $keyword . "' OR email LIKE '%".$keyword."%' OR service LIKE '%".$keyword."%' OR message LIKE '%".$keyword."%' OR DATE_FORMAT(created_at,'%Y-%m-%d' ) = '".$searchdate."'";
            $sql .= $sqlWhere;
            $sqlCount .= $sqlWhere;
        }elseif(($keyword == 'null' || $keyword == 'undefined')  && $searchdate != 'null' && $searchdate != 'undefined'){
            $sqlWhere = " WHERE DATE_FORMAT(created_at,'%Y-%m-%d' ) = '".$searchdate."'";
            $sql .= $sqlWhere;
            $sqlCount .= $sqlWhere;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        $sql .= " ORDER BY updated_at DESC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    }

    public function getAction($id) {
            $book = Booking::find('id="'.$id.'"');
            echo json_encode($book->toArray());
    }
    public function readAction($id){
        $book = Booking::findFirst('id="'.$id.'"');
        $book->status='read';
        if (!$book->save()) {
            $data['error'] = 'Something went wrong.';
        } else {

            $data['success'] = "Success";
        }
        echo json_encode($data);
    }
    public function replyAction(){
        $data = array();
        $guid = new \Utilities\Guid\Guid();

        $book = new Bookingreply();
        $book->assign(array(
            'id' => $guid->GUID(),
            'message' => $_POST['message'],
            'bookid' => $_POST['bookid'],
            'created_at' =>  date("Y-m-d H:i:s"),
            'updated_at' =>  date("Y-m-d H:i:s"),
        ));
        if (!$book->save()){
            $errors = array();
            foreach ($book->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
        }
        else{
            $data['success'] = "Success";
            $rbook = Booking::findFirst('id="'.$_POST['bookid'].'"');
            $rbook->status='replied';
            if (!$rbook->save()) {
                $data['error'] = 'Something went wrong.';
            } else {

                $app = new CB();
                $body = $_POST['message']."<br/><br/> Message ".$rbook->updated_at." ===================================<br/><br/>".$rbook->message;
                $content = $app->mailTemplate($rbook->service, $body);
                //$app->sendMail($rbook->email, 'Re -'.$rbook->service, $content);

                $adminemail = 'contact@sedonahealingarts.com';
                //$adminemail = 'geeksnest@mailinator.com';
                $mail = new PHPMailer();

                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';
                $mail->Port = 587;
                $mail->From = $adminemail;
                $mail->FromName = 'Sedona Healing Arts Team';
                $mail->addAddress($rbook->email);
                $mail->isHTML(true);
                $mail->Subject = 'Re -'.$rbook->service;
                $mail->Body = $content;

                if (!$mail->send()){
                    $data = array('error' => $mail->ErrorInfo);
                    die(json_encode(array("error" =>  $mail->ErrorInfo)));
                }
                else{
                     $data['success'] = "Success";
                }
            }
        }
        echo json_encode($data);
    }
    public function getrepliesAction($id) {
        $book = Bookingreply::find(array(
            "bookid = '".$id."'",
            "order" => "updated_at ASC",

        ));
        echo json_encode($book->toArray());
    }
    public function deleteAction($id){
        $conditions = 'id="' . $id . '"';
        $book = Booking::findFirst(array($conditions));
        if ($book) {
            if ($book->delete()) {
                $data = array('success' => 'Booking Deleted');
                $rbook = Bookingreply::findFirst('bookid="' . $id . '"');
                if ($rbook) {
                    if ($rbook->delete()) {
                        $data = array('success' => 'Booking Deleted');
                    }
                    else
                    {
                        $data = array('error' => 'Booking Not Deleted');
                    }
                }
                die(json_encode($data));
            }
            else
            {
                $data = array('error' => 'Booking Not Deleted');
                die(json_encode($data));
            }
        }
    }
    public function addreservationAction(){
        $request = new \Phalcon\Http\Request();
        $cb = new CB();
        $response = '';
        $invoiceno = hexdec(substr(uniqid(),0,9));

        if($request->getPost('paymenttype')!='cash')
        {
            $data = $cb->creditcardPayment($_POST, $invoiceno , 'Offline Reservation');
            $response['authorize'] = $data;
        }

        if(isset($data['success']) || $request->getPost('paymenttype')=='cash'){

            if ($request->isPost() == true) {
                try{


                    $transactionManager = new TransactionManager();
                    $guid = new \Utilities\Guid\Guid();

                    $transaction = $transactionManager->get();
                    $datecreated = date("Y-m-d H:i:s");
                    $members = Members::findFirst('email="'.$request->getPost('email').'"');
                    if (!$members) {
                        $members = new Members();
                        $members->setTransaction($transaction);

                        $members->memberid = $guid->GUID();
                        $members->email = $request->getPost('email');
                        $members->address1 = $request->getPost('address1');
                        $members->address2 = $request->getPost('address2');
                        $members->country = $request->getPost('country');
                        $members->firstname = $request->getPost('firstname');
                        $members->lastname = $request->getPost('lastname');
                        $members->phoneno = $request->getPost('phoneno');
                        $members->state = $request->getPost('state');
                        $members->zipcode = $request->getPost('zipcode');
                        $members->status = 'INCOMPLETE';
                        $members->created_at = $datecreated;
                        $members->updated_at = date("Y-m-d H:i:s");
                        if ($members->save() == false) {
                            $array = [];
                            foreach ($members->getMessages() as $message) {
                                $array[] = $message;
                            }
                            var_dump($array);
                            die();
                            $transaction->rollback('Cannot save members.');
                        }
                    }

                    $b = $request->getPost('billinginfo');

                    $res = new Reservations();
                    $res->setTransaction($transaction);
                    $res->id = $guid->GUID();
                    $res->invoiceno = $invoiceno;
                    $res->memberid = $members->memberid;
                    $res->billingfname = $b['billingfname'];
                    $res->billinglname = $b['billinglname'];
                    $res->billingcity = $b['city'];
                    $res->billingcountry = $b['country']['name'];
                    $res->paymenttype = $request->getPost('paymenttype');
                    $res->billingstate = $b['state'];
                    $res->billingzipcode = $b['zip'];
                    $res->billingadd1 = $b['al1'];
                    $res->billingadd2 = $b['al2'];
                    $res->amount = $request->getPost('amount');
                    $res->created_at = $datecreated;
                    $res->updated_at = date("Y-m-d H:i:s");
                    if($res->save()==false){
                        $array = [];
                        foreach ($res->getMessages() as $message) {
                            $array[] = $message;
                        }
                        $transaction->rollback('Cannot save Invoice.');
                    }

                    $ss = $request->getPost('reservationlist');
                    foreach($ss as $m => $k) {
                        $sched = new Schedules();
                        $sched->setTransaction($transaction);
                        $sched->id = $guid->GUID();
                        $sched->memberid = $members->memberid;
                        $sched->scheduledate = $k['reservationdate'];
                        $sched->startdatetime = $k['startdatetime'];
                        $sched->enddatetime = $k['enddatetime'];
                        $sched->serviceid = $k['servicechoice']['serviceid'];
                        $sched->invoiceid = $invoiceno;
                        $sched->price = $k['servicechoice']['price'];
                        $sched->description = $k['servicechoice']['description'];
                        $sched->hourday = $k['servicechoice']['hourday'];
                        $sched->status = 'RESERVED';
                        $sched->created_at = $datecreated;
                        $sched->updated_at = date("Y-m-d H:i:s");
                        $sched->type = "OFFLINE";
                        if ($sched->save() == false) {
                            $array = [];
                            foreach ($sched->getMessages() as $message) {
                                $array[] = $message;
                            }
                            $transaction->rollback('Cannot save reservation.');
                        }
                    }
                    $content = $cb->invoiceTemplate($ss, $b, $invoiceno, $datecreated, $request->getPost('paymenttype'));
                    //$res = $cb->sendMail($members->email, 'Sedona Healing Arts Reservation Details', $content);


                    $adminemail = 'contact@sedonahealingarts.com';
                    //$adminemail = 'geeksnest@mailinator.com';
                    $mail = new PHPMailer();

                    $mail->isSMTP();
                    $mail->Host = 'smtp.mandrillapp.com';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'efrenbautistajr@gmail.com';
                    $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';
                    $mail->Port = 587;
                    $mail->From = $adminemail;
                    $mail->FromName = 'Sedona Healing Arts Team';
                    $mail->addAddress($members->email);
                    $mail->isHTML(true);
                    $mail->Subject = 'Sedona Healing Arts Reservation Details';
                    $mail->Body = $content;

                    if (!$mail->send()){
                        $data = array('error' => $mail->ErrorInfo);
                        die(json_encode(array("error" =>  $mail->ErrorInfo)));
                    }

                    $transaction->commit();
                }catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
                    die( json_encode(array('401' => $e->getMessage())) );
                }

                $response['200'] = "Success.";
                $response['invoiceno'] = $invoiceno;
                die(json_encode($response));
            }
        }else{
            die(json_encode($data));
        }

    }

    public function validateAction($stime, $etime){
        $book = Schedules::find("'$stime' < enddatetime AND '$etime' > startdatetime AND status != 'VOID' AND status!= 'TMP'");
        echo json_encode($book->toArray());
    }

    public function getbyemailAction($email){
        $member = Members::find("email='$email'");
        echo json_encode($member->toArray());
    }

    public function getinvoiceAction($invoiceno){
        $app = new CB();

        // getting the query
        $sql = 'SELECT * FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid WHERE invoiceno = "'.$invoiceno.'"';
        $get = $app->dbSelect($sql);
        die(json_encode($get));
    }


    public  function getmontheventsAction($month,$year){
        $app = new CB();

        // getting the query
        $sql = 'SELECT schedules.id as schedid, schedules.startdatetime,  schedules.enddatetime,  schedules.scheduledate, schedules.status, members.firstname, members.lastname, members.email, members.phoneno, schedules.hourday, schedules.description, schedules.updated_at, schedules.created_at,reservations.invoiceno,  pages.title, pagecategory.colorlegend FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category = pagecategory.id WHERE YEAR(schedules.scheduledate) = '.$year.' AND MONTH(schedules.scheduledate)='.$month.' AND schedules.status != "TMP"';
        $get = $app->dbSelect($sql);
        die(json_encode($get));
    }

    public  function updatesheduleAction(){
        $request = new \Phalcon\Http\Request();
        $book = Schedules::find("('".$request->getPost('cstime')."' < enddatetime AND '".$request->getPost('cetime')."' > startdatetime) AND id != '".$request->getPost('schedid')."' AND status != 'VOID'");
        if(count($book->toArray()) > 0){
            die(json_encode(array("error"=>"Someone is already reserved in that schedule.")));
        }else{
            $data = array();
            $b = Schedules::findFirst("id = '".$request->getPost('schedid')."'");
            $b->scheduledate = $request->getPost('reservation');
            $b->startdatetime = $request->getPost('cstime');
            $b->enddatetime = $request->getPost('cetime');
            $b->updated_at = date("Y-m-d H:i:s");
            if (!$b->save()) {
                $data['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
            }
            die(json_encode($data));
        }
    }

    public function updatestatusAction($id, $status, $message){
        $b = Schedules::findFirst("id = '".$id."'");
        $b->status = $status;
        $b->updated_at = date("Y-m-d H:i:s");
        if (!$b->save()) {
            $data['error'] = "Something went wrong saving page status, please try again.";
        } else {
            if($status=='RESERVED'){
                $m = Members::findFirst('memberid="'.$b->memberid.'"');
                $app = new CB();

                $stime = date_create($b->startdatetime);
                $etime = date_create($b->enddatetime);
                $body = 'This is a confirmation email to your appointment on '.$b->scheduledate.' at '.date_format($stime, 'g:i A').' - '.date_format($etime, 'g:i A').'. Should you have queries or require any clarifications, please do not hesitate to contact me on the number <strong>928-282-3875</strong>. If for any reason you wish to cancel your appointment, I appreciate a prompt and early notification from your side. Looking forward to your presence.';
                $content = $app->mailTemplate('Sedona Healing Arts Reservation Approval', $body);
                //$res = $app->sendMail($m->email, 'Sedona Healing Arts Arts Reservation Approval', $content);


                $adminemail = 'contact@sedonahealingarts.com';
                //$adminemail = 'geeksnest@mailinator.com';
                $mail = new PHPMailer();

                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';
                $mail->Port = 587;
                $mail->From = $adminemail;
                $mail->FromName = 'Sedona Healing Arts Team';
                $mail->addAddress($m->email);
                $mail->isHTML(true);
                $mail->Subject = 'Sedona Healing Arts Arts Reservation Approval';
                $mail->Body = $content;

                if (!$mail->send()){
                    $data = array('error' => $mail->ErrorInfo);
                    die(json_encode(array("error" =>  $mail->ErrorInfo)));
                }
                else{
                    $data['success'] = "Success";
                }

            }elseif($status=='VOID'){
                $m = Members::findFirst('memberid="'.$b->memberid.'"');
                $app = new CB();

                $stime = date_create($b->startdatetime);
                $etime = date_create($b->enddatetime);
                $body = 'This is a confirmation email to the cancellation of your appointment on '.$b->scheduledate.' at '.date_format($stime, 'g:i A').' - '.date_format($etime, 'g:i A').'. with the following reasons: <br><br> '.$message;
                $content = $app->mailTemplate('Sedona Healing Arts Reservation Cancellation', $body);
                //$res = $app->sendMail($m->email, 'Sedona Healing Arts Arts Reservation Cancellation', $content);

                $adminemail = 'contact@sedonahealingarts.com';
                //$adminemail = 'geeksnest@mailinator.com';
                $mail = new PHPMailer();

                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';
                $mail->Port = 587;
                $mail->From = $adminemail;
                $mail->FromName = 'Sedona Healing Arts Team';
                $mail->addAddress($m->email);
                $mail->isHTML(true);
                $mail->Subject = 'Sedona Healing Arts Arts Reservation Cancellation';
                $mail->Body = $content;

                if (!$mail->send()){
                    $data = array('error' => $mail->ErrorInfo);
                    die(json_encode(array("error" =>  $mail->ErrorInfo)));
                }
                else{
                    $data['success'] = "Success";
                }


            }

        }
        $up = explode(' ',$b->updated_at);
        $data['updated_at'] = $up[0];
        die(json_encode($data));
    }

    public function countingstatusAction($currtime){
        $app = new CB();
        // getting the query
        $sql = "
        SELECT
            SUM(status = 'DONE') AS done,
            SUM(status = 'VOID') AS void,
            SUM(status = 'PENDING') AS pending,
            SUM(status = 'RESERVED' && startdatetime >= '$currtime' && enddatetime >= '$currtime') AS reserved,
            SUM(status = 'RESERVED' && startdatetime <= '$currtime' && enddatetime <= '$currtime') AS unattended
        FROM schedules
        ";
        $get = $app->dbSelect($sql);
        die(json_encode($get));
    }


  public function listbookingAction($num, $page, $keyword, $day, $month,$year, $status, $currdate){
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT reservations.paymenttype, schedules.id as schedid, schedules.startdatetime,  schedules.enddatetime,  schedules.scheduledate, schedules.status, members.firstname, members.lastname, members.email, schedules.updated_at, schedules.created_at, schedules.hourday, schedules.description, reservations.invoiceno,  pages.title, schedules.created_at, pagecategory.colorlegend  FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category=pagecategory.id ';
        $sqlCount = 'SELECT COUNT(*) FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid ';
        $sqlWhereKey = '';
        $sqlWhereDate = '';
        $sqlWhereStatus = '';
        $where ='';

        //Keyword
        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlWhereKey = " (firstname LIKE '%" . $keyword . "%' OR lastname LIKE '%" . $keyword . "' OR email LIKE '%".$keyword."%' OR description LIKE '%".$keyword."%' OR invoiceno LIKE '%".$keyword."%' OR pages.title LIKE '%".$keyword."%') ";
        }

        //Date
        if(!empty($day) && !empty($month) && !empty($year)){
            $sqlWhereDate = " (YEAR(scheduledate) = '".$year."' AND MONTH(scheduledate) = '".$month."' AND DAY(scheduledate)='".$day."') ";
        }elseif(empty($day) && !empty($month) && !empty($year)){
            $sqlWhereDate = " (YEAR(scheduledate) = '".$year."' AND MONTH(scheduledate) = '".$month."') ";
        }elseif(empty($day) && empty($month) && !empty($year)){
            $sqlWhereDate = " (YEAR(scheduledate) = '".$year."') ";
        }

        $where = !empty($sqlWhereKey) || !empty($sqlWhereDate) ? " WHERE " . (!empty($sqlWhereKey) ? $sqlWhereKey . (!empty($sqlWhereDate) ? ' AND '.$sqlWhereDate : ' ') : $sqlWhereDate). ' ' : ' ' ;

        //Status
        if($status == 'UNATTENDED'){
            $where .= (!empty($where) ? " AND ('$currdate' > enddatetime AND '$currdate' > startdatetime) AND schedules.status='RESERVED' AND schedules.status!='TMP' " : " WHERE '$currdate' > enddatetime AND '$currdate' > startdatetime AND schedules.status='RESERVED' AND schedules.status!='TMP' ");
        }elseif($status == 'ALL'){
            $where .= (!empty($where) ? " " :  " AND schedules.status!='TMP' ");
        }elseif($status == 'RESERVED'){
            $where .= (!empty($where) ? " AND ('$currdate' < enddatetime AND '$currdate' < startdatetime) AND schedules.status='RESERVED' AND schedules.status!='TMP' " : " WHERE '$currdate' < enddatetime AND '$currdate' < startdatetime AND schedules.status='RESERVED' AND schedules.status!='TMP' ");
        }else{
            $where .= (!empty($where) ? " AND schedules.status='$status' AND schedules.status!='TMP' " : " WHERE schedules.status='$status' AND schedules.status!='TMP' ");
        }

        $sql .= $where;
        $sqlCount .= $where;

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }

        $sql .= " ORDER BY schedules.created_at DESC ";
        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query


        //var_dump($sql);

        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    }



    /* FRONT END METHODS */
    public function registrationAction(){
        $request = new \Phalcon\Http\Request();
        $formfrom = $request->getPost('formfrom');
        $members = Members::findFirst('email="'.$request->getPost('email').'" OR username="'.$request->getPost('username').'"');
        $guid = new \Utilities\Guid\Guid();
        if (!$members) {
            $members = new Members();

            $members->memberid = $guid->GUID();
            $members->email = $request->getPost('email');
            $members->username = $request->getPost('username');
            $members->password = sha1($request->getPost('password'));
            $members->city = $request->getPost('city');
            $members->country = $request->getPost('country');
            $members->firstname = $request->getPost('firstname');
            $members->lastname = $request->getPost('lastname');
            $members->phoneno = $request->getPost('phoneno');
            $members->state = $request->getPost('state');
            $members->zipcode = $request->getPost('zipcode');

            /* Generate Confirmation Code */
            $jwt = new \Security\Jwt\JWT();
            $app = new CB();
            $payload = array(
                "id" => $members->memberid,
                "iat" => time(),
                "exp" => time() + (120 * 60)); // time in the future
            $token = $jwt->encode($payload, $app->getConfig()['hashkey']);

            $members->confirmationcode = $token;

            $members->status = 'PENDING';
            $members->created_at = date("Y-m-d H:i:s");
            $members->updated_at = date("Y-m-d H:i:s");
            if ($members->save() == false) {
                $array = [];
                foreach ($members->getMessages() as $message) {
                    $array[] = $message;
                }
                die(json_encode(array("error" => "Something went wrong with our servers please try again later.")));
            }

            $bi = new Billinginfo();
            $guid = new \Utilities\Guid\Guid();
            $bi->id = $guid->GUID();
            $bi->memberid = $members->memberid;
            $bi->firstname = $request->getPost("firstname");
            $bi->lastname = $request->getPost("lastname");
            $bi->address = $request->getPost("address1");
            $bi->address2 = $request->getPost("address2");
            $bi->city = $request->getPost('city');
            $bi->country = $request->getPost("country");
            $bi->state = $request->getPost("state");
            $bi->zipcode = $request->getPost("zipcode");
            $bi->phoneno = $request->getPost("phoneno");

            if ($bi->save() == false) {
                $array = [];
                foreach ($bi->getMessages() as $message) {
                    $array[] = $message;
                }
                die(json_encode(array("error" => "Something went wrong with our servers please try again later.")));
            }

            if($formfrom == 'registration'){
                $body = 'Hi '.$request->getPost('firstname').' '.$request->getPost('lastname').'! <br><br>Thank you for registering to Sedona Healing Arts. To complete your registration please click
                <br><br><a style="display:inline-block; border-radius:3px; background:orangered; border:1px solid red; padding:7px 15px; text-decoration:none; color:white; font-size:18px; margin:10px 0;" href="'.$app->getConfig()['application']['baseURL'].'/sd/confirmation/'.$token.'">VERIFY</a>
                <br><br>Or proceed to the given link below:
                <br><br> <a href="'.$app->getConfig()['application']['baseURL'].'/sd/confirmation/'.$token.'">'.$app->getConfig()['application']['baseURL'].'/sd/confirmation/'.$token.'</a> <br><br> Thank You!<br>Sedona Healing Arts Support Team';
            }
            elseif ($formfrom == 'checkout') {
              $body = 'Hi '.$request->getPost('firstname').' '.$request->getPost('lastname').'! <br><br>Thank you for registering to Sedona Healing Arts Online Shop Application. To complete your registration please click
              <br><br><a style="display:inline-block; border-radius:3px; background:orangered; border:1px solid red; padding:7px 15px; text-decoration:none; color:white; font-size:18px; margin:10px 0;" href="'.$app->getConfig()['application']['baseURL'].'/shop/confirmation/'.$token.'">VERIFY</a>
              <br><br>Or proceed to the given link below:
              <br><br> <a href="'.$app->getConfig()['application']['baseURL'].'/shop/confirmation/'.$token.'">'.$app->getConfig()['application']['baseURL'].'/shop/confirmation/'.$token.'</a> <br><br> Thank You!<br>Sedona Healing Arts Support Team';
            }
            else{
                $body = 'Hi '.$request->getPost('firstname').' '.$request->getPost('lastname').'! <br><br>Thank you for registering to Sedona Healing Arts Online Service Reservation Application. To complete your registration please click the link below: <br><br> <a href="'.$app->getConfig()['application']['baseURL'].'/reservation/confirmation/'.$token.'">'.$app->getConfig()['application']['baseURL'].'/reservation/confirmation/'.$token.'</a> <br><br> Thank You!<br>Sedona Healing Arts Support Team';
            }

            $content = $app->mailTemplate('Sedona Healing Arts Membership Confirmation Email', $body);
            $res = $app->sendMail($request->getPost('email'), 'Sedona Healing Arts Membership Confirmation Email', $content);

            // $sendtoadmin = new PHPMailer();
            //
            // $sendtoadmin->isSMTP();
            // $sendtoadmin->Host = 'smtp.mandrillapp.com';
            // $sendtoadmin->SMTPAuth = true;
            // $sendtoadmin->Username = 'efrenbautistajr@gmail.com';
            // $sendtoadmin->Password = '6Xy18AJ15My59aQNTHE5kA';
            //             //$mail->SMTPSecure = 'ssl';
            // $sendtoadmin->Port = 587;
            // $sendtoadmin->From = 'geeksnest101@gmail.com';
            // $sendtoadmin->FromName = 'Sedona Healing Arts Team';
            // $sendtoadmin->addAddress($request->getPost('email'));
            // $sendtoadmin->isHTML(true);
            // $sendtoadmin->Subject = 'Sedona Healing Arts Membership Confirmation Email';
            // $sendtoadmin->Body = $content;
            // if (!$sendtoadmin->send()) {
            //     $data = array('error' => $sendtoadmin->ErrorInfo);
            //     die(json_encode(array("error" =>  $sendtoadmin->ErrorInfo)));
            // }

            if($request->getPost('login') == true){
                $cart = $request->getPost("cart");

                foreach ($cart as $key => $value) {
                    $s = new Shopbag();
                    $guid = new \Utilities\Guid\Guid();
                    $s->assign(array(
                      'id' => $guid->GUID(),
                      'productid' => $value['productid'],
                      'quantity' => $value['cartquantity'],
                      'memberid' => $members->memberid,
                      'created_at' => date("Y-m-d H:i:s")));

                    if ($s->save() == false) {
                      $data = [];
                      foreach ($m->getMessages() as $message) {
                        $data[] = $message;
                      }
                    }
                }
            }

            die(json_encode(array("success" => "Success! One more step to complete your membership is to complete the instruction in the email we sent you.", "data" => array("memberid" => $members->memberid, "email" => $members->email, "name" => $members->firstname . " " .$members->lastname ) , "mailerror" => $res)));
        }else{
            if($members->email == $request->getPost('email')){
                die(json_encode(array("error" => "Email already taken.")));
            }elseif($members->username == $request->getPost('username')){
                die(json_encode(array("error" => "Username already taken.")));
            }

        }

    }
    public function confirmationAction($value){
        $jwt = new \Security\Jwt\JWT();
        $app = new CB();
        $val  = $jwt->decode($value, $app->getConfig()['hashkey'], array('HS256'));

        $members = Members::findFirst('confirmationcode="'.$value.'" AND memberid="'.$val->id.'"');
        if ($members) {
            /* Clearing Token */
            $members->confirmationcode = '';
            $members->updated_at = date("Y-m-d H:i:s");
            $members->status = 'REGISTERED';
            if ($members->save() == false) {
                die(json_encode(array("error" => "Something went wrong with our servers please try again later.")));
            }else{
                die(json_encode(array('success' => 'Thank you for completing your membership. You can now proceed to login.')));
            }
        }else{
            die(json_encode(array('error' => 'Error')));
        }
    }

    public function loginAction(){
        $jwt = new \Security\Jwt\JWT();
        $app = new CB();
        $request = new \Phalcon\Http\Request();
        $members = Members::findFirst('username="'.$request->getPost('loginemailusername').'" OR email="'.$request->getPost('loginemailusername').'"');
        if ($members) {
            if($members->status == 'PENDING') {
                die(json_encode(array("pending" => "Please confirmed your email before you login")));
            }elseif ($members->password != sha1($request->getPost('loginpassword'))) {
                die(json_encode(array("error" => "Username or Password is incorrect.")));
            }else{
                $payload = array(
                    "id" => $members->memberid,
                    "email" => $members->email,
                    "lastname" => $members->lastname,
                    "firstname" => $members->firstname,
                    "address1" => $members->address1,
                    "address2" => $members->address2,
                    "zipcode" => $members->zipcode,
                    "country" => $members->country,
                    "state" => $members->state,
                    "phoneno" => $members->phoneno,
                    "exp" => time() + (60 * 60)); // time in the future
                $token = $jwt->encode($payload, $app->getConfig()['hashkey']);

                die(json_encode(array('success' => 'Success', 'token' => $token )));
            }
        }else{
            die(json_encode(array('error' => 'Username or Email does not belong to any user.')));
        }
    }

    public function resendconfirmationAction(){
        $request = new \Phalcon\Http\Request();
        $members = Members::findFirst('email="'.$request->getPost('email').'" AND status="PENDING"');

        if ($members) {

            /* Generate Confirmation Code */
            $jwt = new \Security\Jwt\JWT();
            $app = new CB();
            $payload = array(
                "id" => $members->memberid,
                "iat" => time(),
                "exp" => time() + (120 * 60)); // time in the future
            $token = $jwt->encode($payload, $app->getConfig()['hashkey']);

            $members->confirmationcode = $token;
            $members->updated_at = date("Y-m-d H:i:s");
            if ($members->save() == false) {
                $array = [];
                foreach ($members->getMessages() as $message) {
                    $array[] = $message;
                }
                die(json_encode(array("error" => "Something went wrong with our servers please try again later.")));
            }
            $body = 'Hi '.$members->firstname.' '.$members->lastname.'! <br><br>Than you for registering to Sedona Healing Arts Online Service Reservation Application. To complete your registration please click the link below: <br><br> <a href="'.$app->getConfig()['application']['baseURL'].'/reservation/confirmation/'.$token.'">'.$app->getConfig()['application']['baseURL'].'/reservation/confirmation/'.$token.'</a> <br><br> Thank You!<br>Sedona Healing Arts Support Team';
            $content = $app->mailTemplate('Sedona Healing Arts Membership Confirmation Email', $body);
            $res = $app->sendMail($request->getPost('email'), 'Sedona Healing Arts Membership Confirmation Email', $content);
            die(json_encode(array('success' => 'Email confirmation has been sent.')));
        }else{
            die(json_encode(array('error' => 'Username or Email does not belong to any user or is already registered.' )));
        }

    }

    public function forgotpasswordAction(){
        $jwt = new \Security\Jwt\JWT();
        $app = new CB();
        $request = new \Phalcon\Http\Request();
        $members = Members::findFirst('email="'.$request->getPost('email').'"');
        if ($members) {
            if($members->status == 'PENDING') {
                die(json_encode(array("pending" => "Please confirmed your email first or Click Resend Confirmation Email.")));
            }else{
                $payload = array(
                    "email" => $members->email,
                    "ia" => time(),
                    "exp" => time() + (60 * 60)); // time in the future
                $token = $jwt->encode($payload, $app->getConfig()['hashkey']);
                $members->forgotpassword = $token;
                if ($members->save() == false) {
                    $array = [];
                    foreach ($members->getMessages() as $message) {
                        $array[] = $message;
                    }
                    die(json_encode(array('error' => $array)));
                }
                $content = $app->mailTemplate("Reset Password Instructions", "Hi ". $members->firstname ."! <br><br> Click the link below to reset your password: <a href='".$app->getConfig()['application']['baseURL']."/resetpassword/".$token."'> ".$app->getConfig()['application']['baseURL']."/resetpassword/".$token." </a> <br><br>");
                $res = $app->sendMail($members->email, 'Sedona Healing Arts Forgot Password', $content);

                die(json_encode(array('success' => 'Instructions has been sent to your email for password reset.')));
            }
        }else{
            die(json_encode(array('error' => 'Username or Email does not belong to any user.')));
        }
    }

    public function checktokenAction(){
        $jwt = new \Security\Jwt\JWT();
        $app = new CB();
        $request = new \Phalcon\Http\Request();
        $members = Members::findFirst('forgotpassword="'.$request->getPost('token').'"');
        if ($members) {
            $val  = $jwt->decode($members->forgotpassword, $app->getConfig()['hashkey'], array('HS256'));
            if(time() >= $val->exp){
                die(json_encode(array('error' => 'Your token has expired already. Please request for reset password again.')));
            }else{
                die(json_encode(array('success' => 'Valid Token')));
            }
        }else{
            die(json_encode(array('error' => 'Invalid Token')));
        }
    }

    public function changepasswordAction(){
        $jwt = new \Security\Jwt\JWT();
        $app = new CB();
        $request = new \Phalcon\Http\Request();

        $val  = $jwt->decode($request->getPost('token'), $app->getConfig()['hashkey'], array('HS256'));

        $members = Members::findFirst('forgotpassword="'.$request->getPost('token').'" AND email="'.$val->email.'"');
        if ($members) {
            if(time() >= $val->exp){
                die(json_encode(array('error' => 'Your token has expired already. Please request for reset password again.')));
            }else{

                $members->password = sha1($request->getPost('password'));
                $members->forgotpassword = '';
                if ($members->save() == false) {
                    $array = [];
                    foreach ($members->getMessages() as $message) {
                        $array[] = $message;
                    }
                    die(json_encode(array('error' => $array)));
                }
                $content = $app->mailTemplate("Reset Password Success", "Hi ". $members->firstname ."! <br><br> Your password has successfully updated! <br> <br> Thanks!");
                $res = $app->sendMail($members->email, 'Sedona Healing Arts Forgot Password Changed', $content);
                die(json_encode(array('success' => 'Your password has been changed successfully!')));
            }
        }else{
            die(json_encode(array('error' => 'Invalid Token')));
        }
    }

    public function updatepaymentAction(){

        $request = new \Phalcon\Http\Request();

        $app = new CB();
        $datecreated = date("Y-m-d H:i:s");

        $app->modelsManager('UPDATE \Models\Reservations  SET status="PAID", created_at = "'.$datecreated.'" WHERE invoiceno = "'.$request->getPost('invoiceno').'" ');
        $app->modelsManager('UPDATE \Models\Schedules  SET status="PENDING", created_at = "'.$datecreated.'" WHERE invoiceid = "'.$request->getPost('invoiceno').'" ');

        $data = $app->modelsManager('select \Models\Members.email from \Models\Members INNER JOIN \Models\Reservations on \Models\Members.memberid = \Models\Reservations.memberid WHERE invoiceno="'.$request->getPost('invoiceno').'"');

        $arr = $data->toArray();

        if(isset($arr[0]['email'])){
//            $content = $app->mailTemplate('Paypal Payment Recieved', 'Thank you for subscribing to our services! We have recieved you paypal payment! Your reference number is: '.$request->getPost('invoiceno'));
//            $res = $app->sendMail($data->email, 'Sedona Healing Arts Membership Confirmation Email', $content);

            $ssched = Schedules::find('invoiceid="'.$request->getPost('invoiceno').'"');
            $ss = [];
            foreach ($ssched as $m) {
                $service = Pages::findFirst('pageid="'.$m->serviceid.'"');
                $ss[] = array(
                    'reservationdate' => $m->scheduledate,
                    'startdatetime' => $m->startdatetime,
                    'enddatetime' => $m->enddatetime,
                    'service' => array('cattitle'=> $service->title ),
                    'servicechoice' => array('serviceid'=> $m->serviceid, 'price' => $m->price, 'description' => $m->description, 'hourday' =>$m->hourday)
                );
            }

            $b = '';
            $content = $app->invoiceTemplate($ss, $b, $request->getPost('invoiceno'), $datecreated, 'paypal');
            $res = $app->sendMail($arr[0]['email'], 'Sedona Healing Arts Reservation Details', $content);
            die(json_encode(array('success'=>'Success!')));
        }else{
            die(json_encode(array('error'=>'Error!')));
        }


    }

    public function onlinereservationAction(){

        $request = new \Phalcon\Http\Request();
        $cb = new CB();
        $response = '';
        $invoiceno = hexdec(substr(uniqid(),0,9));
        $data = '';
        $statusR = '';
        $statusS = '';
            if ($request->isPost() == true) {
                try{

                    if($request->getPost('paymenttype')!='paypal')
                    {
                        $data = $cb->creditcardPayment($_POST, $invoiceno , 'Online Reservation');
                        $response['authorize'] = $data;
                        $statusR = 'PAID';
                        $statusS = 'PENDING';
                    }else{
                        $statusR = 'UNPAID';
                        $statusS = 'TMP';
                    }

                    if(isset($data['success']) || $request->getPost('paymenttype')=='paypal') {
                        $transactionManager = new TransactionManager();
                        $guid = new \Utilities\Guid\Guid();

                        $transaction = $transactionManager->get();

                        $members = Members::findFirst('memberid="' . $request->getPost('id') . '"');
                        if ($members) {

                            $b = $request->getPost('billinginfo');

                            $datecreated = date("Y-m-d H:i:s");

                            $res = new Reservations();
                            $res->setTransaction($transaction);
                            $res->id = $guid->GUID();
                            $res->invoiceno = $invoiceno;
                            $res->memberid = $request->getPost('id');
                            $res->billingfname = $b['billingfname'];
                            $res->billinglname = $b['billinglname'];
                            $res->billingcity = $b['city'];
                            $res->billingcountry = $b['country']['name'];
                            $res->paymenttype = $request->getPost('paymenttype');
                            $res->billingstate = $b['state'];
                            $res->billingzipcode = $b['zip'];
                            $res->billingadd1 = $b['al1'];
                            $res->billingadd2 = $b['al2'];
                            $res->status = $statusR;
                            $res->amount = $request->getPost('amount');
                            $res->created_at = $datecreated;
                            $res->updated_at = date("Y-m-d H:i:s");
                            if ($res->save() == false) {
                                $array = [];
                                foreach ($res->getMessages() as $message) {
                                    $array[] = $message;
                                }
                                $transaction->rollback('Cannot save Invoice.');
                            }

                            $ss = $request->getPost('reservationlist');
                            foreach ($ss as $m => $k) {
                                $sched = new Schedules();
                                $sched->setTransaction($transaction);
                                $sched->id = $guid->GUID();
                                $sched->memberid = $request->getPost('id');
                                $sched->scheduledate = $k['reservationdate'];
                                $sched->startdatetime = $k['startdatetime'];
                                $sched->enddatetime = $k['enddatetime'];
                                $sched->serviceid = $k['servicechoice']['serviceid'];
                                $sched->invoiceid = $invoiceno;
                                $sched->price = $k['servicechoice']['price'];
                                $sched->description = $k['servicechoice']['description'];
                                $sched->hourday = $k['servicechoice']['hourday'];
                                $sched->status = $statusS;
                                $sched->created_at = $datecreated;
                                $sched->updated_at = date("Y-m-d H:i:s");
                                $sched->type = "ONLINE";
                                if ($sched->save() == false) {
                                    $array = [];
                                    foreach ($sched->getMessages() as $message) {
                                        $array[] = $message;
                                    }
                                    $transaction->rollback('Cannot save reservation.');
                                }
                            }
                            if($request->getPost('paymenttype')!='paypal') {
                                $content = $cb->invoiceTemplate($ss, $b, $invoiceno, $datecreated, $request->getPost('paymenttype'));
                                $res = $cb->sendMail($members->email, 'Sedona Healing Arts Reservation Details', $content);
                            }
                            $transaction->commit();
                        } else {
                            die(json_encode(array('401' => 'Error Reserving')));
                        }
                    }else{
                        die(json_encode($data));
                    }
                }catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
                    die( json_encode(array('401' => $e->getMessage())) );
                }

                $response['200'] = "Success.";
                $response['invoiceno'] = $invoiceno;
                die(json_encode($response));
            }

    }

    public function usercompletionAction($email){
        $app = new CB();
        $members = Members::findFirst('email="'.$email.'" AND status="INCOMPLETE"');
        if ($members) {
            /* Generate Confirmation Code */
            $jwt = new \Security\Jwt\JWT();
            $payload = array(
                "id" => $members->memberid,
                "email" => $members->email,
                "iat" => time(),
                "exp" => time() + (120 * 60)); // time in the future
            $token = $jwt->encode($payload, $app->getConfig()['hashkey']);

            $members->completioncode = $token;

            if ($members->save() == false) {
                $array = [];
                foreach ($members->getMessages() as $message) {
                    $array[] = $message;
                }
                die(json_encode(array('error' => $array)));
            }

            $content = $app->mailTemplate("Profile Completion", "Hi ". $members->firstname ."! <br><br> Click the link below to complete your profile. <br><br> <a href='".$app->getConfig()['application']['baseURL']."/profilecompletion/".$token."'> ".$app->getConfig()['application']['baseURL']."/profilecompletion/".$token." </a> <br> <br> Thanks!");
            $res = $app->sendMail($members->email, 'Sedona Healing Arts Profile Completion', $content);
            die(json_encode(array('success' => 'Instructions were sent to your email for the completion of your account.')));
        }else{
            die(json_encode(array('error' => 'Username or Email does not belong to any user.')));
        }
    }

    public function setprofilecompletiongAction(){
        $jwt = new \Security\Jwt\JWT();
        $app = new CB();
        $request = new \Phalcon\Http\Request();

        $val  = $jwt->decode($request->getPost('token'), $app->getConfig()['hashkey'], array('HS256'));

        $validate = Members::findFirst('username="'.$request->getPost('username').'"');

        if($validate){
            die(json_encode(array('error' => 'Username is already taken.')));
        }else{
            $members = Members::findFirst('completioncode="'.$request->getPost('token').'" AND email="'.$val->email.'"');
            if ($members) {
                if(time() >= $val->exp){
                    die(json_encode(array('error' => 'Your token has expired already. Please request for completion link again.')));
                }else{
                    $members->username = $request->getPost('username');
                    $members->password = sha1($request->getPost('password'));
                    $members->address1 = $request->getPost('address1');
                    $members->address2 = $request->getPost('address2');
                    $members->country = $request->getPost('country');
                    $members->firstname = $request->getPost('firstname');
                    $members->lastname = $request->getPost('lastname');
                    $members->phoneno = $request->getPost('phoneno');
                    $members->state = $request->getPost('state');
                    $members->zipcode = $request->getPost('zipcode');
                    $members->status = 'REGISTERED';
                    $members->completioncode = '';
                    if ($members->save() == false) {
                        $array = [];
                        foreach ($members->getMessages() as $message) {
                            $array[] = $message;
                        }
                        die(json_encode(array('error' => $array)));
                    }
                    $content = $app->mailTemplate("Profile Completion Success", "Hi ". $members->firstname ."! <br><br> You membership is now complete! <br> <br> Thanks!");
                    $res = $app->sendMail($members->email, 'Sedona Healing Arts Forgot Member Completion', $content);
                    die(json_encode(array('success' => 'You have successfully completed your member information.')));
                }
            }else{
                die(json_encode(array('error' => 'Invalid Token')));
            }
        }
    }


    public function getprofilecompletiongAction(){
        $jwt = new \Security\Jwt\JWT();
        $app = new CB();
        $request = new \Phalcon\Http\Request();
        $members = Members::findFirst('completioncode="'.$request->getPost('token').'"');
        if ($members) {
            $val  = $jwt->decode($members->completioncode, $app->getConfig()['hashkey'], array('HS256'));
            if(time() >= $val->exp){
                die(json_encode(array('error' => 'Your token has expired already. Please request for the completion link again.')));
            }else{
                die(json_encode(array('success' => 'Valid Token', 'data'=> $members->toArray() )));
            }
        }else{
            die(json_encode(array('error' => 'Invalid Token')));
        }
    }

    public function profileupdataAction(){
        $request = new \Phalcon\Http\Request();

        $members = Members::findFirst('memberid="'.$request->getPost('id').'"');
            if ($members) {
                    $members->address1 = $request->getPost('address1');
                    $members->address2 = $request->getPost('address2');
                    $members->country = $request->getPost('country');
                    $members->firstname = $request->getPost('firstname');
                    $members->lastname = $request->getPost('lastname');
                    $members->phoneno = $request->getPost('phoneno');
                    $members->state = $request->getPost('state');
                    $members->zipcode = $request->getPost('zipcode');
                    if ($members->save() == false) {
                        $array = [];
                        foreach ($members->getMessages() as $message) {
                            $array[] = $message;
                        }
                        die(json_encode(array('error' => $array)));
                    }
                    die(json_encode(array('success' => 'You have successfully updated your member information.')));
            }else{
                die(json_encode(array('error' => 'Account not found.')));
            }
    }

    public function updatepasswordAction(){
        $request = new \Phalcon\Http\Request();

        $members = Members::findFirst('memberid="'.$request->getPost('id').'"');
        if ($members) {
            if($members->password != sha1($request->getPost('oldpassword'))){
                die(json_encode(array('error' => 'You current password did not match.')));
            }
            $members->password = sha1($request->getPost('newpassword'));
            if ($members->save() == false) {
                $array = [];
                foreach ($members->getMessages() as $message) {
                    $array[] = $message;
                }
                die(json_encode(array('error' => $array)));
            }
            die(json_encode(array('success' => 'You have successfully update your password.')));
        }else{
            die(json_encode(array('error' => 'Account not found.')));
        }
    }



    public function memberreservationsAction($id){
        $app = new CB();

        $sql = 'SELECT reservations.paymenttype, schedules.id as schedid, schedules.startdatetime,  schedules.enddatetime,  schedules.scheduledate, schedules.status, members.firstname, members.lastname, members.email, schedules.hourday, schedules.price, schedules.description, reservations.invoiceno,  pages.title, schedules.created_at, pagecategory.colorlegend  FROM reservations INNER JOIN schedules ON invoiceid = invoiceno INNER JOIN members ON reservations.memberid = members.memberid INNER JOIN pages ON pages.pageid=schedules.serviceid INNER JOIN pagecategory ON pages.category=pagecategory.id WHERE members.memberid="'.$id.'"';

        $sql .= " ORDER BY schedules.scheduledate DESC ";
        // getting the query

        //var_dump($sql);

        $searchresult = $app->dbSelect($sql);

        echo json_encode(array($searchresult));

    }





}
