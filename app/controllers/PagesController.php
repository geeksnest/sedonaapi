<?php

namespace Controllers;

use \Models\Pages as Pages;
use \Models\Pages_style as Pages_style;
use \Models\Pagecategory as Pagecategory;
use \Models\Serviceprices as Serviceprices;
use \Models\Pageimage as Pageimage;
use \Models\Album as Album;
use \Models\Image as Image;
use \Models\Slider as Slider;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class PagesController extends \Phalcon\Mvc\Controller {

    public function createPagesAction() {

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            try {

                $transactionManager = new TransactionManager();
                $transaction = $transactionManager->get();

                $category = $request->getPost('category');
                $title = $request->getPost('title');
                $slugs = $request->getPost('slugs');
                $subtitle1 = $request->getPost('subtitle1');
                $buttontitle = $request->getPost('buttontitle');
                $body = $request->getPost('body');
                $peoplesaying = $request->getPost('peoplesaying');
                $serviceprice = $request->getPost('serviceprice');
                $prices = $request->getPost('prices');
                $banner = $request->getPost('banner');
                $imagethumb = $request->getPost('imagethumb');
                $layout = $request->getPost('layout');
                $imagethumbsubtitle = $request->getPost('imagethumbsubtitle');
                $thumbdesc = $request->getPost('thumbdesc');
                $metatitle = $request->getPost('metatitle');
                $metadesc = $request->getPost('metadesc');
                $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatags'));

                $textalign = $request->getPost('align');
                $bgcolor = $request->getPost('bgcolor');
                $color = $request->getPost('color');
                $box = $request->getPost('box');
                $featured = $request->getPost('featured');
                $banneropt = $request->getPost('banneropt');
                $bannersize = $request->getPost('bannersize');


                if($category == 'AURA') {
                  $update_query = "UPDATE pages SET status = 0 WHERE category = 'AURA' AND status = 1 ";
                  $update = CB::dbQuery($update_query);
                }

                $guid = new \Utilities\Guid\Guid();
                $id = $guid->GUID();

                $page = new Pages();
                $page->setTransaction($transaction);

                $page->assign(array(
                    'pageid' => $id,
                    'category' => $category,
                    'title' => $title,
                    'pageslugs' => $slugs,
                    'subtitle1' => $subtitle1,
                    'imagethumb' => $imagethumb,
                    'buttontitle' => $buttontitle,
                    'body' => $body,
                    'peoplesaying' => $peoplesaying,
                    'serviceprice' => $serviceprice,
                    'banner' => $banner,
                    'status' => 1,
                    'pagelayout' => $layout,
                    'type' => 'Pages',
                    'imagethumbsubtitle' => $imagethumbsubtitle,
                    'thumbdesc' => $thumbdesc,
                    'metatitle' => $metatitle,
                    'metatags' => $metatags,
                    'metadesc' => $metadesc,
                    'featured' => $featured,
                    'pagecreated_at' => date("Y-m-d H:i:s"),
                    'pageupdated_at' => date("Y-m-d H:i:s"),
                    'thumboption'=> $banneropt,
                    'thumbsize'=>$bannersize
                ));

                if (!$page->save()) {
                    $array = [];
                    foreach ($page->getMessages() as $message) {
                        $array[] = $message;
                    }
                    $transaction->rollback('Cannot save service.');
                }

                foreach($prices as $key => $val){
                    $sp = new Serviceprices();
                    $sp->setTransaction($transaction);

                    $sp->assign(array(
                        'id' => $guid->GUID(),
                        'description' => $val['d'],
                        'price' => $val['amt'],
                        'hourday' => $val['price'],
                        'serviceid' => $page->pageid,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ));

                    if (!$sp->save()) {
                        $array = [];
                        foreach ($sp->getMessages() as $message) {
                            $array[] = $message;
                        }
                        $transaction->rollback('Cannot save service prices.');
                    }
                }

                $style = new Pages_style();
                $style->setTransaction($transaction);

                $style->id = $guid->GUID();
                $style->pageid = $id;
                $style->textalign = $textalign;
                $style->bgcolor = $bgcolor;
                $style->color = $color;
                $style->box = $box;
                $style->classname = CB::randomclassname();
                if (!$style->create()) {
                    foreach ($style->getMessages() as $message) {
                        $error[] = $message->getMessage();
                    }
                    $transaction->rollback($error[0]);
                }

                $transaction->commit();
            }catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }
            $response['200'] = "Success.";
            $response['data'] = array('id' => $page->pageid);
            die(json_encode($response));
        }
    }

    public function saveimageAction() {

         $filename = $_POST['imgfilename'];

        $picture = new Pageimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }

    }

    public function listimageAction() {

        $getimages = Pageimage::find(array("order" => "id DESC"));
        foreach ($getimages as $getimages)
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
        echo json_encode($data);

    }


    public function managepagesAction($num, $page, $keyword, $sort) {

//        if ($keyword == 'null' || $keyword == 'undefined') {
//            // $offsetfinal = ($page * 10) - 10;
//
//            // $db = \Phalcon\DI::getDefault()->get('db');
//            // $stmt = $db->prepare("SELECT * FROM pages ORDER BY dateedited DESC  LIMIT " . $offsetfinal . ",10");
//
//            // $stmt->execute();
//            // $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//
//
//            // $db1 = \Phalcon\DI::getDefault()->get('db');
//            // $stmt1 = $db1->prepare("SELECT * FROM news ORDER BY dateedited DESC");
//
//            // $stmt1->execute();
//            // $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
//
//            // $totalreportdirty = count($searchresult1);
//
//
//            $Pages = Pages::find();
//        } else {
//            $conditions = "title LIKE '%" . $keyword . "%' OR pageslugs LIKE '%" . $keyword . "%'";
//            $Pages = Pages::find(array($conditions));
//        }
//
//        $currentPage = (int) ($page);
//
//        // Create a Model paginator, show 10 rows by page starting from $currentPage
//        $paginator = new \Phalcon\Paginator\Adapter\Model(
//            array(
//                "data" => $Pages,
//                "limit" => 10,
//                "page" => $currentPage
//                )
//            );
//
//        // Get the paginated results
//        $page = $paginator->getPaginate();
//
//        $data = array();
//        foreach ($page->items as $m) {
//            $cat = Pagecategory::findFirst('id="'.$m->category.'"');
//            $catname = $cat->title;
//            $cdata = array(
//                'pageid' => $m->pageid,
//                'title' => $m->title,
//                'pageslugs' => $m->pageslugs,
//                'status' => $m->status,
//                'category' => $catname
//            );
//            $data[] = $cdata;
//        }
//        $p = array();
//        for ($x = 1; $x <= $page->total_pages; $x++) {
//            $p[] = array('num' => $x, 'link' => 'page');
//        }

        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT pages.title, pages.pageid, pages.banner, pages.category, pagecategory.id, pagecategory.title as categorytitle, pages.pageslugs, pages.status FROM pages LEFT JOIN pagecategory ON pages.category = pagecategory.id';
        $sqlCount = 'SELECT COUNT(*) FROM pages LEFT JOIN pagecategory ON pages.category = pagecategory.id';

        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlQuery = " WHERE pages.title LIKE '%" . $keyword . "%' OR pages.pageslugs LIKE '%" . $keyword . "' OR pagecategory.title LIKE '%".$keyword."%' ";
            $sql .= $sqlQuery;
            $sqlCount .= $sqlQuery;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }

        if($sort!="undefined"){
        if($sort == "pageupdated_at" || $sort == "pagecreated_at"){
            $sql .= " ORDER BY UNIX_TIMESTAMP(".$sort.") DESC ";
        } else {
            $sql .= " ORDER BY ".$sort." ASC ";
        }
        }
        

        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        $sqlcat = 'SELECT * FROM pagecategory';
        $cat = $app->dbSelect($sqlcat);

         foreach ($cat as $key => $value) {
            $count=count(Pages::find("category = '".$cat[$key]['id']."'"));
            // $data=Pages::find("category = '".$cat[$key]['id']."'");
            $data = 'SELECT pages.title, pages.pageid, pages.banner, pages.category, pagecategory.id, pagecategory.title as categorytitle, pages.pageslugs, pages.status FROM pages LEFT JOIN pagecategory ON pages.category = pagecategory.id ';
            if ($keyword != 'null' && $keyword != 'undefined') {
                $sqlQuery = " where pages.category = '".$cat[$key]['id']."' AND pages.title LIKE '%" . $keyword . "%' OR pages.pageslugs LIKE '%" . $keyword . "' OR pagecategory.title LIKE '%".$keyword."%' ";
                $data .= $sqlQuery;
                $sqlCount .= $sqlQuery;
            }
            else{
                $sqlQuery = " where pages.category = '".$cat[$key]['id']."'";
                $data .= $sqlQuery;
                $sqlCount .= $sqlQuery;
            }

            if($sort!="undefined"){
                if($sort == "pageupdated_at" || $sort == "pagecreated_at"){
                    $data .= " ORDER BY UNIX_TIMESTAMP(".$sort.") DESC ";
                } else {
                    $data .= " ORDER BY ".$sort." ASC ";
                }
            }
            $dat = $app->dbSelect($data);
            $totalcount[] = array(
                'heading'=>$cat[$key]['title'],
                'count'=>$count,
                'dat'=>$dat
                );
        }
        $other = 'SELECT pages.title, pages.pageid, pages.banner, pages.category, pagecategory.id, pagecategory.title as categorytitle, pages.pageslugs, pages.status FROM pages LEFT JOIN pagecategory ON pages.category = pagecategory.id where pages.category = "Others" order by sortnumber asc ';
        $otherCount = 'SELECT COUNT(*) FROM pages LEFT JOIN pagecategory ON pages.category = pagecategory.id where pages.category = "Others"';
        $others = $app->dbSelect($other);
        $othersCount = $app->dbSelect($otherCount);


        echo json_encode(array('ocount'=>$othersCount[0]["COUNT(*)"],'others'=>$others,'tot'=>$totalcount,'cat'=>$cat,'data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    
   
    }

         public function sortAction(){
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $id = $request->getPost('id');
            foreach ($id as $key => $value) {
             $usave = Pages::findFirst('pageid="' . $value['pageid'] . '"');
             $usave->sortnumber = $key;
             $usave->save();
            }


        }

    }

    public function managepageauraAction($num, $page, $keyword, $sort) {
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT pages.title, pages.pageid, pages.banner, pages.pageslugs, pages.status, pages.thumbdesc, DATE_FORMAT(pages.pagecreated_at,"%d/%m/%Y") as pagecreated_at  FROM pages WHERE category = "AURA" ';
        $sqlCount = 'SELECT COUNT(*) FROM pages WHERE category = "AURA" ';

        if ($keyword != 'null' && $keyword != 'undefined') {
            $sqlQuery = "AND title LIKE '%" . $keyword . "%' OR pageslugs LIKE '%" . $keyword . "%' ";
            $sql .= $sqlQuery;
            $sqlCount .= $sqlQuery;
        }

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        if($sort == "pageupdated_at" || $sort == "pagecreated_at"){
            $sql .= " ORDER BY UNIX_TIMESTAMP(".$sort.") DESC ";
        } else {
            $sql .= " ORDER BY ".$sort." ASC ";
        }

        $sql .= " LIMIT " . $offsetfinal . ",10";
        // getting the query
        $searchresult = $app->dbSelect($sql);

        $totalreportdirty = $app->dbSelect($sqlCount);

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
    }

    public function pageUpdatestatusAction($status,$pageid,$keyword) {

        $data = array();
        $page = Pages::findFirst('pageid="' . $pageid . '"');
        $page->status = $status;
        $page->pageupdated_at = date('Y-m-d H:i:s');
            if (!$page->save()) {
                $data['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
            }

            echo json_encode($data);
    }

    public function pagedeleteAction($pageid) {
        $conditions = 'pageid="' . $pageid . '"';
        $pages = Pages::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($pages) {
            if ($pages->delete()) {
                $data = array('success' => 'Page Deleted');

                $style = Pages_style::findFirst("pageid = '".$pageid."'");
                if($style == true) {
                  if(!$style->delete()) {
                    $data['error'] = "Pages_style is not deleted";
                  }
                }
            }
        }
        echo json_encode($data);
    }

    public function pageeditoAction($pageid) {
        $data = array();
        $leftbaritem = array();
        $rightbaritem = array();

        $pages = Pages::findFirst('pageid="' . $pageid . '"');
        $style = Pages_style::findFirst('pageid="' . $pageid . '"');

        if ($pages) {
            $sp = Serviceprices::find('serviceid="' . $pageid . '"');
            $prices = array();
            foreach($sp as $key=>$val){
                $prices[$key]['amount'] = $val->price;
                $prices[$key]['desc'] = $val->description;
                $prices[$key]['priceper'] = $val->hourday;
            }

            $data = array(
                'pageid' => $pages->pageid,
                'title' => $pages->title,
                'metatitle' => $pages->metatitle,
                'metatags' => $pages->metatags,
                'metadesc' => $pages->metadesc,
                'pageslugs' => htmlentities($pages->pageslugs),
                'subtitle1' => $pages->subtitle1,
                'imagethumb' => $pages->imagethumb,
                'buttontitle' => $pages->buttontitle,
                'body' => $pages->body,
                'peoplesaying' => $pages->peoplesaying,
                'serviceprice' => $pages->serviceprice,
                'banner' => $pages->banner,
                'category' => $pages->category,
                'layout' => $pages->pagelayout,
                'leftbaritem' => $leftbaritem,
                'rightbaritem' => $rightbaritem,
                'imagethumbsubtitle' => $pages->imagethumbsubtitle,
                'thumbdesc' => $pages->thumbdesc,
                'thumboption' => $pages->thumboption,
                'thumbsize' => $pages->thumbsize,
                'prices' => $prices,
                'pageslugs' =>$pages->pageslugs,
                'align' => $style->textalign,
                'bgcolor' => $style->bgcolor,
                'color' => $style->color,
                'box' => $style->box,
                'featured' => $pages->featured,
                'status' => $pages->status
                );
        }
        echo json_encode($data);
    }

    public function saveeditedPagesAction() {

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $pageid = $request->getPost('pageid');
            $title = $request->getPost('title');
            $metatitle = $request->getPost('metatitle');
            $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatags'));
            $metadesc = $request->getPost('metadesc');
            $slugs = $request->getPost('pageslugs');
            $subtitle1 = $request->getPost('subtitle1');
            $subtitle2 = $request->getPost('subtitle2');
            $imagethumb = $request->getPost('imagethumb');
            $buttontitle = $request->getPost('buttontitle');
            $body = $request->getPost('body');
            $peoplesaying = $request->getPost('peoplesaying');
            $serviceprice = $request->getPost('serviceprice');
            $banner = $request->getPost('banner');
            $category = $request->getPost('category');
            $imagethumbsubtitle = $request->getPost('imagethumbsubtitle');
            $thumbdesc  = $request->getPost('thumbdesc');
            $thumboption  = $request->getPost('thumboption');
            $thumbsize  = $request->getPost('thumbsize');
            $prices  = $request->getPost('prices');

            $textalign  = $request->getPost('align');
            $bgcolor  = $request->getPost('bgcolor');
            $color  = $request->getPost('color');
            $box  = $request->getPost('box');
            $featured  = $request->getPost('featured');
            $status  = $request->getPost('status');

            if($category == 'AURA') {
              if($status == '1') {
                $update_query = "UPDATE pages SET status = 0 WHERE category = 'AURA' AND status = 1 ";
                $update = CB::dbQuery($update_query);
              }
            }

            $pages = Pages::findFirst('pageid="' . $pageid . '"');
            $pages->category = $category;
            $pages->title = $title;
            $pages->metatitle = $metatitle;
            $pages->metatags = $metatags;
            $pages->metadesc = $metadesc;
            $pages->pageslugs = $slugs;
            $pages->subtitle1 = $subtitle1;
            $pages->subtitle2 = $subtitle2;
            $pages->pageupdated_at = date("Y-m-d H:i:s");
            $pages->imagethumb = $imagethumb;
            $pages->buttontitle = $buttontitle;
            $pages->body = $body;
            $pages->peoplesaying = $peoplesaying;
            $pages->serviceprice = $serviceprice;
            $pages->banner = $banner;
            $pages->imagethumbsubtitle = $imagethumbsubtitle;
            $pages->thumbdesc = $thumbdesc;
            $pages->thumboption = $thumboption;
            $pages->thumbsize = $thumbsize;
            $pages->featured = $featured;
            $pages->status = $status;

            $app = new CB();
            $app->modelsManager('DELETE FROM \Models\Serviceprices WHERE serviceid = "'.$pageid.'"');

            $guid = new \Utilities\Guid\Guid();
            foreach($prices as $key => $val){
                $sp = new Serviceprices();

                $sp->assign(array(
                    'id' => $guid->GUID(),
                    'description' => $val['desc'],
                    'price' => $val['amount'],
                    'hourday' => $val['priceper'],
                    'serviceid' => $pages->pageid,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ));

                if (!$sp->save()) {
                    $array = [];
                    foreach ($sp->getMessages() as $message) {
                        $array[] = $message;
                    }
                    die(json_encode($array));
                }
            }

            $style = Pages_style::findFirst('pageid="' . $pageid . '"');
            if($style == true) {
              $style->textalign = $textalign;
              $style->bgcolor = $bgcolor;
              $style->color = $color;
              $style->box = $box;
              if($style->save()) {
                $data['success'] = "Success";
              } else {
                $data['error'] = "Something went wrong saving the pages_style";
              }
            } else {
              $newstyle = new Pages_style();
              $newstyle->id = $guid->GUID();
              $newstyle->pageid = $pageid;
              $newstyle->textalign = $textalign;
              $newstyle->bgcolor = $bgcolor;
              $newstyle->color = $color;
              $newstyle->box = $box;
              $newstyle->classname = CB::randomclassname();

              if($newstyle->create()) {
                $data['success'] = "Success";
              } else {
                $data['error'] = "Something went wrong creating the pages_style";
              }
            }

            if (!$pages->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }

            echo json_encode($data);

        }
    }


    public function getPageAction($catslugs,$pageslugs) {
         $find = Pagecategory::findFirst("slugs = '".$catslugs."' ");
         if ($find) {
            $conditions = "pageslugs LIKE'" . $pageslugs . "'";
            echo json_encode(Pages::findFirst(array($conditions))->toArray(), JSON_NUMERIC_CHECK);
         }
    }

    public function getPagecategoryAction($pageslugs){
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT pagecategory.title as categorytitle FROM pages INNER JOIN pagecategory ON pages.category = pagecategory.id WHERE pageslugs LIKE'" . $pageslugs . "'");

        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        echo json_encode($data);
    }

    public function deletepagesimgAction()
    {
        $conditions = 'filename="' . $_POST['imgfilename'] . '"';
        $newsimg = Pageimage::findFirst(array($conditions));
        if ($newsimg) {
            if ($newsimg->delete()) {
                $data = array('success' => 'Image Deleted');
            }
            else
            {
                $data = array('error' => 'Image Not Deleted');
            }
        }
        echo json_encode($data);
    }

    public function createpagecategoryAction()
    {
        $guid = new \Utilities\Guid\Guid();
        $id = $guid->GUID();
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){


            $title = $request->getPost('title');
            $subtitle = $request->getPost('subtitle');
            $colorlegend = $request->getPost('color');
            $slugs = $request->getPost('slugs');


            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();

            $page = new Pagecategory();
            $page->assign(array(
                'id' => $id,
                'title' => $title,
                'subtitle' => $subtitle,
                'date' =>date("Y-m-d H:i:s"),
                'colorlegend' => $colorlegend,
                'slugs' => $slugs
                ));

            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {



                    $data['success'] = "Success";
                }


        }

        echo json_encode($data);

    }

    public function listpagecategoryAction()
    {


        $pagecat = Pagecategory::find(array());
        $listcat = json_encode($pagecat->toArray(), JSON_NUMERIC_CHECK);
        echo $listcat;
    }

    public function managePagesCategoryAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Pagecategory::find();
        } else {
            $conditions = "title LIKE '%" . $keyword . "%'";
            $Pages = Pagecategory::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'title' => $m->title,
                'subtitle' => $m->subtitle,
                'colorlegend' => $m->colorlegend,
                'slugs' => $m->slugs
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }

    public function categorydeleteAction($catid) {
        $conditions = 'id="' . $catid . '"';
        $pages = Pagecategory::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($pages) {
            if ($pages->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }
        echo json_encode($data);
    }

    public function updatecategoryAction($catid)
    {
        $data = array();
        $pagecat = Pagecategory::findFirst('id="' . $catid .'"');
        $pagecat->title = $_POST['title'];
        $pagecat->subtitle = $_POST['subtitle'];
        $pagecat->colorlegend = $_POST['color'];
        $pagecat->slugs = $_POST['slugs'];

        if (!$pagecat->save())
        {
            $data['error'] = "Something went wrong saving the data, please try again.";
        }
        else
        {
            $data['success'] = "Success";
        }
        echo json_encode($data);
    }


    public function getAction(){
        $pages = Pages::find();
        echo json_encode($pages->toArray());
    }

    public function listmenuAction($cat) {

        $pagecat = Pagecategory::findFirst('title="' . $cat .'"');
        $catid = $pagecat->id;

        $query = "SELECT pages.title, pages.pageid, pages.thumbdesc, pages.subtitle1, pages.imagethumbsubtitle, pages.pageslugs,pages.buttontitle,pages.body, pages.peoplesaying, pages.status, pages.pagelayout, pages.type, pages.serviceprice, pages.banner, pages.category, pages.imagethumb,  pagecategory.title as cattitle,  pagecategory.subtitle as catsubtitle,
                  pages_style.textalign, pages_style.bgcolor, pages_style.color, pages_style.box, pages_style.classname
                    FROM pages
                      INNER JOIN pagecategory ON pagecategory.id = pages.category
                      LEFT JOIN pages_style ON pages.pageid = pages_style.pageid
                    WHERE category LIKE '". $catid ."' AND status=1";
        $data = CB::dbSelect($query);

        echo json_encode($data);

    }

    public function getpagesAction() {

//        $pagecat = Pagecategory::find();
//        $newdata = array();
//        if($pagecat){
//            foreach($pagecat as $key => $val){
//            $db = \Phalcon\DI::getDefault()->get('db');
//            $stmt = $db->prepare("SELECT pages.title, pages.pageid FROM pages WHERE category = '". $val->id ."' AND status=1");
//            $stmt->execute();
//            $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//                $x = 0;
//                foreach($data as $k => $v){
//                    $db2 = \Phalcon\DI::getDefault()->get('db');
//                    $stmt2 = $db2->prepare("SELECT * FROM serviceprices WHERE serviceid = '". $v['pageid'] ."'");
//                    $stmt2->execute();
//                    $data2 = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
//                    $data[$x]['prices'] = $data2;
//                    $x++;
//                }
//                $newdata[]= array(
//                    'title'=> $val->title,
//                    'pagecatid'=> $val->id,
//                    'pages'=>$data
//                );
//            }
//        }

//        if($pagecat){

            $newdata = array();
            $db = \Phalcon\DI::getDefault()->get('db');
            $stmt = $db->prepare("SELECT pages.title, pages.pageid, pagecategory.title as cattitle, pagecategory.colorlegend FROM pages INNER JOIN pagecategory ON pagecategory.id = pages.category WHERE status=1");
            $stmt->execute();
            $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            foreach($data as $key => $v){
                $x = 0;
                foreach($data as $k => $v){
                    $db2 = \Phalcon\DI::getDefault()->get('db');
                    $stmt2 = $db2->prepare("SELECT * FROM serviceprices WHERE serviceid = '". $v['pageid'] ."'");
                    $stmt2->execute();
                    $data2 = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
                    $data[$x]['prices'] = $data2;
                    $x++;
                }
            }
//        }

        echo json_encode($data);

    }

    public function addpriceAction(){

    }

    public function loadcatpagesAction($catid){
        $app = new CB();
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT pages.pageid, pages.title FROM pages LEFT JOIN pagecategory on pages.category = pagecategory.id WHERE pagecategory.id='$catid'");
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $stmt = $db->prepare("SELECT * FROM pagecategory WHERE id != '$catid' ");
        $stmt->execute();
        $category = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode(array("dataconflict" => $data, "catpages" => $category));
    }

    public function updatepagescatAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $category = $request->getPost('catid');
            $pageid = $request->getPost('pageid');

            $pages = Pages::findFirst('pageid="' . $pageid . '"');
            $pages->category = $category;
            if(!$pages->save()){
                $errors = array();
                foreach ($pages->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            }
        }
    }

    public function menuauraAction() {
      $find = Pages::findFirst("category = 'AURA' AND status = 1");
      if($find == true) {
        echo json_encode($find->pageslugs);
      }
    }

    public function catcontentAction($pageslugs) {
      $find = Pagecategory::findFirst("slugs = '".$pageslugs."' ");
      $find2 = Pages::findFirst("pageslugs = '".$pageslugs."' ");
      if($find == true) {
        $category = $find->id;
        $album = Album::findFirst('main=1 and category="'.$category.'"');
        if ($album) {
           $image = Slider::find("album_id='".$album->album_id."' and status=1 order by sort ASC");
            $slides = json_encode($image->toArray(), JSON_NUMERIC_CHECK);
        }

        // $catid = $pagecat->id;
        $query = "SELECT pages.title, pages.pageid, pages.thumbdesc, pages.subtitle1, pages.imagethumbsubtitle, pages.pageslugs,pages.buttontitle,pages.body, pages.peoplesaying, pages.status, pages.pagelayout, pages.type, pages.serviceprice, pages.banner, pages.category, pages.imagethumb, pages.thumboption,pages.thumbsize, pagecategory.title as cattitle,  pagecategory.subtitle as catsubtitle,
                  pages_style.textalign, pages_style.bgcolor, pages_style.color, pages_style.box, pages_style.classname
                    FROM pages
                      INNER JOIN pagecategory ON pagecategory.id = pages.category
                      LEFT JOIN pages_style ON pages.pageid = pages_style.pageid
                    WHERE category LIKE '". $category ."' AND status=1 order by pages.sortnumber asc";
        $panel = CB::dbSelect($query);

        echo json_encode(array('data' => $find,'slides'=> $slides,'panel'=>$panel));
      }
      elseif ($find2 == true) {
          $categoryslug = $find2->category;
          if ($categoryslug=='Others') {
               $query = "SELECT pages.title, pages.sortnumber, pages.pageid, pages.thumbdesc, pages.subtitle1, pages.imagethumbsubtitle, pages.pageslugs,pages.buttontitle,pages.body, pages.peoplesaying, pages.status, pages.pagelayout, pages.type, pages.serviceprice, pages.banner, pages.category, pages.imagethumb,  pagecategory.title as cattitle,  pagecategory.subtitle as catsubtitle,
                  pages_style.textalign, pages_style.bgcolor, pages_style.color, pages_style.box, pages_style.classname
                    FROM pages
                      INNER JOIN pagecategory ON pagecategory.id = pages.category
                      LEFT JOIN pages_style ON pages.pageid = pages_style.pageid
                    WHERE category LIKE '". $category ."' AND status=1 order by pages.sortnumber asc";
        $panel = CB::dbSelect($query);

        echo json_encode(array('datapages' => $find2,'panel'=>$panel));
          }

      }


    }


}
