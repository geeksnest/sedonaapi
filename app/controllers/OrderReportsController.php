<?php

namespace Controllers;

use \Models\Product as Product;
use \Models\Members as Members;
use \Models\Orderlist as Orderlist;
use \Models\Billinginfo as Billinginfo;
use \Models\Shippinginfo as Shippinginfo;
use \Models\Orderprod as Orderprod;
use \Models\Ordercomment as Ordercomment;
use \Models\Shopbag as Shopbag;
use \Models\Ordertemp as Ordertemp;
use \Models\Orderprodtemp as Orderprodtemp;
use \Models\Prodcat as Prodcat;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class OrderReportsController extends \Phalcon\Mvc\Controller {
  
  public function orderlistAction($offset, $page, $startdate, $enddate,$status,$payment,$members){
    $app = new CB();
    $offsetfinal = ($page * 10) - 10;

    if($offsetfinal < 0){
      $offsetfinal = 0;
    }

    $dummysql = "SELECT orderlist.id,orderlist.invoiceno,orderlist.amount,orderlist.status,orderlist.created_at,orderlist.firstname,
      orderlist.lastname,orderlist.paymenttype,shippinginfo.firstname AS shipfirstname,shippinginfo.lastname AS shiplastname FROM orderlist INNER JOIN members ON orderlist.memberid=members.memberid 
      LEFT JOIN shippinginfo ON orderlist.id=shippinginfo.orderid ";
    $searchsql = "SELECT orderlist.id,orderlist.invoiceno,orderlist.amount,orderlist.status,orderlist.created_at,orderlist.firstname,
    orderlist.lastname,orderlist.paymenttype,shippinginfo.firstname AS shipfirstname,shippinginfo.lastname AS shiplastname FROM orderlist INNER JOIN members ON orderlist.memberid=members.memberid 
    LEFT JOIN shippinginfo ON orderlist.id=shippinginfo.orderid ";

      $sql = "SELECT orderlist.id,
      orderlist.invoiceno,
      orderlist.amount,
      orderlist.status,
      orderlist.created_at,
      orderlist.firstname,
      orderlist.lastname,
      orderlist.paymenttype,
      shippinginfo.firstname AS shipfirstname,
      shippinginfo.lastname AS shiplastname FROM orderlist INNER JOIN members ON orderlist.memberid=members.memberid 
      LEFT JOIN shippinginfo ON orderlist.id=shippinginfo.orderid ";

    $sqlCount = "SELECT COUNT(*) FROM orderlist INNER JOIN members ON orderlist.memberid=members.memberid LEFT JOIN billinginfo on members.memberid=billinginfo.memberid ";

    if($startdate != undefined){
        //status not empty
        if ($status!=undefined and $payment==undefined and $members==undefined) {
          $sqlWhere = "WHERE orderlist.status = '".$status."' and orderlist.created_at >= '" . $startdate . "' and orderlist.created_at <= '" . $enddate . "'";
          $sql .= $sqlWhere;
          $searchsql .= $sqlWhere;
          $sqlCount .= $sqlWhere;
        }
        //payment not empty
        elseif ($payment!=undefined and $status==undefined and $members==undefined) {
          $sqlWhere = "WHERE orderlist.paymenttype = '".$payment."' and orderlist.created_at >= '" . $startdate . "' and orderlist.created_at <= '" . $enddate . "'";
          $sql .= $sqlWhere;
          $searchsql .= $sqlWhere;
          $sqlCount .= $sqlWhere;
        }
        //members not empty
        elseif ($members!=undefined and $status==undefined and $payment==undefined) {
          $sqlWhere = "WHERE orderlist.memberid = '".$members."' and orderlist.created_at >= '" . $startdate . "' and orderlist.created_at <= '" . $enddate . "'";
          $sql .= $sqlWhere;
          $searchsql .= $sqlWhere;
          $sqlCount .= $sqlWhere;
        }
        //payment and status not empty
        elseif ($status!=undefined and $members==undefined and $payment!=undefined) {
          $sqlWhere = "WHERE orderlist.status = '".$status."' and orderlist.paymenttype = '".$payment."' and orderlist.created_at >= '" . $startdate . "' and orderlist.created_at <= '" . $enddate . "'";
          $sql .= $sqlWhere;
          $searchsql .= $sqlWhere;
          $sqlCount .= $sqlWhere;
        }
        //payment and members not empty
        elseif ($members!=undefined and $status==undefined and $payment!=undefined) {
          $sqlWhere = "WHERE orderlist.memberid = '".$members."' and orderlist.paymenttype = '".$payment."' and orderlist.created_at >= '" . $startdate . "' and orderlist.created_at <= '" . $enddate . "'";
          $sql .= $sqlWhere;
          $searchsql .= $sqlWhere;
          $sqlCount .= $sqlWhere;
        }
        //status and members not empty
        elseif ($status!=undefined and $payment==undefined and $members!=undefined) {
          $sqlWhere = "WHERE orderlist.memberid = '".$members."' and orderlist.status = '".$status."' and orderlist.created_at >= '" . $startdate . "' and orderlist.created_at <= '" . $enddate . "'";
          $sql .= $sqlWhere;
          $searchsql .= $sqlWhere;
          $sqlCount .= $sqlWhere;
        }
        //payment status and members not empty
        elseif ($status!=undefined and $payment!=undefined and $members!=undefined) {
          $sqlWhere = "WHERE orderlist.paymenttype = '".$payment."' and orderlist.memberid = '".$members."' and orderlist.status = '".$status."' and orderlist.created_at >= '" . $startdate . "' and orderlist.created_at <= '" . $enddate . "'";
          $sql .= $sqlWhere;
          $searchsql .= $sqlWhere;
          $sqlCount .= $sqlWhere;
        }
        else{
         $sqlWhere = "WHERE orderlist.created_at >= '" . $startdate . "' and orderlist.created_at <= '" . $enddate . "'";
         $sql .= $sqlWhere;
         $searchsql .= $sqlWhere;
         $sqlCount .= $sqlWhere;
        }
     
    }
    elseif ($status != undefined) {
      //payment not empty
       if ($payment!=undefined and $members==undefined) {
          $sqlWhere = "WHERE orderlist.paymenttype = '".$payment."' and orderlist.status = '" . $status . "'";
          $sql .= $sqlWhere;
          $searchsql .= $sqlWhere;
          $sqlCount .= $sqlWhere;
        }
        //members not empty
       elseif ($members!=undefined and $payment==undefined) {
          $sqlWhere = "WHERE orderlist.memberid = '".$members."' and orderlist.status = '" . $status . "'";
          $sql .= $sqlWhere;
          $searchsql .= $sqlWhere;
          $sqlCount .= $sqlWhere;
        }
        //payment and members not empty
       elseif ($members!=undefined and $payment!=undefined) {
          $sqlWhere = "WHERE orderlist.paymenttype = '".$payment."' and orderlist.memberid = '".$members."' and orderlist.status = '" . $status . "'";
          $sql .= $sqlWhere;
          $searchsql .= $sqlWhere;
          $sqlCount .= $sqlWhere;
        }
        else{
          $sqlWhere = "WHERE orderlist.status = '" . $status . "'";
          $sql .= $sqlWhere;
          $searchsql .= $sqlWhere;
          $sqlCount .= $sqlWhere;
        }
    }
    elseif ($payment != undefined) {
      if ($members != undefined) {
        $sqlWhere = "WHERE orderlist.paymenttype = '".$payment."' and orderlist.memberid = '".$members."' ";
        $sql .= $sqlWhere;
        $searchsql .= $sqlWhere;
        $sqlCount .= $sqlWhere;
      }
      else{
      $sqlWhere = "WHERE orderlist.paymenttype = '".$payment."'";
      $sql .= $sqlWhere;
      $searchsql .= $sqlWhere;
      $sqlCount .= $sqlWhere;
      }
    }
    elseif ($members != undefined) {
      $sqlWhere = "WHERE orderlist.memberid = '".$members."'";
      $sql .= $sqlWhere;
      $searchsql .= $sqlWhere;
      $sqlCount .= $sqlWhere;
    }

    $sql .= " ORDER BY orderlist.created_at DESC LIMIT " . $offsetfinal . ", 10";
    $searchresult = $app->dbSelect($sql);
    $dummyall = $app->dbSelect($dummysql);
    $dummysearch = $app->dbSelect($searchsql);
    $total_items = $app->dbSelect($sqlCount);
    // die(var_dump(array('sql' => $searchresult, 'sqlcount' => $total_items )));

    $total_processing = $app->dbSelect("SELECT COUNT(*) FROM orderlist WHERE status='processing'");
    $total_completed = $app->dbSelect("SELECT COUNT(*) FROM orderlist WHERE status='completed'");
    $total_pending = $app->dbSelect("SELECT COUNT(*) FROM orderlist WHERE status='pending'");
    $total_today = $app->dbSelect("SELECT COUNT(*) FROM orderlist WHERE created_at LIKE '%" . date('Y-m-d') . "%'");

    echo json_encode(array(
      'orders' => $searchresult,
      'allorders' => $dummyall,
      'searchorders' => $dummysearch, 
      'index' =>$page, 
      'total_items' => $total_items[0]['COUNT(*)'], 
      'processing' => $total_processing[0]['COUNT(*)'],
      'completed' => $total_completed[0]['COUNT(*)'],
      'pending' => $total_pending[0]['COUNT(*)'],
      'today' => $total_today[0]['COUNT(*)']), JSON_NUMERIC_CHECK);
    // echo json_encode($page);

  }



    public function TopMonthlyAction(){
        $app = new CB();
        $currentMonth = (int)date('m');
             $test = "SELECT count(*) as count,sum(orderprod.quantity) as qty, orderlist.id,orderlist.created_at,orderprod.orderid,orderprod.productid,
             orderprod.quantity,product.name,product.productid FROM orderlist INNER JOIN orderprod ON orderlist.id=orderprod.orderid INNER JOIN product ON orderprod.productid=product.productid WHERE Month(orderlist.created_at)='".$currentMonth."' group by product.productid order by qty desc limit 10";

             $result1 = $app->dbSelect($test);
             foreach ($result1 as $get1) 
                {
                 
                   $data[] = array(
                    'prodname'=> $get1['name'],
                    'data'=>$get1['count']
                    );
                   $xdata[] = array(
                    'name'=> $get1['name'],
                    'qty' => $get1['qty']
                    );

                }
 echo json_encode(array('data' => $data, 'xdata'=>$xdata,'months' =>$months,'totalreservation'=>$totalcount), JSON_NUMERIC_CHECK);

    }

     public function MonthlySalesAction(){
        $app = new CB();
        $sql = "Select * from orderlist where status != 'VOID'";
        $result = $app->dbSelect($sql);
        // $totalcount = count(schedules::find());
        $months = array();
        $currentMonth = (int)date('m');
        $currentyear = (int)date('Y');

        for ($x = $currentMonth; $x > $currentMonth - 12; $x--) {
            $months[] = date('F Y', mktime(0, 0, 0, $x, 1));
            $months1[] = date('n', mktime(0, 0, 0, $x, 1));
            $year[] = date('Y', mktime(0, 0, 0, $x, 1));
        }

          foreach ($months as $key => $get) 
            {
             $test='Select SUM(amount) as totalsales,count(*) as count from orderlist where Month(created_at) = "'.$months1[$key].'" and Year(created_at) = "'.$year[$key].'" and status != "VOID"';
             $result1 = $app->dbSelect($test);
             foreach ($result1 as $get1) 
                {
                    if ($months1[$key]!=null)
                {
                   $data[] = array(
                'tot'=>$get1['totalsales'],
                'count'=>$get1['count'],
                'month' => $months1[$key],
                'year' => $year[$key]
                );
               }
               
                }

            }

 echo json_encode(array('data' => $data, 'months' =>$months,'totalreservation'=>$totalcount), JSON_NUMERIC_CHECK);

    }

    public function DailyOrdersAction(){
        $date = date("Y-m-d");
        $app = new CB();
        $sql = "SELECT count(*) as count,sum(orderprod.quantity) as qty, orderlist.id,orderlist.created_at,orderprod.orderid,orderprod.productid,
             orderprod.quantity,product.name,product.productid FROM orderlist INNER JOIN orderprod ON orderlist.id=orderprod.orderid INNER JOIN product ON orderprod.productid=product.productid WHERE DATE(orderlist.created_at)='".$date."' group by product.productid order by qty";

        $result = $app->dbSelect($sql);
        
       foreach ($result as $key => $get) 
       {

        $data[] = array(
                    'prodname'=> $get['name'],
                    'data'=>$get['qty']
                    );
        $xdata[] = array(
                    'data'=>$get['qty']
                    );
       }
 echo json_encode(array('data'=>$data,'xdata'=>$xdata,'date'=>$date), JSON_NUMERIC_CHECK);
      // echo json_encode($result);
    }

        public function MonthlyOrdersAction(){
        $currentMonth = (int)date('m');
        $app = new CB();
        $sql = "SELECT count(*) as count,sum(orderprod.quantity) as qty, orderlist.id,orderlist.created_at,orderprod.orderid,orderprod.productid,
             orderprod.quantity,product.name,product.productid FROM orderlist INNER JOIN orderprod ON orderlist.id=orderprod.orderid INNER JOIN product ON orderprod.productid=product.productid WHERE Month(orderlist.created_at)='".$currentMonth."' group by product.productid order by qty";

        $result = $app->dbSelect($sql);
        
       foreach ($result as $key => $get) 
       {

        $data[] = array(
                    'prodname'=> $get['name'],
                    'data'=>$get['qty']
                    );
        $xdata[] = array(
                    'data'=>$get['qty']
                    );
       }
 echo json_encode(array('data'=>$data,'xdata'=>$xdata), JSON_NUMERIC_CHECK);
      // echo json_encode($data);
    }

    public function TopProductsAction(){
        $app = new CB();
        $currentMonth = (int)date('m');
             $test = "SELECT count(*) as count,sum(orderprod.quantity) as qty, orderlist.id,orderlist.created_at,orderprod.orderid,orderprod.productid,
             orderprod.quantity,product.name,product.productid FROM orderlist INNER JOIN orderprod ON orderlist.id=orderprod.orderid INNER JOIN product ON orderprod.productid=product.productid group by product.productid order by qty desc limit 10";

             $result1 = $app->dbSelect($test);
             foreach ($result1 as $get1) 
                {
                   $data[] = array(
                    'prodname'=> $get1['name'],
                    'data'=>$get1['qty']
                    );
                   $xdata[] = array(
                    'name'=> $get1['name'],
                    'data' => $get1['qty']
                    );

                }
 echo json_encode(array('data' => $data, 'xdata'=>$xdata,'months' =>$months,'totalreservation'=>$totalcount), JSON_NUMERIC_CHECK);

    }

}
