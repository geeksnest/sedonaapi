<?php

namespace Controllers;

use \Models\Googlescript as Googlescript;
use \Models\Maintenance as Maintenance;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class SettingsController extends \Phalcon\Mvc\Controller {
    
    public function loadsettingsAction(){

        $googlescript = Googlescript::findFirst();
        if($googlescript){
            $maintenance = Maintenance::findFirst();
            if($maintenance){
                $data = array(
                    'id' => $maintenance->id,
                    'message' => $maintenance->message,
                    'hour' => $maintenance->hour,
                    'status' => $maintenance->status,
                    'date' => $maintenance->date,
                    'enddate'=> $maintenance->enddate 
                    );       
            }else {
                $data = array(
                    'status' => 0
                    );  
            }
            echo json_encode(array('gscript' => $googlescript, 'maintenance' => $data));
        }
    }

    public function updatescriptAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $script = $request->getPost('script');

            $googlescript = Googlescript::findFirst('id=1');
            $googlescript->script = $script;
            $googlescript->save();
        }
    }

    public function loadmaintenanceAction() {
        echo json_encode($data);
    }

    public function savemaintenanceAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $hour = $request->getPost('hour');
            $message = $request->getPost('message');
            $status = $request->getPost('status') == true ? 1 : 0 ;

            $maintenance = Maintenance::findFirst("status=0");
            $maintenance->hour = $hour;
            $maintenance->message = $message;
            $maintenance->status = $status;
            $maintenance->date = date('Y-m-d H:i:s');
            $date = date('Y-m-d H:i:s', strtotime('+'.$hour.' hour'));
            $maintenance->enddate = strtotime($date);

            if($maintenance->save()){
                echo "Success";
            }else {
                echo "Error";
            }
        }
    }

    public function offmaintenanceAction(){
        $off = Maintenance::findFirst("status=1");
        $off->status = 0;
        if($off->save()){
            echo "Maintenance successfully turned off.";
        }else {
            echo "An error occured.";
        }
    }
}