<?php

namespace Controllers;

use Security\Jwt\JWT;
use \Models\Feusers as Feusers;
use \Models\Members as Members;
use Phalcon\Http\Request;
use \Models\Shopbag as Shopbag;
use \Models\Billinginfo as Billinginfo;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class FeuserController extends \Phalcon\Mvc\Controller {

    public function registeruserAction(){

        $data = array();
        $request = new Request();
        $cb = new CB();

        if ($request->isPost()){

            $firstname   = $request->getPost("firstname");
            $lastname   = $request->getPost("lastname");
            $email   = $request->getPost("email");
            $password   = $request->getPost("password");

            $findemail = Feusers::findFirst("email = '".$email."'");
            if($findemail){
                $data['status'] = "exist";
                $data['message'] = "Email is Already Exist!";
                $data['email'] = $email;
            }
            else{
                $saveuser = new Feusers();
                $saveuser->userid = $cb->genGuid();
                $saveuser->firstname = $firstname;
                $saveuser->lastname = $lastname;
                $saveuser->email = $email;
                $saveuser->password = sha1($password);
                $saveuser->created_at = date("Y-m-d H:i:s");

                // $bi = new Billinginfo();
                // $bi->id = $cb->genGuid();
                // $bi->memberid = $saveuser->userid;
                // $bi->firstname = $firstname;
                // $bi->lastname = $lastname;
                // $bi->address

                if($saveuser->save()){
                    $data['status'] = "success";
                    $data['message'] = "Congratulations you are now registered!";
                }
                else{
                    $data['status'] = "error";
                    $data['message'] = "Something went wrong!";
                }
            }


            echo json_encode($data);
        }

    }

    public function loginAction() {
        $app = new CB();
        $request = new \Phalcon\Http\Request();
        $user = Members::findFirst('(email="' .  $request->getPost('username').'" OR username="' .$request->getPost('username').'") AND  password="'. sha1($request->getPost('password')).'" AND status = "REGISTERED"');
        if($user) {
            $jwt = new JWT();

            $timestamp = time();

            $access_token = array(
                "id" => $user->memberid,
                "lastname" => $user->lastname,
                "firstname" => $user->firstname,
                "level" => $app->config->clients->member,
                "exp" => $timestamp + (60 * 60)); // time in the future

            $token = $jwt->encode($access_token, $app->config->hashkey);

            $guid = new \Utilities\Guid\Guid();
            $refresh_token = array(
                "id" => $guid->GUID(),
                "exp" => strtotime('+1 day', $timestamp),
                "created" => $timestamp
            );

            $atoken = $jwt->encode($access_token, $app->config->hashkey);
            $rtoken = $jwt->encode($refresh_token, $app->config->hashkey);

            $app->storeRedis($user->memberid,$atoken);

            $bag = $app->dbSelect("SELECT product.productcode,
                product.name,
                product.color,
                productimages.filename,
                product.maxquantity,
                product.minquantity,
                product.price,
                product.productid,
                product.quantity,
                product.shortdesc,
                product.slugs,
                product.discount,
                product.discount_from,
                product.discount_to,
                shopbag.quantity AS cartquantity FROM shopbag INNER JOIN product ON shopbag.productid=product.productid
                INNER JOIN productimages ON product.productid=productimages.productid WHERE productimages.status=1 AND shopbag.memberid='" . $user->memberid . "'");

            if(count($bag) == 0){
                $bag = [];
            }

            echo json_encode(array('access' => $atoken, 'refresh' => $rtoken, 'cart' => $bag));
        }
        else{
            echo json_encode(array('error' => 'Username or Password is invalid.'));
        }
    }

    public function checkfacebookAction(){
        $app = new CB();
        $request = new \Phalcon\Http\Request();

        $checkfbid = Members::findFirst("facebookid='".$request->getPost('fbid')."'");
        if($checkfbid){

            $jwt = new JWT();

            $timestamp = time();

            $access_token = array(
                "id" => $checkfbid->memberid,
                "lastname" => $checkfbid->lastname,
                "firstname" => $checkfbid->firstname,
                "level" => $app->config->clients->agent,
                "exp" => $timestamp + (60 * 60)); // time in the future

            $token = $jwt->encode($access_token, $app->config->hashkey);

            $guid = new \Utilities\Guid\Guid();
            $refresh_token = array(
                "id" => $guid->GUID(),
                "exp" => strtotime('+1 day', $timestamp),
                "created" => $timestamp
            );

            $atoken = $jwt->encode($access_token, $app->config->hashkey);
            $rtoken = $jwt->encode($refresh_token, $app->config->hashkey);

            $app->storeRedis($user->memberid,$atoken);

            $data['access'] = $atoken;
            $data['refresh'] = $rtoken;
        }
        else{
            $guid = new \Utilities\Guid\Guid();
            $memberidgen = $guid->GUID();
            $savemember = new Members();
            $savemember->memberid = $memberidgen;
            $savemember->facebookid = $request->getPost('fbid');
            $savemember->firstname = $request->getPost('first_name');
            $savemember->lastname = $request->getPost('last_name');
            $savemember->address1 = $request->getPost('hometown');

            if($savemember->save()){

                $checkfbid = Members::findFirst("facebookid='".$request->getPost('fbid')."'");

                if($checkfbid){

                    $jwt = new JWT();

                    $timestamp = time();

                    $access_token = array(
                        "id" => $checkfbid->memberid,
                        "lastname" => $checkfbid->lastname,
                        "firstname" => $checkfbid->firstname,
                        "level" => $app->config->clients->agent,
                        "exp" => $timestamp + (60 * 60)); // time in the future

                    $token = $jwt->encode($access_token, $app->config->hashkey);

                    $guid = new \Utilities\Guid\Guid();
                    $refresh_token = array(
                        "id" => $guid->GUID(),
                        "exp" => strtotime('+1 day', $timestamp),
                        "created" => $timestamp
                    );

                    $atoken = $jwt->encode($access_token, $app->config->hashkey);
                    $rtoken = $jwt->encode($refresh_token, $app->config->hashkey);

                    $app->storeRedis($user->memberid,$atoken);

                    $data['access'] = $atoken;
                    $data['refresh'] = $rtoken;
                }
                else{

                }


            }
            else{
                $data['error'] = 'error';
            }
        }
        echo json_encode($data);
    }

    public function logoutAction(){
        $request = new \Phalcon\Http\Request();
        $jwt = new \Security\Jwt\JWT();
        $parsetoken = explode(" ",$request->getHeader('Authorization'));
        $app = new CB();
        $token = $jwt->decode($parsetoken[1], $app->config->hashkey, array('HS256'));

        if($app->removeRedis($token->id)){
            die(json_encode(array("success" => 'Succcess')));
        }else{
            die(json_encode(array("error" => 'Succcess')));
        }
    }

    public function checkoutloginAction() {
        $app = new CB();
        $request = new \Phalcon\Http\Request();
        $user = Members::findFirst('(email="' .  $request->getPost('user')['username'] .'" OR username="' . $request->getPost('user')['username'] .'") AND  password="'. sha1($request->getPost('user')['password']).'" AND status = "REGISTERED"');
        if($user) {
            $jwt = new JWT();

            $timestamp = time();

            $access_token = array(
                "id" => $user->memberid,
                "lastname" => $user->lastname,
                "firstname" => $user->firstname,
                "level" => $app->config->clients->member,
                "exp" => $timestamp + (60 * 60)); // time in the future

            $token = $jwt->encode($access_token, $app->config->hashkey);

            $guid = new \Utilities\Guid\Guid();
            $refresh_token = array(
                "id" => $guid->GUID(),
                "exp" => strtotime('+1 day', $timestamp),
                "created" => $timestamp
            );

            $atoken = $jwt->encode($access_token, $app->config->hashkey);
            $rtoken = $jwt->encode($refresh_token, $app->config->hashkey);

            $app->storeRedis($user->memberid,$atoken);

            try {
                $transactionManager = new TransactionManager();
                $transaction = $transactionManager->get();

                foreach ($request->getPost('cart') as $key => $value) {
                    $s = new Shopbag();
                    $s->setTransaction($transaction);
                    $guid = new \Utilities\Guid\Guid();
                    $s->assign(array(
                        'id' => $guid->GUID(),
                        'productid' => $value['productid'],
                        'quantity' => $value['cartquantity'],
                        'memberid' => $user->memberid,
                        'created_at' => date('Y-m-d H:i:s')
                    ));

                    if($s->save() == false){
                      $data = [];
                      foreach ($s->getMessages() as $message) {
                        $data[] = $message;
                      }
                      $transaction->rollback();
                      die(json_encode(array('error' => 'Something went wrong, please try again later' )));
                    }
                }

                $transaction->commit();

                echo json_encode(array('access' => $atoken, 'refresh' => $rtoken));
            } catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }
        } else {
            echo json_encode(array('error' => 'Username or Password is invalid.'));
        }
    }

}
